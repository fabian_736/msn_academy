<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RutasController extends Controller
{

    public function form_login(){
        return view('auth.form_login');
    }

    public function forgot(){
        return view('auth.forgot');
    }

    public function reset(){
        return view('auth.reset');
    }

    public function newpass(){
        return view('auth.newpass');
    }

}
