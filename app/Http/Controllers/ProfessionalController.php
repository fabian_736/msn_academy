<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProfessionalController extends Controller
{

    public function tutorial_professional(){
        return view('professional.tutorial.index');
    }
    
    public function portal_professional()
    {  
        return view('professional.portal.index');
    } 

    public function perfil_professional()
    {  
        return view('professional.perfil.index');
    } 

    public function perfil_edit_professional()
    {  
        return view('professional.perfil.edit');
    } 

    public function perfil_certificado_professional()
    {  
        return view('professional.perfil.certificado');
    }

    public function practice_professional()
    {  
        return view('professional.practice.index');
    }

    public function cuestionario_professional()
    {  
        return view('professional.cuestionario.index');
    }

    public function insignia_professional()
    {  
        return view('professional.insignia.index');
    }

    public function muro_professional()
    {  
        return view('professional.muro.index');
    }

    public function premio_professional()
    {  
        return view('professional.premio.index');
    }

    
    public function premioselect_professional()
    {  
        return view('professional.premio.premioselect');
    }
}