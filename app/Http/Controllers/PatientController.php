<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PatientController extends Controller
{
    public function tutorial_patient(){
        return view('patient.tutorial.index');
    }

    public function portal_patient()
    {  
        return view('patient.portal.index');
    } 

    public function profile_patient()
    {  
        return view('patient.profile.index');
    } 

    public function profile_edit_patient()
    {  
        return view('patient.profile.edit');
    } 

    public function retos_patient()
    {  
        return view('patient.retos.index');
    } 

    public function cuestionario_patient()
    {  
        return view('patient.cuestionario.index');
    }

    public function cuestionario2_patient()
    {  
        return view('patient.cuestionario.index_2');
    }

    public function noticias_patient()
    {  
        return view('patient.noticias.index');
    }

    public function blog_patient()
    {  
        return view('patient.blog.index');
    }
}
