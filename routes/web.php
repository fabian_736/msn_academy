<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\RutasController;
use App\Http\Controllers\ProfessionalController;
use App\Http\Controllers\PatientController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('login',[RutasController::class,'form_login'])->name('login.form_login');
Route::get('forgot',[RutasController::class,'forgot'])->name('login.forgot');
Route::get('reset',[RutasController::class,'reset'])->name('login.reset');
Route::get('newpass',[RutasController::class,'newpass'])->name('login.newpass');

Route::get('patient/tutorial/index',[PatientController::class,'tutorial_patient'])->name('tutorial_patient.index');
Route::get('patient/index',[PatientController::class,'portal_patient'])->name('portal_patient.index');
Route::get('patient/perfil',[PatientController::class,'profile_patient'])->name('profile_patient.index');
Route::get('patient/perfil/edit',[PatientController::class,'profile_edit_patient'])->name('profile_patient.edit');
Route::get('patient/retos',[PatientController::class,'retos_patient'])->name('retos_patient.index');
Route::get('patient/cuestionario',[PatientController::class,'cuestionario_patient'])->name('cuestionario_patient.index');
Route::get('patient/cuestionario/2',[PatientController::class,'cuestionario2_patient'])->name('cuestionario2_patient.index');
Route::get('patient/noticias',[PatientController::class,'noticias_patient'])->name('noticias_patient.index');
Route::get('patient/blog',[PatientController::class,'blog_patient'])->name('blog_patient.index');


{/* PROFESIONAL */}

{/* INICIO */}
Route::get('professional/tutorial/index',[ProfessionalController::class,'tutorial_professional'])->name('tutorial_professional.index');
Route::get('professional/index',[ProfessionalController::class,'portal_professional'])->name('portal_professional.index');


{/* PERFIL */}
Route::get('professional/perfil',[ProfessionalController::class,'perfil_professional'])->name('perfil_professional.index');
Route::get('professional/perfil/edit',[ProfessionalController::class,'perfil_edit_professional'])->name('perfil_professional.edit');
Route::get('professional/perfil/certificado',[ProfessionalController::class,'perfil_certificado_professional'])->name('certificado_profesional.edit');


{/* ENTRENAMIENTOS */}
Route::get('professional/practice/index',[ProfessionalController::class,'practice_professional'])->name('practice_profesional.index');


{/* CUESTIONARIO */}
Route::get('professional/cuestionario/index',[ProfessionalController::class,'cuestionario_professional'])->name('cuestionario_profesional.index');


{/* INSIGNIA */}
Route::get('professional/insignia/index',[ProfessionalController::class,'insignia_professional'])->name('insignia_profesional.index');


{/* MURO */}
Route::get('professional/muro/index',[ProfessionalController::class,'muro_professional'])->name('muro_profesional.index');


{/* PREMIOS */}
Route::get('professional/premio/index',[ProfessionalController::class,'premio_professional'])->name('premio_profesional.index');
Route::get('professional/premio/select',[ProfessionalController::class,'premioselect_professional'])->name('premio_profesional.select');
