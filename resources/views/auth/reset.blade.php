@extends('layouts.auth.app')
@section('content')



    <div class="container p-0 m-0">

        <!-- Outer Row -->
        <div class="row">

            <div class="col-xl-10 col-lg-12 col-md-12 ">
 
                <div class="o-hidden border-0 my-5" id="cardlogin">
                        <!-- Nested Row within Card Body -->
                        <div class="d-flex flex-row-reverse">
                            <div class="col-lg-6 shadow-lg mx-auto bg-white" >
                                <div class="p-5" style="margin-top: 30px; margin-bottom: 30px;">
                                    <div class="text-center ">
                                        <img src="{{ url('img/logo/1.png') }}" class="w-100" alt=""
                                            style="margin-bottom: 50px;">
                                    </div>
                                
                                    <div class="form-group">
                                            <input type="email"
                                                class="form-control form-control-user"
                                                id="exampleInputEmail" aria-describedby="emailHelp"
                                                value="{{ old('email') }}" autocomplete="email" autofocus
                                                placeholder="Codigo de confirmacion" name="email" required>

                                          
                                        </div>
                                        <a  value="Continuar" href="{{route('login.newpass')}}" class="btn btn-user btn-block mt-5" style="border-radius: 40px; color: white;  background-color: #312783; font-weight: bold">Continuar</a>
                                       
                                
                                    <div class="text-center">
                                    

                                        <a class="btn-icon-split text-white" href="{{ url()->previous() }}">
                                            <span class="icon text-white-50">
                                                <i class="fas fa-key" style="color: #312783"></i>
                                            </span>
                                            <span style="color: #312783; font-weight: bold">Enviar nuevamente el codigo de confirmacion</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
            </div>

        </div>

    </div>

   

    <style>::placeholder {
        color: gray !important;
      }
      
      
      @media (min-width: 500px){
        #cardlogin{
            margin-left: 10px;
            margin-right: 10px;
        }
      }</style>



@endsection
