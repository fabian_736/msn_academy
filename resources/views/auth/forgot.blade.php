@extends('layouts.auth.app')
@section('content')



    <div class="container p-0 m-0">

        <!-- Outer Row -->
        <div class="row ">

            <div class="col-xl-10 col-lg-12 col-md-12 ">

                <div class="o-hidden border-0 my-5" id="cardlogin">
                    <!-- Nested Row within Card Body -->
                    <div class="d-flex flex-row-reverse">
                        <div class="col-lg-6 shadow-lg mx-auto bg-white">
                            <div class="p-5" style="margin-top: 30px; margin-bottom: 30px;">
                                <div class="text-center ">
                                    <img src="{{ url('img/logo/1.png') }}" class="w-100" alt=""
                                    style="margin-bottom: 50px;">
                                </div>
                                <div class="row mx-auto text-center my-3">
                                    <div class="col">
                                        <label for=""
                                            style="text-decoration: underline #312783; color: #312783; font-weight: bold">Recibirá
                                            el código de confirmación al siguiente correo electrónico</label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <input type="email" class="form-control form-control-user" id="exampleInputEmail"
                                        aria-describedby="emailHelp" value="{{ old('email') }}" autocomplete="email"
                                        autofocus placeholder="Correo electronico" name="email">


                                </div>
                                <button onclick="alert()" class="btn btn-user btn-block mt-5"
                                    style="border-radius: 40px; color: white;  background-color: #312783; font-weight: bold">
                                    Enviar Codigo
                                </button>

                                <div class="text-center">


                                    <a class="btn-icon-split text-white" href="{{ url()->previous() }}">
                                        <span class="icon text-white-50">
                                            <i class="fas fa-key" style="color: #312783"></i>
                                        </span>
                                        <span style="color: #312783; font-weight: bold">Recuperar mi contraseña</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>



    <style>
        ::placeholder {
            color: gray !important;
        }

        @media (min-width: 500px){
        #cardlogin{
            margin-left: 10px;
            margin-right: 10px;
        }
      }

    </style>


    <script>
        function alert() {
            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-success',
                    cancelButton: 'btn btn-danger'
                },
                buttonsStyling: false
            })

            swalWithBootstrapButtons.fire({
                title: 'Estas seguro de enviar el codigo?',
                text: "Una vez enviado el codigo, caducara en los proximos 15 minutos",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Si, enviar codigo!',
                cancelButtonText: 'No, cancelar!',
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {
                    swalWithBootstrapButtons.fire(
                        'Enviado!',
                        'Tu codigo ha sido enviado.',
                        'success'
                    ).then(function() {
                        window.location = "{{route('login.reset')}}";
                    });
                } else if (
                    /* Read more about handling dismissals below */
                    result.dismiss === Swal.DismissReason.cancel
                ) {
                    swalWithBootstrapButtons.fire(
                        'Cancelado',
                        'No se ha enviado el codigo',
                        'error'
                    )
                }
            })
        }

       
    </script>


@endsection
