@extends('layouts.auth.app')
@section('content')



    <div class="container p-0 m-0">

        <!-- Outer Row -->
        <div class="row">

            <div class="col-xl-10 col-lg-12 col-md-12 ">

                <div class="o-hidden border-0 my-5" id="cardlogin">
                    <!-- Nested Row within Card Body -->
                    <div class="d-flex flex-row-reverse">
                        <div class="col-lg-6 shadow-lg mx-auto bg-white">
                            <div class="p-5" style="margin-top: 30px; margin-bottom: 30px;">
                                <div class="text-center ">
                                    <img src="{{ url('img/logo/1.png') }}" class="w-100" alt=""
                                            style="margin-bottom: 50px;">
                                </div>

                                    <div class="form-row">
                                        <div class="col">
                                            <input type="password" class="form-control" placeholder="Nueva contraseña"
                                                name="" id="">
                                        </div>
                                    </div>
                                    <div class="form-row my-3">
                                        <div class="col">
                                            <input type="password" class="form-control"
                                                placeholder="Confirmar nueva contraseña" name="" id="">
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col d-flex justify-content-end">
                                        <i class="fas fa-info-circle" data-container="body"
                                                data-toggle="popover" data-placement="right"
                                                data-content="Debe ser alfanúmerica, minimo una mayúscula, una minúscula y un caracter especial"></i>
                                        </div>
                                    </div>
                                    <a href="{{route('login.form_login')}}" class="btn btn-user btn-block mt-5"
                                        style="border-radius: 40px; color: white;  background-color: #312783; font-weight: bold"
                                        >Continuar</a>

                             
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>



    <style>
        ::placeholder {
            color: gray !important;
        }

        @media (min-width: 500px){
        #cardlogin{
            margin-left: 10px;
            margin-right: 10px;
        }
      }

    </style>


    <script>
        function init() {
            var x = document.getElementById("primero");
            var y = document.getElementById("segundo");
            x.style.display = "block";
            y.style.display = "none";
        }

        function myFunctionB1() {
            var x = document.getElementById("primero");
            var y = document.getElementById("segundo");
            if (x.style.display === "none") {
                x.style.display = "block";
                y.style.display = "none";
            } else {
                x.style.display = "none";
                y.style.display = "none";
            }

        }

        function myFunctionB2() {
            var x = document.getElementById("primero");
            var y = document.getElementById("segundo");
            if (y.style.display === "none") {
                y.style.display = "block";
                x.style.display = "none";
            } else {
                x.style.display = "none";
                y.style.display = "none";
            }
        }

        init();
    </script>


@endsection
