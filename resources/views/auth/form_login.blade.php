@extends('layouts.auth.app')
@section('content')



    <div class="container p-0 m-0">

        <!-- Outer Row -->
        <div class="row ">

            <div class="col-xl-10 col-lg-12 col-md-12 ">

                <div class="o-hidden border-0 my-5" id="cardlogin">
                        <!-- Nested Row within Card Body -->
                        <div class="d-flex flex-row-reverse">
                            <div class="col-lg-6 shadow-lg mx-auto bg-white rounded" >
                                <div class="p-5" style="margin-top: 40px; margin-bottom: 40px;">
                                    <div class="text-center ">
                                        <img src="{{ url('img/logo/1.png') }}" class="w-100" alt=""
                                            style="margin-bottom: 50px;">
                                    </div>
                                    <div class="row mx-auto text-center my-3">
                                        <!--
                                        <div class="col">
                                            <i class="fas fa-stethoscope active"></i>
                                            <label for="" onclick="myFunctionB1()" style="cursor: pointer; text-decoration: underline #312783; color: #312783; font-weight: bold; font-size: 15px">PROFESIONALES</label>
                                        </div>
                                    -->
                                        <div class="col">
                                            <i class="far fa-head-side-medical"></i>
                                            <label for="" onclick="myFunctionB2()" style="cursor: pointer; text-decoration: underline #312783; color: #312783; font-weight: bold; font-size: 15px">PACIENTES</label>
                                        </div>
                                    </div>
                                  
                                    <form class="user" action="" id="primero">
                                        
                                        <!--
                                        <div class="row my-3">
                                            <div class="col text-center">
                                                <label for="" style="color: #312783; font-weight: bold">PACIENTES</label>
                                            </div>
                                        </div>    

                                    -->
                                    <div class="form-group">
                                            <input type="email"
                                                class="form-control form-control-user"
                                                id="exampleInputEmail" aria-describedby="emailHelp"
                                                value="{{ old('email') }}" autocomplete="email" autofocus
                                                placeholder="Correo electronico" name="email" required>

                                          
                                        </div>
                                        <div class="form-group">
                                            <input type="password"
                                                class="form-control form-control-user "
                                                id="exampleInputPassword" placeholder="Contraseña" name="password" required
                                                autocomplete="current-password">

                              
                                        </div>
                                        <a href="{{route('tutorial_patient.index')}}"
                                            class="btn btn-user btn-block mt-5"
                                            style="border-radius: 40px; color: white;  background-color: #312783; font-weight: bold">
                                            Iniciar Sesion
                                        </a>
                                    </form>
                                    <form class="user" action="" id="segundo" style="display: none;">
                                        <div class="row my-3">
                                            <div class="col text-center">
                                                <label for="" style="color: #312783; font-weight: bold">PACIENTES</label>
                                            </div>
                                        </div>  

                                        <div class="form-group">
                                            <input type="email"
                                                class="form-control form-control-user"
                                                id="exampleInputEmail" aria-describedby="emailHelp"
                                                value="{{ old('email') }}" autocomplete="email" autofocus
                                                placeholder="Correo electronico" name="email" required>

                                          
                                        </div>
                                        <div class="form-group">
                                            <input type="password"
                                                class="form-control form-control-user "
                                                id="exampleInputPassword" placeholder="Contraseña" name="password" required
                                                autocomplete="current-password">

                              
                                        </div>
                                        <a href="{{route('tutorial_patient.index')}}"
                                            class="btn btn-user btn-block mt-5"
                                            style="border-radius: 40px; color: white;  background-color: #312783; font-weight: bold;">
                                            Iniciar Sesion
                                        </a>
                                    </form>
                                    <div class="text-center">
                                        <a class="btn-icon-split text-white" href="{{route('login.forgot')}}">
                                            <span class="icon">
                                                <i class="fas fa-key" style="color: #312783"></i>
                                            </span>
                                            <span style="color: #312783; font-weight: bold">Recuperar mi contraseña</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
            </div>

        </div>

    </div>

    <style>::placeholder {
        color: gray !important;
      }
      
      
      @media (min-width: 500px){
        #cardlogin{
            margin-left: 10px;
            margin-right: 10px;
        }
      }
      
      
      
      
      </style>


    <script>

    function init() {
    var x = document.getElementById("primero");
    var y = document.getElementById("segundo");
    x.style.display = "block";
    y.style.display = "none";
    }

    function myFunctionB1() {
    var x = document.getElementById("primero");
    var y = document.getElementById("segundo");
    if (x.style.display === "none") {
    x.style.display = "block";
    y.style.display = "none";
    } else {
    x.style.display = "none";
    y.style.display = "none";
    }

    }

    function myFunctionB2() {
    var x = document.getElementById("primero");
    var y = document.getElementById("segundo");
    if (y.style.display === "none") {
    y.style.display = "block";
    x.style.display = "none";
    } else {
    x.style.display = "none";
    y.style.display = "none";
    }
    }

    init();


    </script>


@endsection
