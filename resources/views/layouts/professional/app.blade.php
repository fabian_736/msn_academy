<!-- HEAD (HTML) -->

@include('layouts.professional.components.head');

<!-- FIN HEAD (HTML) -->

<!-- NAVBAR -->

@include('layouts.professional.components.navbar');

<!-- FIN NAVBAR -->

<div class="content">
  <div class="content">
    <div class="row">

      <!-- BASE PRINCIPAL (SIDEBAR-LEFT - CONTENIDO - SIDEBAR-RIGHT) -->

      <div class="col-md-1" style="padding-left: 0 !important; padding-right: 0 !important;">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              @include('layouts.professional.components.left');
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-8" style="padding-left: 0 !important; padding-right: 0 !important;">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              <div class="card-transparent">
                @yield('content')
              </div>
            </div>
          </div>

        </div>
      </div>
      <div class="col-md-3" style="padding-left: 0 !important; padding-right: 0 !important;">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              @include('layouts.professional.components.right');
            </div>
          </div>
        </div>
      </div>

      <!-- FIN BASE PRINCIPAL (SIDEBAR-LEFT - CONTENIDO - SIDEBAR-RIGHT) -->

    </div>
  </div>

</div>

<!-- FOOTER -->

@include('layouts.professional.components.footer');

<!-- FIN FOOTER -->



<!-- END (HTML) -->

@include('layouts.professional.components.end');

<!-- FIN END (HTML) -->