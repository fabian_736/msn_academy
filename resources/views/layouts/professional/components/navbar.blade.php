<nav class="navbar navbar-expand-lg navbar-transparent" style="padding-left: 5%; padding-right: 5%">
    <a class="navbar-brand" href="{{route('portal_professional.index')}}">
        <img src="{{url('img/logo_login.png')}}" class="d-inline-block align-top img-responsive " alt="">
    </a>

    <div class="collapse navbar-collapse mt-5" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto ml-5">
            <li class="nav-item active">
                <form class="navbar-form">
                    <div class="input-group no-border">
                        <input type="text" value="" class="form-control" placeholder="Search...">
                        <button type="submit" class="btn btn-white btn-round btn-just-icon">
                            <i class="material-icons">search</i>
                            <div class="ripple-container"></div>
                        </button>
                    </div>
                </form>
            </li>
        </ul>
        <div class="fileinput fileinput-new text-center col-md-4" data-provides="fileinput" >
            <div class="fileinput-new thumbnail img-circle img-raised p-1 bg-white">
                <a href="{{route('perfil_professional.index')}}"><img src="{{ url('img/user.png') }}" style="border-radius: 60px" rel="nofollow" alt="..."></a>
            </div>
        </div>
        <label for="" class="col-md-7 text-right font-weight-bold" style="left: 85%;color: #053F72; width: 12%; position: absolute; border-bottom-style: solid; border-color: #053F72;">2000
            Puntos</label>
    </div>
</nav>

