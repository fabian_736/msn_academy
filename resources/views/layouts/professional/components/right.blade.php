<div class="card-transparent py-3">
    <div class="form-reverse mx-auto">
        <a href="{{route('insignia_profesional.index')}}">
            <div class="col">

                <div class="bg-dark mx-auto d-flex align-items-center" style="background:linear-gradient(90deg, rgba(56,185,195,1) 35%, rgba(245,215,142,1) 100%);;
            border-radius: 80px; width: 130px; height: 130px;">
                    <i class="fas fa-medal text-white mx-auto" style="font-size: 75px"></i>
                </div>
            </div>
            <div class="col d-flex justify-content-center mt-2">
                <label for="" class="">INSIGNIAS</label>
            </div>
        </a>
    </div>

    <div class="form-reverse mx-auto my-3">
        <a href="{{route('premio_profesional.index')}}">
        <div class="col">
            <div class="bg-dark mx-auto d-flex align-items-center" style="background: linear-gradient(90deg, rgba(56,185,195,1) 35%, rgba(245,215,142,1) 100%);
            border-radius: 80px; width: 130px; height: 130px;">
                <i class="fas fa-trophy text-white mx-auto" style="font-size: 75px"></i>
            </div>
        </div>
        <div class="col d-flex justify-content-center mt-2">
            <label for="" class="">PREMIOS</label>
        </div>
    </a>
    </div>

    <div class="form-reverse mx-auto">
        <a href="{{route('muro_profesional.index')}}">
            <div class="col">
                <div class="bg-dark mx-auto d-flex align-items-center" style="background: linear-gradient(90deg, rgba(56,185,195,1) 35%, rgba(245,215,142,1) 100%);
            border-radius: 80px; width: 130px; height: 130px;">
                    <i class="fas fa-book text-white mx-auto" style="font-size: 75px"></i>
                </div>
            </div>
            <div class="col d-flex justify-content-center mt-2">
                <label for="" class="">MURO</label>
            </div>
        </a>
    </div>
</div>