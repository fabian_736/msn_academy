</div>
  </div>
  <!--   Core JS Files   -->
  <script src="{{url('js/app.js')}}"></script>
  <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

  
<!-- ESTE SCRIPT ES PARA EL MURO -->



<script>
  function readURL(input) {
   if (input.files && input.files[0]) { //Revisamos que el input tenga contenido
     var reader = new FileReader(); //Leemos el contenido
     
     reader.onload = function(e) { //Al cargar el contenido lo pasamos como atributo de la imagen de arriba
       $('#blah').attr('src', e.target.result);
     }
     
     reader.readAsDataURL(input.files[0]);
   }
 }
 
 $("#file-upload").change(function() { //Cuando el input cambie (se cargue un nuevo archivo) se va a ejecutar de nuevo el cambio de imagen y se verá reflejado.
   readURL(this);
 });
 </script>

<script>
  //EL COMENTARIO DEL USUARIO Y LA CONSTRUCCION DEL CAJON
  function userSend(text) {
    var img = ' <div class="fileinput fileinput-new text-center " data-provides="fileinput">'
                        +'<div class="fileinput-new thumbnail img-circle img-raised p-1 bg-white" style="width:40px; height:40px">'
                           +'<a href="{{route("perfil_professional.index")}}"><img src="{{ url("img/user2.png") }}" style="border-radius: 60px; width: 100%; height: 100%" rel="nofollow" alt="..."></a>'
                        +'</div>'
                    +'</div>';
    $('#chat_converse').append('<div class="row my-3">'+
      '<div class="col-md-1 d-flex justify-content-center">'
      + img + 
      '</div>' +
      '<div class="col-md-8 px-3 py-2" style="background: #F5F5F5; border-radius: 20px">'
      + text + '<br /><br /><img id="blah" width="150" height="100" src="#" alt="Imagen" style="display: none; border-radius: 20px" />'+
      '</div>' +
    '</div>'
    );
    $('#chatSend').val('');
    if ($('.chat_converse').height() >= 256) {
      $('.chat_converse').addClass('is-max');
    }
    $('.chat_converse').scrollTop($('.chat_converse')[0].scrollHeight);
  }


  //ENVIA EL MENSAJE Y RETORNAMOS EL KEY PARA VISUALIZARLO EN EL CAJON
  $('#chatSend').bind("enterChat", function(e) {
    userSend($('#chatSend').val());
    adminSend('How may I help you.');
  });
  $('#fab_send').bind("enterChat", function(e) {
    userSend($('#chatSend').val());
    adminSend('How may I help you.');
  });
  $('#chatSend').keypress(function(event) {
    if (event.keyCode === 13) {
      event.preventDefault();
      if (jQuery.trim($('#chatSend').val()) !== '') {
        $(this).trigger("enterChat");
      }
    }
  });

  $('#fab_send').click(function(e) {
    if (jQuery.trim($('#chatSend').val()) !== '') {
      $(this).trigger("enterChat");
    }
  });
</script>

<!-- FIN DEL SCRIPT PARA EL MURO -->


  <script>
    $(document).ready(function() {
      $().ready(function() {
        $sidebar = $('.sidebar');

        $sidebar_img_container = $sidebar.find('.sidebar-background');

        $full_page = $('.full-page');

        $sidebar_responsive = $('body > .navbar-collapse');

        window_width = $(window).width();

        fixed_plugin_open = $('.sidebar .sidebar-wrapper .nav li.active a p').html();

        if (window_width > 767 && fixed_plugin_open == 'Dashboard') {
          if ($('.fixed-plugin .dropdown').hasClass('show-dropdown')) {
            $('.fixed-plugin .dropdown').addClass('open');
          }

        }

        $('.fixed-plugin a').click(function(event) {
          // Alex if we click on switch, stop propagation of the event, so the dropdown will not be hide, otherwise we set the  section active
          if ($(this).hasClass('switch-trigger')) {
            if (event.stopPropagation) {
              event.stopPropagation();
            } else if (window.event) {
              window.event.cancelBubble = true;
            }
          }
        });

        $('.fixed-plugin .active-color span').click(function() {
          $full_page_background = $('.full-page-background');

          $(this).siblings().removeClass('active');
          $(this).addClass('active');

          var new_color = $(this).data('color');

          if ($sidebar.length != 0) {
            $sidebar.attr('data-color', new_color);
          }

          if ($full_page.length != 0) {
            $full_page.attr('filter-color', new_color);
          }

          if ($sidebar_responsive.length != 0) {
            $sidebar_responsive.attr('data-color', new_color);
          }
        });

        $('.fixed-plugin .background-color .badge').click(function() {
          $(this).siblings().removeClass('active');
          $(this).addClass('active');

          var new_color = $(this).data('background-color');

          if ($sidebar.length != 0) {
            $sidebar.attr('data-background-color', new_color);
          }
        });

        $('.fixed-plugin .img-holder').click(function() {
          $full_page_background = $('.full-page-background');

          $(this).parent('li').siblings().removeClass('active');
          $(this).parent('li').addClass('active');


          var new_image = $(this).find("img").attr('src');

          if ($sidebar_img_container.length != 0 && $('.switch-sidebar-image input:checked').length != 0) {
            $sidebar_img_container.fadeOut('fast', function() {
              $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
              $sidebar_img_container.fadeIn('fast');
            });
          }

          if ($full_page_background.length != 0 && $('.switch-sidebar-image input:checked').length != 0) {
            var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');

            $full_page_background.fadeOut('fast', function() {
              $full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
              $full_page_background.fadeIn('fast');
            });
          }

          if ($('.switch-sidebar-image input:checked').length == 0) {
            var new_image = $('.fixed-plugin li.active .img-holder').find("img").attr('src');
            var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');

            $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
            $full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
          }

          if ($sidebar_responsive.length != 0) {
            $sidebar_responsive.css('background-image', 'url("' + new_image + '")');
          }
        });

        $('.switch-sidebar-image input').change(function() {
          $full_page_background = $('.full-page-background');

          $input = $(this);

          if ($input.is(':checked')) {
            if ($sidebar_img_container.length != 0) {
              $sidebar_img_container.fadeIn('fast');
              $sidebar.attr('data-image', '#');
            }

            if ($full_page_background.length != 0) {
              $full_page_background.fadeIn('fast');
              $full_page.attr('data-image', '#');
            }

            background_image = true;
          } else {
            if ($sidebar_img_container.length != 0) {
              $sidebar.removeAttr('data-image');
              $sidebar_img_container.fadeOut('fast');
            }

            if ($full_page_background.length != 0) {
              $full_page.removeAttr('data-image', '#');
              $full_page_background.fadeOut('fast');
            }

            background_image = false;
          }
        });

        $('.switch-sidebar-mini input').change(function() {
          $body = $('body');

          $input = $(this);

          if (md.misc.sidebar_mini_active == true) {
            $('body').removeClass('sidebar-mini');
            md.misc.sidebar_mini_active = false;

            $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar();

          } else {

            $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar('destroy');

            setTimeout(function() {
              $('body').addClass('sidebar-mini');

              md.misc.sidebar_mini_active = true;
            }, 300);
          }

          // we simulate the window Resize so the charts will get updated in realtime.
          var simulateWindowResize = setInterval(function() {
            window.dispatchEvent(new Event('resize'));
          }, 180);

          // we stop the simulation of Window Resize after the animations are completed
          setTimeout(function() {
            clearInterval(simulateWindowResize);
          }, 1000);

        });
      });
    });
  </script>
  <script>
    $(document).ready(function() {
      // Javascript method's body can be found in assets/js/demos.js
      md.initDashboardPageCharts();

      md.initVectorMap();

    });
  </script>
  <style>
    body,
    html {
      height: 100%;
      background-repeat: no-repeat;
      background: url("<?php echo url('/img/home.png') ?>") no-repeat center center fixed;
      background-size: 100% 100%;
    }

    .navbar-brand {
      padding: 0px !important;
    }

    .navbar-brand>img {
      height: 100px !important;
      width: 90px !important;
    }
  </style>
</body>

</html>