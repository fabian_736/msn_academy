<!-- HEAD (HTML) -->

@include('layouts.patient.components.head');

<!-- FIN HEAD (HTML) -->

<!-- NAVBAR -->

@include('layouts.patient.components.navbar');

<!-- FIN NAVBAR -->

<div class="content">
  <div class="content">
    <div class="row">

      <!-- BASE PRINCIPAL (SIDEBAR-LEFT - CONTENIDO) -->

      <div class="col-md-1" id="left" style="padding-left: 0 !important; padding-right: 0 !important;">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              @include('layouts.patient.components.left');
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-10 mx-auto" style="padding-left: 0 !important; padding-right: 0 !important;">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              <div class="card-transparent">
                @yield('content')
              </div>
            </div>
          </div>

        </div>
      </div>


      <!-- FIN BASE PRINCIPAL (SIDEBAR-LEFT - CONTENIDO - SIDEBAR-RIGHT) -->

    </div>
  </div>

</div>

<!-- FOOTER -->

@include('layouts.patient.components.footer');

<!-- FIN FOOTER -->



<!-- END (HTML) -->

@include('layouts.patient.components.end');

<!-- FIN END (HTML) -->



<style>
  @media (max-width: 500px) {
    #left{
        display: none;
    }
}
</style>