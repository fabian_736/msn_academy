<div class="card-transparent py-3">
    <div class="form-reverse mx-auto">
        <a href="{{route('retos_patient.index')}}">
            <div class="col">

                <div class="bg-dark mx-auto d-flex align-items-center" style="background: linear-gradient(90deg, rgba(49,39,131,1) 0%, rgba(227,6,19,1) 100%);
            border-radius: 80px; width: 130px; height: 130px;">
                    <i class="fas fa-heartbeat text-white mx-auto" style="font-size: 75px"></i>
                </div>
            </div>
            <div class="col d-flex justify-content-center mt-2">
                <label for="" class="">MIS CURSOS</label>
            </div>
        </a>
    </div>

    <div class="form-reverse mx-auto my-3">
        <a href="{{route('noticias_patient.index')}}">
        <div class="col">
            <div class="bg-dark mx-auto d-flex align-items-center" style="background: linear-gradient(90deg, rgba(49,39,131,1) 0%, rgba(227,6,19,1) 100%);
            border-radius: 80px; width: 130px; height: 130px;">
                <i class="fas fa-file-invoice text-white mx-auto" style="font-size: 75px"></i>
            </div>
        </div>
        <div class="col d-flex justify-content-center mt-2">
            <label for="" class="">NOTICIAS</label>
        </div>
    </a>
    </div>

    <div class="form-reverse mx-auto">
        <a href="{{route('blog_patient.index')}}">
            <div class="col">
                <div class="bg-dark mx-auto d-flex align-items-center" style="background: linear-gradient(90deg, rgba(49,39,131,1) 0%, rgba(227,6,19,1) 100%);
            border-radius: 80px; width: 130px; height: 130px;">
                    <i class="fas fa-book text-white mx-auto" style="font-size: 75px"></i>
                </div>
            </div>
            <div class="col d-flex justify-content-center mt-2">
                <label for="" class="">BLOG</label>
            </div>
        </a>
    </div>
</div>