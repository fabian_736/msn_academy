<div class="card-transparent py-5">
    <div class="row">
        <div class="col d-flex justify-content-end mr-3">
            <i class="fas fa-bell fa-lg" style="cursor: pointer" data-toggle="modal" data-target="#ModalPush"></i>
        </div>
    </div>
    <!-- 
    <div class="row my-4">
        <div class="col d-flex justify-content-end mr-3">
            <i class="fab fa-instagram fa-lg"></i>
        </div>
    </div>
    <div class="row">
        <div class="col d-flex justify-content-end mr-3">
            <i class="fab fa-facebook fa-lg"></i>
        </div>
    </div>
    <div class="row my-4">
        <div class="col d-flex justify-content-end mr-3">
            <i class="fab fa-whatsapp fa-lg"></i>
        </div>
    </div>
    
-->
    <div class="fixed-plugin">
        <div class="dropdown show-dropdown">
            <a href="#" data-toggle="dropdown" aria-expanded="false">
                <i class="fas fa-palette fa-2x p-2 text-white"></i>
            </a>
            <ul class="dropdown-menu py-3" x-placement="top-start"
                style="position: absolute; top: -110px; left: -231px; will-change: top, left;">
                <li class="header-title">Escoge un color de fondo:</li>
                <li class="adjustments-line">
                    <a href="javascript:void(0)" class="switch-trigger active-color">
                        <div class="badge-colors ml-auto mr-auto">
                            <span class="badge filter" style="background-color: #F2F2F2" onclick="day()"></span>
                            <span class="badge filter" style="background-color: #D9306E;" onclick="pink()"></span>
                            <span class="badge filter" style="background-color: #A7DDF2;" onclick="blue()"></span>
                            <span class="badge filter" style="background-color: #F2AE30;" onclick="yellow()"></span>
                            <span class="badge filter" style="background-color: #F20322;" onclick="red()"></span>
                            <span class="badge filter" style="background-color: #F24F13;" onclick="orange()"></span>
                        </div>
                    </a>
                </li>
                <li class="adjustments-line">
                    <a href="javascript:void(0)" class="switch-trigger active-color">
                        <div class="badge-colors ml-auto mr-auto">
                            <span class="badge filter" style="background-color: #262626;" onclick="dark()"></span>
                            <span class="badge filter" style="background-color: #F4CFE5;" onclick="pinklight()"></span>
                            <span class="badge filter" style="background-color: #B0DAD5;" onclick="bluelight()"></span>
                            <span class="badge filter" style="background-color: #AED2D6;" onclick="bluelight2()"></span>
                            <span class="badge filter" style="background-color: #F5E59A;"
                                onclick="yellowlight()"></span>
                            <span class="badge filter" style="background-color: #F16866;" onclick="redlight()"></span>
                        </div>
                    </a>
                </li>
            </ul>
        </div>
    </div>

    <div class="row  d-flex justify-content-end mr-1" style="margin-top: 120px">
        <label for="" style="writing-mode: vertical-lr;
transform: rotate(180deg);">COLOMBIA</label>
    </div>
</div>



<div class="modal fade" id="ModalPush" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title font-weight-bold" id="exampleModalLabel">Mis notificaciones</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row-reverse">
                    <div class="col card p-2">
                        <div class="row-reverse">
                            <div class="col">
                                <div class="row">
                                    <div class="col">
                                        <span>2021-10-05</span>
                                    </div>
                                    <div class="col d-flex justify-content-end align-items-center">
                                        <i class="fas fa-trash-alt text-danger"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="col">
                                <label for="" class="font-weight-bold">Hay una nueva noticia disponible!</label>
                            </div>
                            <div class="col d-flex justify-content-end">
                                <label for="" style="color: #69B26E">Ver más</label>
                            </div>
                        </div>
                    </div>
                    <div class="col card p-2">
                        <div class="row-reverse">
                            <div class="col">
                                <div class="row">
                                    <div class="col">
                                        <span>2021-11-25</span>
                                    </div>
                                    <div class="col d-flex justify-content-end align-items-center">
                                        <i class="fas fa-trash-alt text-danger"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="col">
                                <label for="" class="font-weight-bold">Carlos ramirez ha publicado en el blog!</label>
                            </div>
                            <div class="col d-flex justify-content-end">
                                <label for="" style="color: #69B26E">Ver más</label>
                            </div>
                        </div>
                    </div>
                    <div class="col card p-2">
                        <div class="row-reverse">
                            <div class="col">
                                <div class="row">
                                    <div class="col">
                                        <span>2021-10-05</span>
                                    </div>
                                    <div class="col d-flex justify-content-end align-items-center">
                                        <i class="fas fa-trash-alt text-danger"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="col">
                                <label for="" class="font-weight-bold">Tienes un reto pendiente en MI PATOLOGIA!</label>
                            </div>
                            <div class="col d-flex justify-content-end">
                                <label for="" style="color: #69B26E">Ver más</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col d-flex justify-content-center">
                            <a href="javascript:;" style="text-decoration-line: underline; color: #053F72;">Ver todas las notificaciones</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    miStorage = window.localStorage;

    var estado = localStorage.getItem('estado');

    if (estado == 1)
        day();
    else if (estado == 0)
        dark();
    else if (estado == 3)
        pink();
    else if (estado == 4)
        blue();
    else if (estado == 5)
        yellow();
    else if (estado == 6)
        red();
    else if (estado == 7)
        orange();
    else if (estado == 8)
        pinklight();
    else if (estado == 9)
        bluelight();
    else if (estado == 10)
        bluelight2();
    else if (estado == 11)
        yellowlight();
    else if (estado == 12)
        redlight();

    function dark() {
        localStorage.setItem('estado', 0);
        if (document.body.style === "background: #262626 !important;") {
            // disable dark mode
        } else {
            document.body.style = "background: #262626 !important;";
            Array.from(document.getElementsByTagName("a")).forEach(e => {

                e.style.setProperty("color", "white", "important");

            });
            Array.from(document.getElementsByTagName("nav")).forEach(e => {

                e.style.setProperty("background-color", "#262626", "important");

            });
            document.getElementsByTagName("h1")[0].style.color = "white !important"
            document.getElementsByTagName("img")[0].style = "box-shadow: 0px 4px 6px 2px #404040;"

        }
    }

    function day() {
        localStorage.setItem('estado', 1);
        localStorage.removeItem('estado', 1);
return window.location.reload(); 
    }

    function pink() {
        localStorage.setItem('estado', 3);
        if (document.body.style === "background: #D9306E !important;") {
            // disable dark mode
        } else {
            document.body.style = "background: #D9306E !important;"
            Array.from(document.getElementsByTagName("a")).forEach(e => {

                e.style.setProperty("color", "white", "important");

            });
            Array.from(document.getElementsByTagName("nav")).forEach(e => {

                e.style.setProperty("background-color", "#D9306E", "important");

            });
            document.getElementsByTagName("h1")[0].style.color = "white !important"
            document.getElementsByTagName("img")[0].style = "box-shadow: 0px 4px 6px 2px #404040;"
        }
    }

    function blue() {
        localStorage.setItem('estado', 4);
        if (document.body.style === "background: #A7DDF2 !important;") {
            // disable dark mode
        } else {
            document.body.style = "background: #A7DDF2 !important;"
            Array.from(document.getElementsByTagName("a")).forEach(e => {

                e.style.setProperty("color", "dark", "important");

            });
            Array.from(document.getElementsByTagName("nav")).forEach(e => {

                e.style.setProperty("background-color", "#A7DDF2", "important");

            });
            document.getElementsByTagName("h1")[0].style.color = "white !important"
            document.getElementsByTagName("img")[0].style = "box-shadow: 0px 4px 6px 2px #404040;"
        }
    }

    function yellow() {
        localStorage.setItem('estado', 5);
        if (document.body.style === "background: #F2AE30 !important;") {
            // disable dark mode
        } else {
            document.body.style = "background: #F2AE30 !important;"
            Array.from(document.getElementsByTagName("a")).forEach(e => {

                e.style.setProperty("color", "white", "important");

            });
            Array.from(document.getElementsByTagName("nav")).forEach(e => {

                e.style.setProperty("background-color", "#F2AE30", "important");

            });
            document.getElementsByTagName("h1")[0].style.color = "white !important"
            document.getElementsByTagName("img")[0].style = "box-shadow: 0px 4px 6px 2px #404040;"
        }
    }

    function red() {
        localStorage.setItem('estado', 6);
        if (document.body.style === "background: #F20322 !important;") {
            // disable dark mode
        } else {
            document.body.style = "background: #F20322 !important;"
            Array.from(document.getElementsByTagName("a")).forEach(e => {

                e.style.setProperty("color", "white", "important");

            });
            Array.from(document.getElementsByTagName("nav")).forEach(e => {

                e.style.setProperty("background-color", "#F20322", "important");

            });
            document.getElementsByTagName("h1")[0].style.color = "white !important"
            document.getElementsByTagName("img")[0].style = "box-shadow: 0px 4px 6px 2px #404040;"
        }
    }

    function orange() {
        localStorage.setItem('estado', 7);
        if (document.body.style === "background: #F24F13 !important;") {
            // disable dark mode
        } else {
            document.body.style = "background: #F24F13 !important;"
            Array.from(document.getElementsByTagName("a")).forEach(e => {

                e.style.setProperty("color", "white", "important");

            });
            Array.from(document.getElementsByTagName("nav")).forEach(e => {

                e.style.setProperty("background-color", "#F24F13", "important");

            });
            document.getElementsByTagName("h1")[0].style.color = "white !important"
            document.getElementsByTagName("img")[0].style = "box-shadow: 0px 4px 6px 2px #404040;"
        }
    }

    function pinklight() {
        localStorage.setItem('estado', 8);
        if (document.body.style === "background: #F4CFE5 !important;") {
            // disable dark mode
        } else {
            document.body.style = "background: #F4CFE5 !important;"
            Array.from(document.getElementsByTagName("a")).forEach(e => {

                e.style.setProperty("color", "white", "important");

            });
            Array.from(document.getElementsByTagName("nav")).forEach(e => {

                e.style.setProperty("background-color", "#F4CFE5", "important");

            });
            document.getElementsByTagName("h1")[0].style.color = "black !important"
            document.getElementsByTagName("img")[0].style = "box-shadow: 0px 4px 6px 2px #404040;"
        }
    }

    function bluelight() {
        localStorage.setItem('estado', 9);
        if (document.body.style === "background: #B0DAD5 !important;") {
            // disable dark mode
        } else {
            document.body.style = "background: #B0DAD5 !important;"
            Array.from(document.getElementsByTagName("a")).forEach(e => {

                e.style.setProperty("color", "white", "important");

            });
            Array.from(document.getElementsByTagName("nav")).forEach(e => {

                e.style.setProperty("background-color", "#B0DAD5", "important");

            });
            document.getElementsByTagName("h1")[0].style.color = "black !important"
            document.getElementsByTagName("img")[0].style = "box-shadow: 0px 4px 6px 2px #404040;"
        }
    }

    function bluelight2() {
        localStorage.setItem('estado', 10);
        if (document.body.style === "background: #AED2D6 !important;") {
            // disable dark mode
        } else {
            document.body.style = "background: #A3D2D6 !important;"
            Array.from(document.getElementsByTagName("a")).forEach(e => {

                e.style.setProperty("color", "white", "important");

            });
            Array.from(document.getElementsByTagName("nav")).forEach(e => {

                e.style.setProperty("background-color", "#A3D2D6", "important");

            });
            document.getElementsByTagName("h1")[0].style.color = "white !important"
            document.getElementsByTagName("img")[0].style = "box-shadow: 0px 4px 6px 2px #404040;"
        }
    }

    function yellowlight() {
        localStorage.setItem('estado', 11);
        if (document.body.style === "background: #F5E59A !important;") {
            // disable dark mode
        } else {
            document.body.style = "background: #F5E59A !important;"
            Array.from(document.getElementsByTagName("a")).forEach(e => {

                e.style.setProperty("color", "white", "important");

            });
            Array.from(document.getElementsByTagName("nav")).forEach(e => {

                e.style.setProperty("background-color", "#F5E59A", "important");

            });
            document.getElementsByTagName("h1")[0].style.color = "white !important"
            document.getElementsByTagName("img")[0].style = "box-shadow: 0px 4px 6px 2px #404040;"
        }
    }

    function redlight() {
        localStorage.setItem('estado', 12);
        if (document.body.style === "background: #F16866 !important;") {
            // disable dark mode
        } else {
            document.body.style = "background: #F16866 !important;"
            Array.from(document.getElementsByTagName("a")).forEach(e => {

                e.style.setProperty("color", "white", "important");

            });
            Array.from(document.getElementsByTagName("nav")).forEach(e => {

                e.style.setProperty("background-color", "#F16866", "important");

            });
            document.getElementsByTagName("h1")[0].style.color = "white !important"
            document.getElementsByTagName("img")[0].style = "box-shadow: 0px 4px 6px 2px #404040;"
        }
    }
</script>

