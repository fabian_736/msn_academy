<nav class="navbar navbar-transparent" style="padding-left: 5%; padding-right: 5%">

    
        <div class="col">
            <img src="{{url('img/logo/1.png')}}" style="min-width: 35%; max-width: 35%; cursor: pointer" alt="" onclick="click_logo()" >
        </div>
        <div class="col d-flex justify-content-end ">
            <form class="navbar-form ">
                <div class="input-group no-border">
                    <input type="text" value="" class="form-control pr-5" placeholder="Search...">
                    <button type="submit" class="btn btn-white btn-round btn-just-icon">
                        <i class="material-icons">search</i>
                        <div class="ripple-container"></div>
                    </button>
                </div>
            </form>
        </div>
    

        <script>
            function click_logo(){
                window.location.href='{{route("portal_patient.index")}}';

            }
            
        </script>

   

</nav>