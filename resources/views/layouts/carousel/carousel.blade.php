<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.1.3/assets/owl.carousel.min.css">
    <title>MSN</title>
</head>
<style>
    .parrafo {
        color: white !important;
    }

    .shadow-effect {
        background: #02718B;
        padding: 20px;
        border-radius: 100px;
        height: 400px;
        width: 200px;
        text-align: center;
        border: 1px solid #ECECEC;
        box-shadow: 0 19px 38px rgba(0, 0, 0, 0.10), 0 15px 12px rgba(0, 0, 0, 0.02);
    }

    #customers-testimonials .shadow-effect p {
        font-family: inherit;
        font-size: 17px;
        line-height: 1.5;
        margin: 0 0 17px 0;
        font-weight: 300;
    }

    .testimonial-name {
        margin: -17px auto 0;
        display: table;
        width: auto;
        background: #3190E7;
        padding: 9px 35px;
        border-radius: 12px;
        text-align: center;
        color: #fff;
        box-shadow: 0 9px 18px rgba(0, 0, 0, 0.12), 0 5px 7px rgba(0, 0, 0, 0.05);
    }

    #customers-testimonials .item {
        text-align: center;
        padding: 50px;
        margin-bottom: 80px;
        opacity: .2;
        -webkit-transform: scale3d(0.8, 0.8, 1);
        transform: scale3d(0.8, 0.8, 1);
        -webkit-transition: all 0.3s ease-in-out;
        -moz-transition: all 0.3s ease-in-out;
        transition: all 0.3s ease-in-out;
    }

    #customers-testimonials .owl-item.active.center .item {
        opacity: 1;
        -webkit-transform: scale3d(1.0, 1.0, 1);
        transform: scale3d(1.0, 1.0, 1);
    }

    .owl-carousel .owl-item img {
        transform-style: preserve-3d;
        max-width: 90px;
        margin: 0 auto 17px;
    }

    #customers-testimonials.owl-carousel .owl-dots .owl-dot.active span,
    #customers-testimonials.owl-carousel .owl-dots .owl-dot:hover span {
        background: #3190E7;
        transform: translate3d(0px, -50%, 0px) scale(0.7);
    }

    #customers-testimonials.owl-carousel .owl-dots {
        display: inline-block;
        width: 100%;
        text-align: center;
    }

    #customers-testimonials.owl-carousel .owl-dots .owl-dot {
        display: inline-block;
    }

    #customers-testimonials.owl-carousel .owl-dots .owl-dot span {
        background: #3190E7;
        display: inline-block;
        height: 20px;
        margin: 0 2px 5px;
        transform: translate3d(0px, -50%, 0px) scale(0.3);
        transform-origin: 50% 50% 0;
        transition: all 250ms ease-out 0s;
        width: 20px;
    }

    @media (min-width: 500px){
        
        #carrousel{
            margin-left: 50px;
        }
       
    }

</style>

<body>


    <section id="carrousel" class="testimonials ">
        <div class="container">

            <div class="row">
                <div class="col-sm-12">
                    <div id="customers-testimonials" class="owl-carousel">

                        <!--TESTIMONIAL 1 -->
                        
                        <div class="item">
                            <div class="shadow-effect">
                                <img class="img-circle" src="./../img/logo_login.png" alt="">
                                <p class="parrafo">Text de prueba en carrousel
                                </p>
                            </div>

                        </div>
                        <!--END OF TESTIMONIAL 1 -->
                        <!--TESTIMONIAL 2 -->
                        <div class="item">
                            <div class="shadow-effect">
                                <img class="img-circle" src="./../img/logo_login.png" alt="">
                                <p class="parrafo">Text de prueba en carrousel
                                </p>
                            </div>

                        </div>
                        <!--END OF TESTIMONIAL 2 -->
                        <!--TESTIMONIAL 3 -->
                        <div class="item">
                            <div class="shadow-effect">
                                <img class="img-circle" src="./../img/logo_login.png" alt="">
                                <p class="parrafo">Text de prueba en carrousel
                                </p>
                            </div>

                        </div>
                        <!--END OF TESTIMONIAL 3 -->
                    </div>
                </div>
            </div>
        </div>
    </section>


</body>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.1.3/owl.carousel.min.js"></script>
<script src="https://use.fontawesome.com/826a7e3dce.js"></script>
<script>
    jQuery(document).ready(function($) {
        "use strict";
        //  TESTIMONIALS CAROUSEL HOOK
        $('#customers-testimonials').owlCarousel({
            loop: true,
            center: true,
            items: 3,
            margin: 0,
            autoplay: true,
            dots: true,
            autoplayTimeout: 3000,
            smartSpeed: 450,
            responsive: {
                0: {
                    items: 1
                },
                768: {
                    items: 2
                },
                1170: {
                    items: 3
                }
            }
        });
    });
</script>

</html>
