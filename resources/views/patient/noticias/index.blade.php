@extends('layouts.patient.app')
@section('content')

    <div class="row p-3">
        <div class="col">
            <label for="" class="font-weight-bold h2" style="color: #312783">Noticias</label>
        </div>
    </div>
    <div class="row">
        <div class="col-3 d-flex justify-content-end mt-auto" style="width: 100%; max-height: 800px">
            <div class="row-reverse ">
                <div class="col">
                    <label for="" class="lead" onclick="recetas()" id="titlerecetas" style="color: #312783; cursor: pointer">Recetas</label>
                </div>
                <div class="col">
                    <label for="" class="lead"  onclick="moda()" id="titlemoda" style="color: #312783; cursor: pointer">Tips de Moda</label>
                </div> 
                <div class="col">
                    <label for="" class="lead" onclick="salud()" id="titlesalud" style="color: #312783; cursor: pointer">Salud</label>
                </div>
            </div>
        </div>
        <div class="col-9 p-3 m-0 card" id="cardcontainer" style="width: 100%; max-height: 800px; overflow-y: scroll;">
            <div id="recetas">
                <div class="row-reverse">
                    <div class="col p-0">
                        <div class="card m-0 mb-3">
                            <div class="row">
                                <div class="col" id="colimg">
                                    <img src="https://s1.1zoom.me/b5050/219/Fast_food_Pizza_Tomatoes_Wood_planks_562796_1920x1080.jpg"
                                        alt="" width="100%" height="100%">
                                </div>
                                <div class="col p-2">
                                    <div class="row-reverse">
                                        <div class="col">
                                            <label for="" style="color: #312783;" class="lead font-weight-bold">Receta #
                                                1</label>
                                        </div>
                                        <div class="col">
                                            <label for="">Lorem ipsum dolor sit amet, consec tetuer adipiscing elit. Lorem ipsum
                                                dolor sit amet.</label>
                                        </div>
                                        <div class="col">
                                            <label for="" class="text-dark">Desayuno</label>
                                        </div>
                                        <div class="row m-0 d-flex align-items-center">
                                            <div class="col">
                                                <i class="far fa-thumbs-up" style="font-size: 20px"></i>
                                            </div>
                                            <div class="col">
                                                <a class="btn font-weight-bold" data-toggle="modal" data-target="#exampleModal"
                                                    style="background: #E30613; color: #ffffff">VER
                                                    MÁS</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row-reverse">
                    <div class="col p-0">
                        <div class="card m-0 mb-3">
                            <div class="row">
                                <div class="col" id="colimg">
                                    <img src="https://s1.1zoom.me/b5251/210/Fast_food_Butterbrot_Bread_Tomatoes_Cucumbers_526208_1920x1080.jpg"
                                        alt="" width="100%" height="100%">
                                </div>
                                <div class="col p-2">
                                    <div class="row-reverse">
                                        <div class="col">
                                            <label for="" style="color: #312783;" class="lead font-weight-bold">Receta #
                                                2</label>
                                        </div>
                                        <div class="col">
                                            <label for="">Lorem ipsum dolor sit amet, consec tetuer adipiscing elit. Lorem ipsum
                                                dolor sit amet.</label>
                                        </div>
                                        <div class="col">
                                            <label for="" class="text-dark">Cena romantica</label>
                                        </div>
                                        <div class="row m-0 d-flex align-items-center">
                                            <div class="col">
                                                <i class="far fa-thumbs-up" style="font-size: 20px"></i>
                                            </div>
                                            <div class="col">
                                                <a class="btn font-weight-bold" style="background: #E30613; color: #ffffff">VER
                                                    MÁS</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row-reverse">
                    <div class="col p-0">
                        <div class="card m-0 mb-3">
                            <div class="row">
                                <div class="col" id="colimg">
                                    <img src="https://images3.alphacoders.com/807/thumb-1920-807704.jpg" alt="" width="100%"
                                        height="100%">
                                </div>
                                <div class="col p-2">
                                    <div class="row-reverse">
                                        <div class="col">
                                            <label for="" style="color: #312783;" class="lead font-weight-bold">Receta #
                                                3</label>
                                        </div>
                                        <div class="col">
                                            <label for="">Lorem ipsum dolor sit amet, consec tetuer adipiscing elit. Lorem ipsum
                                                dolor sit amet.</label>
                                        </div>
                                        <div class="col">
                                            <label for="" class="text-dark">Almuerzo saludable</label>
                                        </div>
                                        <div class="row m-0 d-flex align-items-center">
                                            <div class="col">
                                                <i class="far fa-thumbs-up" style="font-size: 20px"></i>
                                            </div>
                                            <div class="col">
                                                <a class="btn font-weight-bold" style="background: #E30613; color: #ffffff">VER
                                                    MÁS</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="moda" style="display: none">
                <div class="row-reverse">
                    <div class="col p-0">
                        <div class="card m-0 mb-3">
                            <div class="row">
                                <div class="col" id="colimg">
                                    <img src="https://fondosmil.com/fondo/31788.jpg"
                                        alt="" width="100%" height="100%">
                                </div>
                                <div class="col p-2">
                                    <div class="row-reverse">
                                        <div class="col">
                                            <label for="" style="color: #312783;" class="lead font-weight-bold">Receta #
                                                1</label>
                                        </div>
                                        <div class="col">
                                            <label for="">Lorem ipsum dolor sit amet, consec tetuer adipiscing elit. Lorem ipsum
                                                dolor sit amet.</label>
                                        </div>
                                        <div class="col">
                                            <label for="" class="text-dark">Desayuno</label>
                                        </div>
                                        <div class="row m-0 d-flex align-items-center">
                                            <div class="col">
                                                <i class="far fa-thumbs-up" style="font-size: 20px"></i>
                                            </div>
                                            <div class="col">
                                                <a class="btn font-weight-bold" data-toggle="modal" data-target="#exampleModal"
                                                    style="background: #E30613; color: #ffffff">VER
                                                    MÁS</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row-reverse">
                    <div class="col p-0">
                        <div class="card m-0 mb-3">
                            <div class="row">
                                <div class="col" id="colimg">
                                    <img src="https://www.wallpapertip.com/wmimgs/29-293385_sc-stores-hd-pictures-for-shopping-website.jpg"
                                        alt="" width="100%" height="100%">
                                </div>
                                <div class="col p-2">
                                    <div class="row-reverse">
                                        <div class="col">
                                            <label for="" style="color: #312783;" class="lead font-weight-bold">Receta #
                                                2</label>
                                        </div>
                                        <div class="col">
                                            <label for="">Lorem ipsum dolor sit amet, consec tetuer adipiscing elit. Lorem ipsum
                                                dolor sit amet.</label>
                                        </div>
                                        <div class="col">
                                            <label for="" class="text-dark">Cena romantica</label>
                                        </div>
                                        <div class="row m-0 d-flex align-items-center">
                                            <div class="col">
                                                <i class="far fa-thumbs-up" style="font-size: 20px"></i>
                                            </div>
                                            <div class="col">
                                                <a class="btn font-weight-bold" style="background: #E30613; color: #ffffff">VER
                                                    MÁS</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row-reverse">
                    <div class="col p-0">
                        <div class="card m-0 mb-3">
                            <div class="row">
                                <div class="col" id="colimg">
                                    <img src="https://www.wallpapertip.com/wmimgs/70-700481_fashion-tips-women-fashion-full-hd.jpg" alt="" width="100%"
                                        height="100%">
                                </div>
                                <div class="col p-2">
                                    <div class="row-reverse">
                                        <div class="col">
                                            <label for="" style="color: #312783;" class="lead font-weight-bold">Receta #
                                                3</label>
                                        </div>
                                        <div class="col">
                                            <label for="">Lorem ipsum dolor sit amet, consec tetuer adipiscing elit. Lorem ipsum
                                                dolor sit amet.</label>
                                        </div>
                                        <div class="col">
                                            <label for="" class="text-dark">Almuerzo saludable</label>
                                        </div>
                                        <div class="row m-0 d-flex align-items-center">
                                            <div class="col">
                                                <i class="far fa-thumbs-up" style="font-size: 20px"></i>
                                            </div>
                                            <div class="col">
                                                <a class="btn font-weight-bold" style="background: #E30613; color: #ffffff">VER
                                                    MÁS</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="salud" style="display: none">
                <div class="row-reverse">
                    <div class="col p-0">
                        <div class="card m-0 mb-3">
                            <div class="row">
                                <div class="col" id="colimg">
                                    <img src="https://fondosmil.com/fondo/56933.jpg"
                                        alt="" width="100%" height="100%">
                                </div>
                                <div class="col p-2">
                                    <div class="row-reverse">
                                        <div class="col">
                                            <label for="" style="color: #312783;" class="lead font-weight-bold">Receta #
                                                1</label>
                                        </div>
                                        <div class="col">
                                            <label for="">Lorem ipsum dolor sit amet, consec tetuer adipiscing elit. Lorem ipsum
                                                dolor sit amet.</label>
                                        </div>
                                        <div class="col">
                                            <label for="" class="text-dark">Desayuno</label>
                                        </div>
                                        <div class="row m-0 d-flex align-items-center">
                                            <div class="col">
                                                <i class="far fa-thumbs-up" style="font-size: 20px"></i>
                                            </div>
                                            <div class="col">
                                                <a class="btn font-weight-bold" data-toggle="modal" data-target="#exampleModal"
                                                    style="background: #E30613; color: #ffffff">VER
                                                    MÁS</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row-reverse">
                    <div class="col p-0">
                        <div class="card m-0 mb-3">
                            <div class="row">
                                <div class="col" id="colimg">
                                    <img src="https://www.wallpapertip.com/wmimgs/11-119907_health-care-images-hd.jpg"
                                        alt="" width="100%" height="100%">
                                </div>
                                <div class="col p-2">
                                    <div class="row-reverse">
                                        <div class="col">
                                            <label for="" style="color: #312783;" class="lead font-weight-bold">Receta #
                                                2</label>
                                        </div>
                                        <div class="col">
                                            <label for="">Lorem ipsum dolor sit amet, consec tetuer adipiscing elit. Lorem ipsum
                                                dolor sit amet.</label>
                                        </div>
                                        <div class="col">
                                            <label for="" class="text-dark">Cena romantica</label>
                                        </div>
                                        <div class="row m-0 d-flex align-items-center">
                                            <div class="col">
                                                <i class="far fa-thumbs-up" style="font-size: 20px"></i>
                                            </div>
                                            <div class="col">
                                                <a class="btn font-weight-bold" style="background: #E30613; color: #ffffff">VER
                                                    MÁS</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row-reverse">
                    <div class="col p-0">
                        <div class="card m-0 mb-3">
                            <div class="row">
                                <div class="col" id="colimg">
                                    <img src="https://fondosmil.com/fondo/57047.jpg" alt="" width="100%"
                                        height="100%">
                                </div>
                                <div class="col p-2">
                                    <div class="row-reverse">
                                        <div class="col">
                                            <label for="" style="color: #312783;" class="lead font-weight-bold">Receta #
                                                3</label>
                                        </div>
                                        <div class="col">
                                            <label for="">Lorem ipsum dolor sit amet, consec tetuer adipiscing elit. Lorem ipsum
                                                dolor sit amet.</label>
                                        </div>
                                        <div class="col">
                                            <label for="" class="text-dark">Almuerzo saludable</label>
                                        </div>
                                        <div class="row m-0 d-flex align-items-center">
                                            <div class="col">
                                                <i class="far fa-thumbs-up" style="font-size: 20px"></i>
                                            </div>
                                            <div class="col">
                                                <a class="btn font-weight-bold" style="background: #E30613; color: #ffffff">VER
                                                    MÁS</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col">
                            <div onclick="windows()"
                                style="cursor: pointer; border-radius: 110px; width: 210px; height: 210px; border-width: 1px; border-style: solid; border-color: linear-gradient(90deg, rgba(49,39,131,1) 0%, rgba(227,6,19,1) 100%)"
                                class="d-flex justify-content-center align-items-center ">
                                <div style="width: 200px; height: 200px; border-radius: 100px; background: linear-gradient(90deg, rgba(49,39,131,1) 0%, rgba(227,6,19,1) 100%)"
                                    class="d-flex justify-content-center align-items-center">
                                    <i class="fas fa-play" style="font-size: 40px; color: white"></i>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="row-reverse">
                                <div class="col">
                                    <label for="" class="h3 font-weight-bold" style="color:#312783">RECETA 1</label>
                                </div>
                                <div class="col">
                                    <label for="">INGREDIENTES</label>
                                </div>
                                <div class="col p-0">
                                    <ul>
                                        <li>Lorem ipsum dolor sit amet</li>
                                        <li>Lorem ipsum dolor sit amet</li>
                                        <li>Lorem ipsum dolor sit amet</li>
                                        <li>Lorem ipsum dolor sit amet</li>
                                        <li>Lorem ipsum dolor sit amet</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <style>
        @media only screen and (max-width: 750px) and (min-width: 321px) {
            #cardcontainer {
                max-height: 200px !important;
            }

            #colimg {
                display: none;
            }
        }

        #cardcontainer::-webkit-scrollbar {
            width: 8px;
            /* Tamaño del scroll en vertical */
            height: 8px;
            /* Tamaño del scroll en horizontal */
        }

        #cardcontainer::-webkit-scrollbar-thumb {
            background: #ccc;
            border-radius: 4px;
        }

        /* Cambiamos el fondo y agregamos una sombra cuando esté en hover */
        #cardcontainer::-webkit-scrollbar-thumb:hover {
            background: #b3b3b3;
            box-shadow: 0 0 2px 1px rgba(0, 0, 0, 0.2);
        }

        /* Cambiamos el fondo cuando esté en active */
        #cardcontainer::-webkit-scrollbar-thumb:active {
            background-color: #999999;
        }

        #cardcontainer::-webkit-scrollbar-track {
            background: #e1e1e1;
            border-radius: 4px;
        }

        /* Cambiamos el fondo cuando esté en active o hover */
        #cardcontainer::-webkit-scrollbar-track:hover,
        #cardcontainer::-webkit-scrollbar-track:active {
            background: #d4d4d4;
        }

    </style>

    <script>
        function windows() {
            window.open("https://www.proturbiomarspa.com/files/_pdf-prueba.pdf", "ventana1",
                "toolbar=yes,scrollbars=yes,resizable=yes,top=500,left=500,width=4000,height=4000")
        }

        function recetas(){
            $('#recetas').css({
                'display' : 'block'
            })
            $('#moda').css({
                'display':'none'
            })
            $('#salud').css({
                'display':'none'
            })
            $('#titlerecetas').css({
                'font-weight':'bold'
            })
            $('#titlemoda').css({
                'font-weight':'normal'
            })
            $('#titlesalud').css({
                'font-weight':'normal'
            })
        }

        function moda(){
            $('#recetas').css({
                'display' : 'none'
            })
            $('#moda').css({
                'display':'block'
            })
            $('#salud').css({
                'display':'none'
            })
            $('#titlerecetas').css({
                'font-weight':'normal'
            })
            $('#titlemoda').css({
                'font-weight':'bold'
            })
            $('#titlesalud').css({
                'font-weight':'normal'
            })
        }

        function salud(){
            $('#salud').css({
                'display' : 'block'
            })
            $('#recetas').css({
                'display':'none'
            })
            $('#moda').css({
                'display':'none'
            })
            $('#titlerecetas').css({
                'font-weight':'normal'
            })
            $('#titlemoda').css({
                'font-weight':'normal'
            })
            $('#titlesalud').css({
                'font-weight':'bold'
            })
        }
    </script>
@endsection
