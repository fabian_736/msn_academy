@extends('layouts.patient.app2')
@section('content')

<div class="row ">
    <div class="row-reverse">
        <div class="col">
            <label for="" class="h2" style="color: #312783"><b>Cuestionario |</b> Curso 1 </label>
        </div>
        <div class="col">
            <label for="" class="h4" style="color: #312783">Bienvenido a la prueba <b>Ladrones de la felicidad</b> </label>
        </div>
    </div>
</div>


<div class="row my-5 ">

    <!-- POSICIONES -->
    <div class="col-md-1 p-0 m-0">
        <div class="row-reverse">
            <div class="col">
                <div class="col-md-1">
                    <div class="d-flex justify-content-center align-items-center" style="width: 40px; height: 40px; border-radius: 60px; background: linear-gradient(90deg, rgba(49,39,131,1) 0%, rgba(227,6,19,1) 100%);">
                        <a href="" class="text-white font-weight-bold" id="one">1</a>
                        <i class="fas fa-check" id="ico1" style="display: none"></i>
                    </div>
                </div>
            </div>
            <div class="col my-3">
                <div class="col-md-1">
                    <div class="d-flex justify-content-center align-items-center" id="divtwo" style="background: #C1C1C1; width: 40px; height: 40px; border-radius: 60px; ">
                        <a href="" class="text-white font-weight-bold" id="two">2</a>
                        <i class="fas fa-check" id="ico2" style="display: none"></i>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="col-md-1">
                    <div class="d-flex justify-content-center align-items-center" id="divthree" style="background: #C1C1C1; width: 40px; height: 40px; border-radius: 60px; ">
                        <a href="" class="text-white font-weight-bold" id="three">3</a>
                        <i class="fas fa-check" id="ico3" style="display: none"></i>
                    </div>
                </div>
            </div>

            <div class="col my-3">
                <div class="col-md-1">
                    <div class="d-flex justify-content-center align-items-center" id="divfour" style="background: #C1C1C1; width: 40px; height: 40px; border-radius: 60px; ">
                        <a href="" class="text-white font-weight-bold" id="four">4</a>
                        <i class="fas fa-check" id="ico4" style="display: none"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col p-0" id="item1">
        <div class="row-reverse" >
            <div class="col p-0  my-3">
                <label for="" class="h4">¿Cuales de las siguientes imágenes crees que representan mejor lo que podemos denominar ladrones de la felicidad?</label>
            </div>
            <div class="row mx-auto mb-2">
                <div class="col-md-12 p-0">
                    <div class="row">
                        <div class="col-md-2">
                            <div class="card bg-dark">
                                <a href="javascript:;" class="img" onclick="itemselect1()">
                                    <div class="card-body p-0 m-0 d-flex justify-content-center" style="height: 300px; background-image: url('/cuestionario/cuestionario_2/1.jpeg'); background-size: 100% 100%; border-style: solid; border-color: #312783; opacity: 0.7">
                                        <div class="mt-5" style="position: absolute; display: none" id="check1">
                                            <i class="far fa-check-circle text-success" style="font-size: 100px"></i>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="card bg-dark">
                                <a href="javascript:;" class="img" onclick="itemselect2()">
                                    <div class="card-body p-0 m-0 bg-white d-flex justify-content-center" style="height: 300px; background-image: url('/cuestionario/cuestionario_2/2.jpeg'); background-size: 100% 100%; border-style: solid; border-color: #312783; opacity: 0.7">
                                        <div class="mt-5" style="position: absolute; display: none" id="check2">
                                            <i class="far fa-check-circle text-success" style="font-size: 100px"></i>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="card bg-dark">
                                <a href="javascript:;" class="img" onclick="itemselect3()">
                                    <div class="card-body p-0 m-0 bg-white d-flex justify-content-center" style="height: 300px; background-image: url('/cuestionario/cuestionario_2/3.jpeg'); background-size: 100% 100%; border-style: solid; border-color: #312783; opacity: 0.7">
                                        <div class="mt-5" style="position: absolute; display: none" id="check3">
                                            <i class="far fa-check-circle text-success" style="font-size: 100px"></i>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="card bg-dark">
                                <a href="javascript:;" class="img" onclick="itemselect4()">
                                    <div class="card-body p-0 m-0 bg-white d-flex justify-content-center" style="height: 300px; background-image: url('/cuestionario/cuestionario_2/4.jpeg'); background-size: 100% 100%; border-style: solid; border-color: #312783; opacity: 0.7">
                                        <div class="mt-5" style="position: absolute; display: none" id="check4">
                                            <i class="far fa-check-circle text-success" style="font-size: 100px"></i>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="card bg-dark">
                                <a href="javascript:;" class="img" onclick="itemselect5()">
                                    <div class="card-body p-0 m-0 bg-white d-flex justify-content-center" style="height: 300px; background-image: url('/cuestionario/cuestionario_2/5.jpeg'); background-size: 100% 100%; border-style: solid; border-color: #312783; opacity: 0.7">
                                        <div class="mt-5" style="position: absolute; display: none" id="check5">
                                            <i class="far fa-check-circle text-success" style="font-size: 100px"></i>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-10">
                            <a class="btn form-control" onclick="itemall()" style="color: #fff; background: #312783">TODAS LAS ANTERIORES</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col p-0" id="item2" style="display: none;">
        <div class="row-reverse" >
            <div class="col p-0  my-3">
                <label for="" class="h4">Partiendo del hecho de que compararse hace parte de la naturaleza humana, ¿cuales de las siguientes crees que son comparaciones que sí aportan a la felicidad?</label>
            </div>
            <div class="row mx-auto mb-2">
                <div class="col-md-12 p-0">
                    <div class="row mx-auto p-0">
                        <div class="col-md-1 p-0 mt-3 d-flex justify-content-center align-items-center">
                            <div class="list-group fileinput fileinput-new text-center m-0" data-provides="fileinput">
                                <div class="fileinput-new thumbnail img-circle img-raised p-1 d-flex justify-content-center align-items-center"
                                    id="itemDiv" style="width: 40px; height: 40px;">
                                    <a href="javascript:;" onclick="clickOn()"
                                        class="list-group-item font-weight-bold">a</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-10 p-0 mt-3">
                            <div class="row-reverse p-0 m-0 ">
                                <div class="col m-0 p-0">
                                    <label for="" class="h5 font-weight-bold"
                                        style="float: left; color: #38B9C3">Conocer otras experiencias de vida e identificar otras posibilidades de resolver situaciones complicadas</label>
                                </div>
                                <div class="col m-0 p-0">
                                    <hr class="m-0 p-0"
                                        style="width: 100%; height: 2px; border-style: solid; border-color: #38B9C3; float: left">
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="row mx-auto mb-2">
                <div class="col-md-12 p-0">
                    <div class="row mx-auto p-0">
                        <div class="col-md-1 p-0 mt-3 d-flex justify-content-center align-items-center">
                            <div class="fileinput fileinput-new text-center m-0" data-provides="fileinput">
                                <div class="list-group fileinput-new thumbnail img-circle img-raised p-1 d-flex justify-content-center align-items-center"
                                    id="itemDiv" style="width: 40px; height: 40px;">
                                    <a href="javascript:;" onclick="clickOn()"
                                        class="list-group-item font-weight-bold">b</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-10 p-0 mt-3">
                            <div class="row-reverse p-0 m-0 ">
                                <div class="col m-0 p-0">
                                    <label for="" class="h5 font-weight-bold" style="float: left; color: #38B9C3">Sentirme mal por comparaciones como: “su vida es perfecta; siempre se le ve tan bien en las fotos, es tan feliz y su vida está llena de éxito” </label>
                                </div>
                                <div class="col m-0 p-0">
                                    <hr class="m-0 p-0"
                                        style="width: 100%; height: 2px; border-style: solid; border-color: #38B9C3; float: left">
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="row mx-auto mb-2">
                <div class="col-md-12 p-0">
                    <div class="row mx-auto p-0">
                        <div class="col-md-1 p-0 mt-3 d-flex justify-content-center align-items-center">
                            <div class="fileinput fileinput-new text-center m-0" data-provides="fileinput">
                                <div class="list-group fileinput-new thumbnail img-circle img-raised p-1 d-flex justify-content-center align-items-center"
                                    id="itemDiv" style="width: 40px; height: 40px;">
                                    <a href="javascript:;" onclick="clickOn()"
                                        class="list-group-item font-weight-bold">c</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-10 p-0 mt-3">
                            <div class="row-reverse p-0 m-0 ">
                                <div class="col m-0 p-0">
                                    <label for="" class="h5 font-weight-bold" style="float: left; color: #38B9C3">Por contraste, reconocer todo por lo cual estas agradecido, todo lo bueno que tienes y que te permite estar estable y continuar a diferencia de otros. </label>
                                </div>
                                <div class="col m-0 p-0">
                                    <hr class="m-0 p-0"
                                        style="width: 100%; height: 2px; border-style: solid; border-color: #38B9C3; float: left">
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="row mx-auto mb-2">
                <div class="col-md-12 p-0">
                    <div class="row mx-auto p-0">
                        <div class="col-md-1 p-0 mt-3 d-flex justify-content-center align-items-center">
                            <div class="fileinput fileinput-new text-center m-0" data-provides="fileinput">
                                <div class="list-group fileinput-new thumbnail img-circle img-raised p-1 d-flex justify-content-center align-items-center"
                                    style="width: 40px; height: 40px;">
                                    <a href="javascript:;" class="list-group-item font-weight-bold">d</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-10 p-0 mt-3">
                            <div class="row-reverse p-0 m-0 ">
                                <div class="col m-0 p-0">
                                    <label for="" class="h5 font-weight-bold" style="float: left; color: #38B9C3">Conocer a otros me permite adquirir una información más completa sobre su vida, con lo cual, puedo reconocer y empatizar fácilmente con una persona real y no imaginar y sobredimensionar sus éxitos y alegrías al tiempo que subvaloro los míos. </label>
                                </div>
                                <div class="col m-0 p-0">
                                    <hr class="m-0 p-0"
                                        style="width: 100%; height: 2px; border-style: solid; border-color: #38B9C3; float: left">
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

     <div class="col p-0" id="item3" style="display: none;">
        <div class="row-reverse">
            <div class="col p-0">
                <label for="" class="h4 font-weight-bold">Al crear un plan para ser feliz necesitas elegir cuidadosamente cuáles hábitos y propósitos te pondrás como meta. En la siguiente tabla elige una opción de cada lado, marcando con una X los 5 hábitos y propósitos que consideras incluirías en ese plan:</label>
            </div>
            <div class="row my-5 d-flex justify-content-center align-items-center">
                <div class="col-1">
                    <label for="" class="h3">A.</label>
                </div>
                <div class="col">
                    <div class="row-reverse">
                        <div class="col d-flex justify-content-center align-items-center">
                            <img src="{{url('cuestionario/6.jpg')}}" alt="" class="w-50 mb-3">
                        </div>
                        <div class="col d-flex justify-content-center align-items-center">
                            <input type="checkbox" name="" id="">
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="row-reverse">
                        <div class="col d-flex justify-content-center align-items-center">
                            <img src="{{url('cuestionario/7.jpg')}}" alt="" class="w-50 mb-3">
                        </div>
                        <div class="col d-flex justify-content-center align-items-center">
                            <input type="checkbox" name="" id="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row my-5 d-flex justify-content-center align-items-center">
                <div class="col-1">
                    <label for="" class="h3">B.</label>
                </div>
                <div class="col">
                    <div class="row-reverse">
                        <div class="col d-flex justify-content-center align-items-center">
                            <label for="" class="text-dark">Crear un plan de alimentación que se adecúe a mis necesidades de salud, las recomendaciones de mis especialistas y a mis posibilidades. </label>
                        </div>
                        <div class="col d-flex justify-content-center align-items-center">
                            <input type="checkbox" name="" id="">
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="row-reverse">
                        <div class="col d-flex justify-content-center align-items-center">
                            <label for="" class="text-dark">Comer todo lo que me gusta, sin importar nada y a la hora que quiera. Merezco consentirme para ser feliz. </label>
                        </div>
                        <div class="col d-flex justify-content-center align-items-center">
                            <input type="checkbox" name="" id="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row my-5 d-flex justify-content-center align-items-center">
                <div class="col-1">
                    <label for="" class="h3">C.</label>
                </div>
                <div class="col">
                    <div class="row-reverse">
                        <div class="col d-flex justify-content-center align-items-center">
                            <label for="" class="text-dark">Aunque esté sintiendo tristeza o enojo, debo disimularlo lo mejor posible, pues es más importante mostrarme alegre y positivo siempre. </label>
                        </div>
                        <div class="col d-flex justify-content-center align-items-center">
                            <input type="checkbox" name="" id="">
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="row-reverse">
                        <div class="col d-flex justify-content-center align-items-center">
                            <label for="" class="text-dark">Enfrentar mis problemas y obstáculos acogiendo mis emociones, para encontrar un bienestar real que contemple en su totalidad lo que se tiene, lo que no, aceptando las vicisitudes de la vida y encontrando así un sentido de la vida. </label>
                        </div>
                        <div class="col d-flex justify-content-center align-items-center">
                            <input type="checkbox" name="" id="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row my-5 d-flex justify-content-center align-items-center">
                <div class="col-1">
                    <label for="" class="h3">D.</label>
                </div>
                <div class="col">
                    <div class="row-reverse">
                        <div class="col d-flex justify-content-center align-items-center">
                            <img src="{{url('cuestionario/8.jpg')}}" alt="" class="w-50 mb-3">
                        </div>
                        <div class="col d-flex justify-content-center align-items-center">
                            <input type="checkbox" name="" id="">
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="row-reverse">
                        <div class="col d-flex justify-content-center align-items-center">
                            <img src="{{url('cuestionario/9.jpg')}}" alt="" class="w-50 mb-3">
                        </div>
                        <div class="col d-flex justify-content-center align-items-center">
                            <input type="checkbox" name="" id="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row my-5 d-flex justify-content-center align-items-center">
                <div class="col-1">
                    <label for="" class="h3">E.</label>
                </div>
                <div class="col">
                    <div class="row-reverse">
                        <div class="col d-flex justify-content-center align-items-center">
                            <label for="" class="text-dark">Encerrarme en casa sólo a pensar en mis problemas y no comentarlos con nadie, pues nadie me entiende. </label>
                        </div>
                        <div class="col d-flex justify-content-center align-items-center">
                            <input type="checkbox" name="" id="">
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="row-reverse">
                        <div class="col d-flex justify-content-center align-items-center">
                            <label for="" class="text-dark">Hablar o encontrarme con otras personas para hablar sobre muchos temas. </label>
                        </div>
                        <div class="col d-flex justify-content-center align-items-center">
                            <input type="checkbox" name="" id="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> 

    <div class="col p-0" id="item4" style="display: none;">
        <div class="row-reverse" >
            <div class="col p-0  my-3">
                <label for="" class="h4">Verdadero o falso</label>
            </div>
            <div class="row-reverse mx-auto mb-2">
                <div class="col-8 ">
                    <label for="" class="text-dark h4">¿No definir la FELICIDAD parece que nos aleja de estudiarla entenderla y poder construir un plan para lograrla?</label>
                </div>
                <div class="col my-3 ">
                    <div class="row">
                        <div class="col-2">
                            <div class="form-check">
                                <input type="radio" name="flexRadioDefault" id="flexRadioDefault1">
                                <label for="" class="h5 ml-3">Verdadero</label>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-check">
                                <input type="radio" name="flexRadioDefault" id="flexRadioDefault2">
                                <label for="" class="h5 ml-3">Falso</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> 

   

    <!-- 1 CUESTIONARIO -->
    {{-- <div class="col p-0" id="item1">
        <div class="row-reverse">
            <div class="col p-0">
                <label for="" class="h4 font-weight-bold">Las barreras mentales no generan aspectos positivos no
                    sensaciones negativas como:</label>
            </div>
            <div class="row mx-auto mb-2">
                <div class="col-md-12 p-0">
                    <div class="row">
                        <div class="col-md-2">
                            <div class="card bg-dark">
                                <a href="javascript:;" class="img" onclick="itemselect1()">
                                    <div class="card-body p-0 m-0 bg-white d-flex justify-content-center" style="width: 100%; height: 300px; border-style: solid; border-color: #312783; ">
                                        <div class="mt-5" style="position: absolute; display: none" id="check1">
                                            <i class="far fa-check-circle text-success" style="font-size: 100px"></i>
                                        </div>
                                        <div class="col mt-auto d-flex justify-content-center">
                                            <label for="" class="lead h4 text-center font-weight-bold">Falta de
                                                confianza</label>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="card bg-dark">
                                <a href="javascript:;" class="img" onclick="itemselect2()">
                                    <div class="card-body p-0 m-0 bg-white d-flex justify-content-center" style="width: 100%; height: 300px; border-style: solid; border-color: #312783; ">
                                        <div class="mt-5" style="position: absolute; display: none" id="check2">
                                            <i class="far fa-check-circle text-success" style="font-size: 100px"></i>
                                        </div>
                                        <div class="col mt-auto d-flex justify-content-center">
                                            <label for="" class="lead h4 text-center font-weight-bold">Falta de
                                                confianza</label>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="card bg-dark">
                                <a href="javascript:;" class="img" onclick="itemselect3()">
                                    <div class="card-body p-0 m-0 bg-white d-flex justify-content-center" style="width: 100%; height: 300px; border-style: solid; border-color: #312783; ">
                                        <div class="mt-5" style="position: absolute; display: none" id="check3">
                                            <i class="far fa-check-circle text-success" style="font-size: 100px"></i>
                                        </div>
                                        <div class="col mt-auto d-flex justify-content-center">
                                            <label for="" class="lead h4 text-center font-weight-bold">Falta de
                                                confianza</label>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="card bg-dark">
                                <a href="javascript:;" class="img" onclick="itemselect4()">
                                    <div class="card-body p-0 m-0 bg-white d-flex justify-content-center" style="width: 100%; height: 300px; border-style: solid; border-color: #312783; ">
                                        <div class="mt-5" style="position: absolute; display: none" id="check4">
                                            <i class="far fa-check-circle text-success" style="font-size: 100px"></i>
                                        </div>
                                        <div class="col mt-auto d-flex justify-content-center">
                                            <label for="" class="lead h4 text-center font-weight-bold">Falta de
                                                confianza</label>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-8">
                            <a class="btn form-control" onclick="itemall()" style="color: #fff; background: #312783">TODAS LAS ANTERIORES</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> --}}

    <!-- 2 CUESTIONARIO -->
    {{-- <div class="col p-0 " id="item2" style="display: none;">

        <div class="row mx-auto mb-2" >
            <div class="col-md-12 p-0 " >
                <div class="row-reverse">
                    <div class="col-8">
                        <label for="" class="text-dark h4">Lorem ipsum dolor sit amet consectetur adipisicing elit. Placeat, nisi, qui facilis sequi quae ex doloribus minus asperiores debitis nostrum quasi consectetur, accusantium eveniet rem totam similique. Ab, enim eaque?</label>
                    </div>
                    <div class="col my-3">
                        <div class="row">
                            <div class="col-2">
                                <div class="form-check">
                                    <input type="radio" name="flexRadioDefault" id="flexRadioDefault1">
                                    <label for="" class="h5 ml-3">Verdadero</label>
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-check">
                                    <input type="radio" name="flexRadioDefault" id="flexRadioDefault2">
                                    <label for="" class="h5 ml-3">Falso</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row mx-auto mb-2" >
            <div class="col-md-12 p-0 " >
                <div class="row-reverse">
                    <div class="col-8">
                        <label for="" class="text-dark h4">Lorem ipsum dolor sit amet consectetur adipisicing elit. Placeat, nisi, qui facilis sequi quae ex doloribus minus asperiores debitis nostrum quasi consectetur, accusantium eveniet rem totam similique. Ab, enim eaque?</label>
                    </div>
                    <div class="col my-3">
                        <div class="row">
                            <div class="col-2">
                                <div class="form-check">
                                    <input type="radio" name="flexRadioDefault" id="flexRadioDefault1">
                                    <label for="" class="h5 ml-3">Verdadero</label>
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-check">
                                    <input type="radio" name="flexRadioDefault" id="flexRadioDefault2">
                                    <label for="" class="h5 ml-3">Falso</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row mx-auto mb-2" >
            <div class="col-md-12 p-0 " >
                <div class="row-reverse">
                    <div class="col-8">
                        <label for="" class="text-dark h4">Lorem ipsum dolor sit amet consectetur adipisicing elit. Placeat, nisi, qui facilis sequi quae ex doloribus minus asperiores debitis nostrum quasi consectetur, accusantium eveniet rem totam similique. Ab, enim eaque?</label>
                    </div>
                    <div class="col my-3">
                        <div class="row">
                            <div class="col-2">
                                <div class="form-check">
                                    <input type="radio" name="flexRadioDefault" id="flexRadioDefault1">
                                    <label for="" class="h5 ml-3">Verdadero</label>
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-check">
                                    <input type="radio" name="flexRadioDefault" id="flexRadioDefault2">
                                    <label for="" class="h5 ml-3">Falso</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> --}}


    <!-- 3 CUESTIONARIO -->
    {{-- <div class="col p-0" id="item3" style="display: none;">
        <div class="row-reverse">
            <div class="col p-0">
                <label for="" class="h4">Conecte las afirmaciones con el circulo correspondiente indiando si es verdadero (V) o falso (F)</label>
            </div>
            <div class="row mx-auto mb-2">
                <div class="col-md-12 p-0">
                    <div class="row mx-auto p-0">
                        <div class="col-md-1 p-0 mt-3 d-flex justify-content-center align-items-center">
                            <div class="row-reverse">
                                <div class="col mb-2">
                                    <i class="fas fa-check-circle text-success" onclick="check_true()" style="cursor: pointer;"> V</i>
                                </div>
                                <div class="col">
                                    <i class=" fas fa-times-circle text-danger" onclick="check_false()" style="cursor: pointer;"> F</i>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-10 p-0 mt-3">
                            <div class="row-reverse p-0 m-0 ">
                                <div class="col m-0 p-0">
                                    <label for="" class="h5 font-weight-bold" style="float: left; color: #312783">Pacientes en Alto Riesgo II hace referencia aquellos que tiene más de 24 meses en el tratamiento y que tienen niveles de adherencia inferiores al 80% durante los últims 6 meses.</label>
                                </div>
                                <div class="col m-0 p-0">
                                    <hr class="m-0 p-0" style="width: 100%; height: 2px; border-style: solid; border-color: #312783; float: left">
                                </div>
                            </div>

                        </div>
                        <div class="col-md-1 d-flex align-items-center">
                            <div class="col" id="true" style="display: none">
                                <button class="btn-sm btn-success border-0" style="cursor: pointer;">
                                    <span>Verdadero
                                        <input type="checkbox" class="mx-auto" name="true" id="myChecktrue" disabled style="display: none;"></span>
                                </button>
                            </div>
                            <div class="col" id="false" style="display: none">
                                <button class="btn-sm btn-danger border-0" style="cursor: pointer;">
                                    <span>Falso
                                        <input type="checkbox" class="mx-auto" name="false" id="myCheckfalse" disabled style="display: none;"></span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> --}}
</div>


<div class="row mx-auto" id="bottom1">
    <div class="col d-flex align-items-center">
        <label for="" style="text-decoration-line: underline;">SALIR</label>
    </div>
    <div class="col d-flex justify-content-end">
        <a href="javascript:;" class="btn" style="background: #312783;" onclick="item1()">Siguiente</a>
    </div>
</div>

<div class="row mx-auto" id="bottom2" style="display: none">
    <div class="col d-flex align-items-center">
        <label for="" style="text-decoration-line: underline;">SALIR</label>
    </div>
    <div class="col d-flex justify-content-end">
        <a href="javascript:;" class="btn" style="background: #312783;" onclick="item2()">Siguiente</a>
    </div>
</div>

<div class="row mx-auto" id="bottom3" style="display: none;">
    <div class="col d-flex align-items-center">
        <label for="" style="text-decoration-line: underline;">SALIR</label>
    </div>
    <div class="col d-flex justify-content-end">
        <a href="javascript:;" class="btn" style="background: #312783;" onclick="item3()">Siguiente</a>
    </div>
</div>

<div class="row mx-auto" id="bottom4" style="display: none;">
    <div class="col d-flex align-items-center">
        <label for="" style="text-decoration-line: underline;">SALIR</label>
    </div>
    <div class="col d-flex justify-content-end">
        <a href="javascript:;" class="btn" style="background: #312783;" data-toggle="modal" data-target="#exampleModal">Finalizar</a>
    </div>
</div>


<!-- PRIMER MODAL -->

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row ">
                    <div class="col d-flex justify-content-center">
                        <label for="" class="font-weight-bold lead" style="color: #312783;">Has terminado el Curso 1</label>
                    </div>
                </div>
                <div class="row mt-5">
                    <div class="col-md-3 p-0 d-flex justify-content-end align-items-center">
                        <div class="fileinput fileinput-new text-center m-0" data-provides="fileinput">
                            <div class="fileinput-new thumbnail img-circle img-raised d-flex justify-content-center align-items-center" style="background: linear-gradient(90deg, rgba(49,39,131,1) 0%, rgba(227,6,19,1) 100%); width: 200px; height: 100px;">
                                <a href="javascript:;" class="h2 font-weight-bold text-white">20</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8 p-0 m-0">
                        <div class="row-reverse p-0 m-0">
                            <div class="col m-0 p-0">
                                <label for="" class="text-right">Tienes 3 de 5 respuestas correctas, has ganado:</label>
                                <label for="" class="h3 font-weight-bold" style="float: right; color: #312783">PUNTOS</label>
                            </div>
                            <div class="col m-0 p-0">
                                <hr class="m-0 p-0" style="width: 100%; height: 2px; border-style: solid; border-color: #312783; float: left">
                            </div>
                        </div>


                    </div>
                    <div class="row-reverse ml-auto mt-3 mr-3">
                        <div class="col mx-auto">
                            <a class="btn text-white " style="background: #312783;" data-toggle="modal" data-target="#exampleModal2" data-dismiss="modal">SIGUIENTE CURSO</a>
                        </div>
                        <div class="col d-flex justify-content-end">
                            <a style="color: #312783; text-decoration-line: underline" href="{{route('cuestionario_patient.index')}}">Volver a hacer el examen</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- SEGUNDO MODAL -->

<div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row ">
                    <div class="col d-flex justify-content-center">
                        <label for="" class="font-weight-bold lead" style="color: #312783;">Has terminado los retos básicos Curso 1</label>
                    </div>
                </div>
                <div class="row mt-5">
                    <div class="col-md-3 p-0 d-flex justify-content-end align-items-center">
                        <div class="fileinput fileinput-new text-center m-0" data-provides="fileinput">
                            <div class="fileinput-new thumbnail img-circle img-raised d-flex justify-content-center align-items-center" style="background: linear-gradient(90deg, rgba(49,39,131,1) 0%, rgba(227,6,19,1) 100%); width: 200px; height: 100px;">
                                <a href="javascript:;" class="h2 font-weight-bold text-white">20</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8 p-0 m-0">
                        <div class="row-reverse p-0 m-0">
                            <div class="col m-0 p-0">
                                <label for="" class="text-right">Felicitaciones, has cumplido con todos los ejercicios del reto básico Curso 1, has ganado:</label>
                                <label for="" class="h3 font-weight-bold" style="float: right; color: #312783">insignia Curso 1</label>
                            </div>
                            <div class="col m-0 p-0">
                                <hr class="m-0 p-0" style="width: 100%; height: 2px; border-style: solid; border-color: #312783; float: left">
                            </div>
                            <div class="col m-0 p-0">
                                <label for="" class="h3 p-0 m-0" style="float: right; color: #312783">200 Puntos</label>
                            </div>
                        </div>
                    </div>
                    <div class="row-reverse ml-auto mt-3">
                        <div class="col d-flex justify-content-end">
                            <a class="btn text-white" style="background: #312783;" href="{{route('retos_patient.index')}}">CURSO MEDIO</a>
                        </div>
                        <div class="col d-flex justify-content-end">
                            <a href="{{route('retos_patient.index')}}" style="color: #312783; text-decoration-line: underline">Volver a hacer el entrenamiento básico</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>


<script>
    function check_true() {
        $("#myChecktrue").prop('checked', true);
        $("#true").css({
            'display': 'block'
        })
        $("#false").css({
            'display': 'none'
        })
    }

    function check_false() {
        $("#myCheckfalse").prop('checked', true);
        $("#false").css({
            'display': 'block'
        })
        $("#true").css({
            'display': 'none'
        })
    }
</script>

<script>
    function item1() {
        $('#bottom1').css({
            'display': 'none'
        })
        $('#bottom2').css({
            'display': 'block'
        })
        $('#one').css({
            'display': 'none'
        })
        $('#ico1').css({
            'display': 'block',
            'color': 'white'
        })
        $('#divtwo').css({
            'background': 'linear-gradient(90deg, rgba(49,39,131,1) 0%, rgba(227,6,19,1) 100%)'
        })
        $('#item1').css({
            'display': 'none'
        })
        $('#item2').css({
            'display': 'block'
        })
    }

    function item2() {
        $('#bottom1').css({
            'display': 'none'
        })
        $('#bottom2').css({
            'display': 'none'
        })
        $('#bottom3').css({
            'display': 'block'
        })
        $('#one').css({
            'display': 'none'
        })
        $('#ico1').css({
            'display': 'block',
            'color': 'white'
        })
        $('#two').css({
            'display': 'none'
        })
        $('#ico2').css({
            'display': 'block',
            'color': 'white'
        })
        $('#three').css({
            'display': 'block'
        })
        $('#ico3').css({
            'display': 'none',
            'color': 'white'
        })
        $('#divtwo').css({
            'background': 'linear-gradient(90deg, rgba(49,39,131,1) 0%, rgba(227,6,19,1) 100%)'
        })
        $('#divthree').css({
            'background': 'linear-gradient(90deg, rgba(49,39,131,1) 0%, rgba(227,6,19,1) 100%)'
        })
        $('#item1').css({
            'display': 'none'
        })
        $('#item2').css({
            'display': 'none'
        })
        $('#item3').css({
            'display': 'block'
        })
    }

    function item3() {
        $('#bottom1').css({
            'display': 'none'
        })
        $('#bottom2').css({
            'display': 'none'
        })
        $('#bottom3').css({
            'display': 'none'
        })

        $('#bottom4').css({
            'display': 'block'
        })
        $('#one').css({
            'display': 'none'
        })
        $('#ico1').css({
            'display': 'block',
            'color': 'white'
        })
        $('#two').css({
            'display': 'none'
        })
        $('#ico2').css({
            'display': 'block',
            'color': 'white'
        })
        $('#three').css({
            'display': 'none'
        })
        $('#ico3').css({
            'display': 'block',
            'color': 'white'
        })
        $('#four').css({
            'display': 'block'
        })
        $('#ico4').css({
            'display': 'none',
            'color': 'white'
        })
        $('#divtwo').css({
            'background': 'linear-gradient(90deg, rgba(49,39,131,1) 0%, rgba(227,6,19,1) 100%)'
        })
        $('#divthree').css({
            'background': 'linear-gradient(90deg, rgba(49,39,131,1) 0%, rgba(227,6,19,1) 100%)'
        })
        $('#divfour').css({
            'background': 'linear-gradient(90deg, rgba(49,39,131,1) 0%, rgba(227,6,19,1) 100%)'
        })
        $('#item1').css({
            'display': 'none'
        })
        $('#item2').css({
            'display': 'none'
        })
        $('#item3').css({
            'display': 'none'
        })
        $('#item4').css({
            'display': 'block'
        })
    }

</script>

<style>
    .img {
        background-color: powder#312783;
        transition: .5s ease;
    }

    .img:hover {
        background-color: #312783 !important;
        opacity: 0.9;
        color: #312783;
    }

</style>

<script>
    function itemselect1() {
        $('#check1').css({
            'display': 'block'
        })

        $('#check2').css({
            'display': 'none'
        })

        $('#check3').css({
            'display': 'none'
        })

        $('#check4').css({
            'display': 'none'
        })

        $('#check5').css({
            'display': 'none'
        })
    }

    function itemselect2() {
        $('#check1').css({
            'display': 'none'
        })

        $('#check2').css({
            'display': 'block'
        })

        $('#check3').css({
            'display': 'none'
        })

        $('#check4').css({
            'display': 'none'
        })

        $('#check5').css({
            'display': 'none'
        })
    }

    function itemselect3() {
        $('#check1').css({
            'display': 'none'
        })

        $('#check2').css({
            'display': 'none'
        })

        $('#check3').css({
            'display': 'block'
        })

        $('#check4').css({
            'display': 'none'
        })
        $('#check5').css({
            'display': 'none'
        })
    }

    function itemselect4() {
        $('#check1').css({
            'display': 'none'
        })

        $('#check2').css({
            'display': 'none'
        })

        $('#check3').css({
            'display': 'none'
        })

        $('#check4').css({
            'display': 'block'
        })

        $('#check5').css({
            'display': 'none'
        })
    }

    function itemselect5() {
        $('#check1').css({
            'display': 'none'
        })

        $('#check2').css({
            'display': 'none'
        })

        $('#check3').css({
            'display': 'none'
        })

        $('#check4').css({
            'display': 'none'
        })

        
        $('#check5').css({
            'display': 'block'
        })
    }

    function itemall() {
        $('#check1').css({
            'display': 'block'
        })

        $('#check2').css({
            'display': 'block'
        })

        $('#check3').css({
            'display': 'block'
        })

        $('#check4').css({
            'display': 'block'
        })

        $('#check5').css({
            'display': 'block'
        })
    }
</script>



