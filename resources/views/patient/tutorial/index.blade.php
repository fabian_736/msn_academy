
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="{{ url('img/apple-icon.jpg') }}">
    <link rel="icon" type="image/png" href="{{ url('img/logo_login.jpg') }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>
        MSN
    </title>
    <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
    <link rel="stylesheet" type="text/css"
    href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css" rel="stylesheet" />

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>

<!-- CSS Files -->
<link href="{{url('login/app.css')}}" rel="stylesheet" />
</head>

<body class="body body_tutorial" onload="deshabilitaRetroceso()">

    <div class="row mx-auto my-3">
        <div class="col d-flex justify-content-center align-items-center">
            <ul class="list_tutorial m-0 p-0">
                <li class="mr-2">
                    <div style="width: 25px; height: 25px; background: #312783; border-radius: 20px; cursor:pointer" id="point_1"></div>
                </li>
                <li class="mr-2">
                    <div style="width: 25px; height: 25px; border-radius: 20px; background: white; cursor:pointer" id="point_2"></div>
                </li>
                <li class="mr-2">
                    <div style="width: 25px; height: 25px; border-radius: 20px; background: white; cursor:pointer" id="point_3"></div>
                </li>
                <li class="mr-2">
                    <div style="width: 25px; height: 25px; border-radius: 20px; background: white; cursor:pointer" id="point_4"></div>
                </li>
                <li class="mr-2">
                    <div style="width: 25px; height: 25px; border-radius: 20px; background: white; cursor:pointer" id="point_5"></div>
                </li>
            </ul>
        </div>
    </div>

    <div class="row-reverse mx-auto p-0" style="position: absolute; bottom: 20%; right: 5%; max-width: 20%; min-width: 20%">
        <div class="col d-flex justify-content-end p-0 m-0">
            <a href="javascript:;" id="1" class="btn  w-75 m-0 h4 font-weight-bold" style="background: linear-gradient(90deg, rgba(49,39,131,1) 0%, rgba(227,6,19,1) 100%);max-width: 100%; border-radius: 20px">CONTINUAR</a>

            <a href="javascript:;" id="2" class="btn  w-75 m-0 h4 font-weight-bold" style="background: linear-gradient(90deg, rgba(49,39,131,1) 0%, rgba(227,6,19,1) 100%);display: none; max-width: 100%; border-radius: 20px">CONTINUAR</a>

            <a href="javascript:;" id="3" class="btn w-75 m-0 h4 font-weight-bold" style="background: linear-gradient(90deg, rgba(49,39,131,1) 0%, rgba(227,6,19,1) 100%);display: none; max-width: 100%; border-radius: 20px">CONTINUAR</a>

            <a href="javascript:;" id="4" class="btn w-75 m-0 h4 font-weight-bold" style="background: linear-gradient(90deg, rgba(49,39,131,1) 0%, rgba(227,6,19,1) 100%);display: none; max-width: 100%; border-radius: 20px">CONTINUAR</a>

            <a href="{{ route('portal_patient.index') }}" id="5" class="btn w-75 m-0 h4 font-weight-bold" style="background: linear-gradient(90deg, rgba(49,39,131,1) 0%, rgba(227,6,19,1) 100%);display: none; max-width: 100%; border-radius: 20px">FINALIZAR</a>

        </div>
        <div class="col d-flex justify-content-end p-0 m-0 mt-3">

            <a href="javascript:;" id="2_back" class="btn w-75 m-0 h4 font-weight-bold" style="background: linear-gradient(90deg, rgba(49,39,131,1) 0%, rgba(227,6,19,1) 100%);display: none; max-width: 100%; border-radius: 20px"><i class="fas fa-undo"></i> Volver</a>

            <a href="javascript:;" id="3_back" class="btn w-75 m-0 h4 font-weight-bold" style="background: linear-gradient(90deg, rgba(49,39,131,1) 0%, rgba(227,6,19,1) 100%);display: none; max-width: 100%; border-radius: 20px"><i class="fas fa-undo"></i> Volver</a>

            <a href="javascript:;" id="4_back" class="btn w-75 m-0 h4 font-weight-bold" style="background: linear-gradient(90deg, rgba(49,39,131,1) 0%, rgba(227,6,19,1) 100%);display: none; max-width: 100%; border-radius: 20px"><i class="fas fa-undo"></i> Volver</a>

            <a href="javascript:;" id="5_back" class="btn w-75 m-0 h4 font-weight-bold" style="background: linear-gradient(90deg, rgba(49,39,131,1) 0%, rgba(227,6,19,1) 100%);display: none; max-width: 100%; border-radius: 20px"><i class="fas fa-undo"></i> Volver</a>

        </div>
    </div>



    <style>
        .list_tutorial{
            list-style: none;
        }

        .list_tutorial li{
            display: inline-block;
        }

        .body_tutorial{
            background-image: url("./../../img/tutorial/1920x961/1.jpg");
            background-size:     cover;                      /* <------ */
            background-repeat:   no-repeat;
            background-position: center center;
            background-attachment: fixed;
        }
    </style>





<script src="{{url('js/app.js')}}"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>


</body>

<script>
    $( "#1" ).click(function() {
        $("#point_2").css({
            'background' : '#312783'
        });
        $("#point_3").css({
            'background' : 'white'
        });
        $("#point_4").css({
            'background' : 'white'
        });
        $("#point_5").css({
            'background' : 'white'
        });
        $(".body_tutorial").css({
            'background-image': 'url("./../../img/tutorial/1920x961/2.jpg")',
            'background-size': 'cover',
            'background-repeat':'no-repeat',
            'background-position': 'center center',
            'background-attachment': 'fixed'
        });

        $("#1").css({
            'display': 'none'
        });

        $("#2").css({
            'display': 'block'
        });

        $("#3").css({
            'display': 'none'
        });

        $("#4").css({
            'display': 'none'
        });

        $("#5").css({
            'display': 'none'
        });

        $("#2_back").css({
            'display': 'block'
        });

        $("#3_back").css({
            'display': 'none'
        });

        $("#4_back").css({
            'display': 'none'
        });

        $("#5_back").css({
            'display': 'none'
        });

    });

    $( "#2" ).click(function() {
        $("#point_2").css({
            'background' : '#312783'
        });
        $("#point_3").css({
            'background' : '#312783'
        });
        $("#point_4").css({
            'background' : 'white'
        });
        $("#point_5").css({
            'background' : 'white'
        });
        $(".body").css({
            'background-image': 'url("./../../img/tutorial/1920x961/3.jpg")',
             'background-size': 'cover',
            'background-repeat':'no-repeat',
            'background-position': 'center center',
            'background-attachment': 'fixed'
        });

        $("#1").css({
            'display': 'none'
        });

        $("#2").css({
            'display': 'none'
        });

        $("#3").css({
            'display': 'block'
        });

        $("#4").css({
            'display': 'none'
        });

        $("#5").css({
            'display': 'none'
        });

        $("#6").css({
            'display': 'none'
        });

        $("#2_back").css({
            'display': 'none'
        });

        $("#3_back").css({
            'display': 'block'
        });

        $("#4_back").css({
            'display': 'none'
        });

        $("#5_back").css({
            'display': 'none'
        });

    });

    $( "#3" ).click(function() {
        $("#point_2").css({
            'background' : '#312783'
        });
        $("#point_3").css({
            'background' : '#312783'
        });
        $("#point_4").css({
            'background' : '#312783'
        });
        $("#point_5").css({
            'background' : 'white'
        });
        $(".body").css({
            'background-image': 'url("./../../img/tutorial/1920x961/4.jpg")',
             'background-size': 'cover',
            'background-repeat':'no-repeat',
            'background-position': 'center center',
            'background-attachment': 'fixed'
        });

        $("#1").css({
            'display': 'none'
        });

        $("#2").css({
            'display': 'none'
        });

        $("#3").css({
            'display': 'none'
        });

        $("#4").css({
            'display': 'block'
        });

        $("#5").css({
            'display': 'none'
        });


        $("#2_back").css({
            'display': 'none'
        });

        $("#3_back").css({
            'display': 'none'
        });

        $("#4_back").css({
            'display': 'block'
        });

        $("#5_back").css({
            'display': 'none'
        });

    });

    $( "#4" ).click(function() {
        $("#point_2").css({
            'background' : '#312783'
        });
        $("#point_3").css({
            'background' : '#312783'
        });
        $("#point_4").css({
            'background' : '#312783'
        });
        $("#point_5").css({
            'background' : '#312783'
        });
        $(".body").css({
            'background-image': 'url("./../../img/tutorial/1920x961/5.jpg")',
             'background-size': 'cover',
            'background-repeat':'no-repeat',
            'background-position': 'center center',
            'background-attachment': 'fixed'
        });

        $("#1").css({
            'display': 'none'
        });

        $("#2").css({
            'display': 'none'
        });

        $("#3").css({
            'display': 'none'
        });

        $("#4").css({
            'display': 'none'
        });

        $("#5").css({
            'display': 'block'
        });

        $("#2_back").css({
            'display': 'none'
        });

        $("#3_back").css({
            'display': 'none'
        });

        $("#4_back").css({
            'display': 'none'
        });

        $("#5_back").css({
            'display': 'block'
        });

    });

  

    
</script>

<script>
 function deshabilitaRetroceso(){
    window.location.hash="no-back-button";
    window.location.hash="Again-No-back-button" //chrome
    window.onhashchange=function(){window.location.hash="";}
}
</script>


<script>
     $( "#point_1" ).click(function() {
        $("#point_1").css({
            'background' : '#312783'
        });
        $("#point_2").css({
            'background' : 'white'
        });
        $("#point_3").css({
            'background' : 'white'
        });
        $("#point_4").css({
            'background' : 'white'
        });
        $("#point_5").css({
            'background' : 'white'
        });
        $(".body_tutorial").css({
            'background-image': 'url("./../../img/tutorial/1920x961/1.jpg")',
             'background-size': 'cover',
            'background-repeat':'no-repeat',
            'background-position': 'center center',
            'background-attachment': 'fixed'
        });

        $("#1").css({
            'display': 'block'
        });

        $("#2").css({
            'display': 'none'
        });

        $("#3").css({
            'display': 'none'
        });

        $("#4").css({
            'display': 'none'
        });

        $("#5").css({
            'display': 'none'
        });

        $("#2_back").css({
            'display': 'none'
        });

        $("#3_back").css({
            'display': 'none'
        });

        $("#4_back").css({
            'display': 'none'
        });

        $("#5_back").css({
            'display': 'none'
        });

    });

    $( "#point_2" ).click(function() {
        $("#point_2").css({
            'background' : '#312783'
        });
        $("#point_3").css({
            'background' : 'white'
        });
        $("#point_4").css({
            'background' : 'white'
        });
        $("#point_5").css({
            'background' : 'white'
        });
        $(".body_tutorial").css({
            'background-image': 'url("./../../img/tutorial/1920x961/2.jpg")',
             'background-size': 'cover',
            'background-repeat':'no-repeat',
            'background-position': 'center center',
            'background-attachment': 'fixed'
        });

        $("#1").css({
            'display': 'none'
        });

        $("#2").css({
            'display': 'block'
        });

        $("#3").css({
            'display': 'none'
        });

        $("#4").css({
            'display': 'none'
        });

        $("#5").css({
            'display': 'none'
        });

        $("#2_back").css({
            'display': 'block'
        });

        $("#3_back").css({
            'display': 'none'
        });

        $("#4_back").css({
            'display': 'none'
        });

        $("#5_back").css({
            'display': 'none'
        });

    });

    $( "#point_3" ).click(function() {
        $("#point_2").css({
            'background' : '#312783'
        });
        $("#point_3").css({
            'background' : '#312783'
        });
        $("#point_4").css({
            'background' : 'white'
        });
        $("#point_5").css({
            'background' : 'white'
        });
        $(".body").css({
            'background-image': 'url("./../../img/tutorial/1920x961/3.jpg")',
             'background-size': 'cover',
            'background-repeat':'no-repeat',
            'background-position': 'center center',
            'background-attachment': 'fixed'
        });

        $("#1").css({
            'display': 'none'
        });

        $("#2").css({
            'display': 'none'
        });

        $("#3").css({
            'display': 'block'
        });

        $("#4").css({
            'display': 'none'
        });

        $("#5").css({
            'display': 'none'
        });

        $("#6").css({
            'display': 'none'
        });

        $("#2_back").css({
            'display': 'none'
        });

        $("#3_back").css({
            'display': 'block'
        });

        $("#4_back").css({
            'display': 'none'
        });

        $("#5_back").css({
            'display': 'none'
        });
    });

    $( "#point_4" ).click(function() {
        $("#point_2").css({
            'background' : '#312783'
        });
        $("#point_3").css({
            'background' : '#312783'
        });
        $("#point_4").css({
            'background' : '#312783'
        });
        $("#point_5").css({
            'background' : 'white'
        });
        $(".body").css({
            'background-image': 'url("./../../img/tutorial/1920x961/4.jpg")',
             'background-size': 'cover',
            'background-repeat':'no-repeat',
            'background-position': 'center center',
            'background-attachment': 'fixed'
        });

        $("#1").css({
            'display': 'none'
        });

        $("#2").css({
            'display': 'none'
        });

        $("#3").css({
            'display': 'none'
        });

        $("#4").css({
            'display': 'block'
        });

        $("#5").css({
            'display': 'none'
        });


        $("#2_back").css({
            'display': 'none'
        });

        $("#3_back").css({
            'display': 'none'
        });

        $("#4_back").css({
            'display': 'block'
        });

        $("#5_back").css({
            'display': 'none'
        });

    });

    $( "#point_5" ).click(function() {
        $("#point_2").css({
            'background' : '#312783'
        });
        $("#point_3").css({
            'background' : '#312783'
        });
        $("#point_4").css({
            'background' : '#312783'
        });
        $("#point_5").css({
            'background' : '#312783'
        });
        $(".body").css({
            'background-image': 'url("./../../img/tutorial/1920x961/5.jpg")',
             'background-size': 'cover',
            'background-repeat':'no-repeat',
            'background-position': 'center center',
            'background-attachment': 'fixed'
        });

        $("#1").css({
            'display': 'none'
        });

        $("#2").css({
            'display': 'none'
        });

        $("#3").css({
            'display': 'none'
        });

        $("#4").css({
            'display': 'none'
        });

        $("#5").css({
            'display': 'block'
        });

        $("#2_back").css({
            'display': 'none'
        });

        $("#3_back").css({
            'display': 'none'
        });

        $("#4_back").css({
            'display': 'none'
        });

        $("#5_back").css({
            'display': 'block'
        });
    });
</script>


<script>
    $( "#2_back" ).click(function() {
        $("#point_1").css({
            'background' : '#312783'
        });
        $("#point_2").css({
            'background' : 'white'
        });
        $("#point_3").css({
            'background' : 'white'
        });
        $("#point_4").css({
            'background' : 'white'
        });
        $("#point_5").css({
            'background' : 'white'
        });
        $(".body_tutorial").css({
            'background-image': 'url("./../../img/tutorial/1920x961/1.jpg")',
             'background-size': 'cover',
            'background-repeat':'no-repeat',
            'background-position': 'center center',
            'background-attachment': 'fixed'
        });

        $("#1").css({
            'display': 'block'
        });

        $("#2").css({
            'display': 'none'
        });

        $("#3").css({
            'display': 'none'
        });

        $("#4").css({
            'display': 'none'
        });

        $("#5").css({
            'display': 'none'
        });

        $("#2_back").css({
            'display': 'none'
        });

        $("#3_back").css({
            'display': 'none'
        });

        $("#4_back").css({
            'display': 'none'
        });

        $("#5_back").css({
            'display': 'none'
        });


   });

   $( "#3_back" ).click(function() {
    $("#point_2").css({
            'background' : '#312783'
        });
        $("#point_3").css({
            'background' : 'white'
        });
        $("#point_4").css({
            'background' : 'white'
        });
        $("#point_5").css({
            'background' : 'white'
        });
        $(".body_tutorial").css({
            'background-image': 'url("./../../img/tutorial/1920x961/2.jpg")',
             'background-size': 'cover',
            'background-repeat':'no-repeat',
            'background-position': 'center center',
            'background-attachment': 'fixed'
        });

        $("#1").css({
            'display': 'none'
        });

        $("#2").css({
            'display': 'block'
        });

        $("#3").css({
            'display': 'none'
        });

        $("#4").css({
            'display': 'none'
        });

        $("#5").css({
            'display': 'none'
        });

        $("#2_back").css({
            'display': 'block'
        });

        $("#3_back").css({
            'display': 'none'
        });

        $("#4_back").css({
            'display': 'none'
        });

        $("#5_back").css({
            'display': 'none'
        });

   });

   $( "#4_back" ).click(function() {
    $("#point_2").css({
            'background' : '#312783'
        });
        $("#point_3").css({
            'background' : '#312783'
        });
        $("#point_4").css({
            'background' : 'white'
        });
        $("#point_5").css({
            'background' : 'white'
        });
        $(".body").css({
            'background-image': 'url("./../../img/tutorial/1920x961/3.jpg")',
             'background-size': 'cover',
            'background-repeat':'no-repeat',
            'background-position': 'center center',
            'background-attachment': 'fixed'
        });

        $("#1").css({
            'display': 'none'
        });

        $("#2").css({
            'display': 'none'
        });

        $("#3").css({
            'display': 'block'
        });

        $("#4").css({
            'display': 'none'
        });

        $("#5").css({
            'display': 'none'
        });

        $("#6").css({
            'display': 'none'
        });

        $("#2_back").css({
            'display': 'none'
        });

        $("#3_back").css({
            'display': 'block'
        });

        $("#4_back").css({
            'display': 'none'
        });

        $("#5_back").css({
            'display': 'none'
        });
   });

   
   $( "#5_back" ).click(function() {
    $("#point_2").css({
            'background' : '#312783'
        });
        $("#point_3").css({
            'background' : '#312783'
        });
        $("#point_4").css({
            'background' : '#312783'
        });
        $("#point_5").css({
            'background' : 'white'
        });
        $(".body").css({
            'background-image': 'url("./../../img/tutorial/1920x961/4.jpg")',
             'background-size': 'cover',
            'background-repeat':'no-repeat',
            'background-position': 'center center',
            'background-attachment': 'fixed'
        });

        $("#1").css({
            'display': 'none'
        });

        $("#2").css({
            'display': 'none'
        });

        $("#3").css({
            'display': 'none'
        });

        $("#4").css({
            'display': 'block'
        });

        $("#5").css({
            'display': 'none'
        });


        $("#2_back").css({
            'display': 'none'
        });

        $("#3_back").css({
            'display': 'none'
        });

        $("#4_back").css({
            'display': 'block'
        });

        $("#5_back").css({
            'display': 'none'
        });
   });



</script>




</html>
