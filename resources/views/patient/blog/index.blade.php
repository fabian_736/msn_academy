@extends('layouts.patient.app')
@section('content')

<div class="row-reverse p-0 m-0">
    <div class="col p-0 m-0">
        <label for="" class="h3 font-weight-bold" style="color: #312783">Blog</label>
    </div>
    <div class="col p-0 m-0">
        <label for="" class="lead font-weight-bold" style="color: #312783">Tema del mes</label>
    </div>
</div>

<div class="row-reverse">
    <div class="col">
        <div class="card p-3 mx-auto">
            <div class="row">
                <div class="col-md-2 d-flex justify-content-center">
                    <div class="fileinput fileinput-new text-center " data-provides="fileinput">
                        <div class="fileinput-new thumbnail img-circle img-raised p-1 bg-white" style="width:300px; height:100px">
                            <a href="{{route('perfil_professional.index')}}"><img src="{{ url('img/user2.png') }}" style="border-radius: 60px; width: 100%; height: 100%" rel="nofollow" alt="..."></a>
                        </div>
                    </div>
                </div>
                <div class="col-md-10">
                    <div class="row d-flex justify-content-between">
                        <div class="col">
                            <label for="" class="lead font-weight-bold" style="color: #312783">Dra. Laura Gomez</label>
                        </div>
                        <div class="col d-flex justify-content-end">
                            <label for="" class="font-weight-bold" style="color: #312783">Tema del mes</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <label for="">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation incidunt ut laoreet dolore magna aliquam erat volutpat. </label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col card" id="cardcontainer" style="height: 100px; overflow-y: scroll;">
                            <div id="chat_converse" class="chat_converse">

                            </div>

                        </div>
                    </div>
                    <div class="row mx-auto">
                        <div class="col-md-9 m-0 p-0">
                            <textarea id="chatSend" name="chat_message" cols="30" rows="10" class="form-control" placeholder="Escribe un comentario..." style="width: 100%; height: 40px !important; background: #F5F5F5; border-radius: 20px; padding: 10px"></textarea>
                        </div>
                        <div class="col-md-1 col-sm-12 col-xs-12 p-0 m-0 d-flex justify-content-center align-items-center" style="cursor: pointer; width: 40px; height: 40px; border-radius: 60px; background: #312783">
                            <i class="fas fa-paper-plane" style="font-size: 20px; color: white; "></i>
                        </div>
                        <div class="col-md-1 col-sm-12 col-xs-12 pt-2 m-0 d-flex justify-content-center align-items-center">
                            <label for="file-upload" class="subir text-dark ">
                                <i class="fas fa-paperclip m-0 p-0" style="font-size: 20px; cursor: pointer;"></i>
                            </label>
                            <input id="file-upload" onchange='cambiar()' type="file" style='display: none;' />
                   
                        </div>
                        <div class="col-md-1 col-sm-12 col-xs-12 p-0 m-0 d-flex justify-content-start align-items-center" style="cursor: pointer;">
                        <i class="far fa-thumbs-up" style="color: #AEDC5A; font-size: 20px" ></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col">
        <div class="card p-3 mx-auto">
            <div class="row">
                <div class="col-md-2 d-flex justify-content-center">
                    <div class="fileinput fileinput-new text-center " data-provides="fileinput">
                        <div class="fileinput-new thumbnail img-circle img-raised p-1 bg-white" style="width:300px; height:100px">
                            <a href="{{route('perfil_professional.index')}}"><img src="{{ url('img/user2.png') }}" style="border-radius: 60px; width: 100%; height: 100%" rel="nofollow" alt="..."></a>
                        </div>
                    </div>
                </div>
                <div class="col-md-10">
                    <div class="row d-flex justify-content-between">
                        <div class="col">
                            <label for="" class="lead font-weight-bold" style="color: #312783">Dra. Laura Gomez</label>
                        </div>
                        <div class="col d-flex justify-content-end">
                            <label for="" class="font-weight-bold" style="color: #312783">Tema del mes</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <label for="">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation incidunt ut laoreet dolore magna aliquam erat volutpat. </label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col card" id="cardcontainer" style="height: 100px; overflow-y: scroll;">
                            <div id="chat_converse" class="chat_converse">

                            </div>

                        </div>
                    </div>
                    <div class="row mx-auto">
                        <div class="col-md-9 m-0 p-0">
                            <textarea id="chatSend" name="chat_message" cols="30" rows="10" class="form-control" placeholder="Escribe un comentario..." style="width: 100%; height: 40px !important; background: #F5F5F5; border-radius: 20px; padding: 10px"></textarea>
                        </div>
                        <div class="col-md-1 col-sm-12 col-xs-12 p-0 m-0 d-flex justify-content-center align-items-center" style="cursor: pointer; width: 40px; height: 40px; border-radius: 60px; background: #312783">
                            <i class="fas fa-paper-plane" style="font-size: 20px; color: white; "></i>
                        </div>
                        <div class="col-md-1 col-sm-12 col-xs-12 pt-2 m-0 d-flex justify-content-center align-items-center">
                            <label for="file-upload" class="subir text-dark ">
                                <i class="fas fa-paperclip m-0 p-0" style="font-size: 20px; cursor: pointer;"></i>
                            </label>
                            <input id="file-upload" onchange='cambiar()' type="file" style='display: none;' />
                   
                        </div>
                        <div class="col-md-1 col-sm-12 col-xs-12 p-0 m-0 d-flex justify-content-start align-items-center" style="cursor: pointer;">
                        <i class="far fa-thumbs-up" style="color: #AEDC5A; font-size: 20px" ></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script>
    function cambiar() {
        var pdrs = document.getElementById('file-upload').files[0].name;
    }
</script>

<style>
     #cardcontainer::-webkit-scrollbar {
            width: 8px;
            /* Tamaño del scroll en vertical */
            height: 8px;
            /* Tamaño del scroll en horizontal */
        }

        #cardcontainer::-webkit-scrollbar-thumb {
            background: #ccc;
            border-radius: 4px;
        }

        /* Cambiamos el fondo y agregamos una sombra cuando esté en hover */
        #cardcontainer::-webkit-scrollbar-thumb:hover {
            background: #b3b3b3;
            box-shadow: 0 0 2px 1px rgba(0, 0, 0, 0.2);
        }

        /* Cambiamos el fondo cuando esté en active */
        #cardcontainer::-webkit-scrollbar-thumb:active {
            background-color: #999999;
        }

        #cardcontainer::-webkit-scrollbar-track {
            background: #e1e1e1;
            border-radius: 4px;
        }

        /* Cambiamos el fondo cuando esté en active o hover */
        #cardcontainer::-webkit-scrollbar-track:hover,
        #cardcontainer::-webkit-scrollbar-track:active {
            background: #d4d4d4;
        }
</style>

@endsection