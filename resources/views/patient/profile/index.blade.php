@extends('layouts.patient.app2')
@section('content')

<div class="row">
    <div class="col-md-1">
        <div style="width: 16px; height: 100% !important; border-radius: 40px; padding: 3px; background: #FFFFFF; box-shadow: inset 0px 12.6135px 12.6135px rgba(0, 0, 0, 0.25);">
            <div class="d-flex justify-content-center" style="height: 100%; width: 100%; border-radius: 40px;">
                <div id="contenedor" style="position:absolute; width: 12px; border-radius: 40px; z-index: 1000"></div>
                <div class="row d-flex justify-content-center" style="z-index: 1001">
                    <div class="bottom-circle-one" onclick="f1()" id="div1"></div>
                    <div class="bottom-circle" onclick="f2()" id="div2"></div>
                    <div class="bottom-circle" onclick="f3()" id="div3"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="row">
            <div class="card m-0 p-4" id="card1">
                <div class="row-reverse p-0 mb-5">
                    <div class="col m-0 p-0">
                        <label for="" class="h2 font-weight-bold m-0" style="color: #312783">Cristina Perez</label>
                    </div>
                    <div class="col m-0 p-0">
                        <label for="" class="h3 m-0" style="color: #312783"><b>Hipertensión Pulmonar</b> - PAP03</label>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <label for="">
                            Hola soy Cristina, divertida, emprendedora y muy inteligente 
                        </label>
                    </div>
                    <div class="col">
                        <div class="row">
                            <div class="col">
                                <div class="row">
                                    <div class="mx-auto d-flex justify-content-center align-items-center" style="z-index: 1001; width: 80px; height: 80px; border-radius: 80px; background: linear-gradient(90deg, rgba(49,39,131,1) 0%, rgba(227,6,19,1) 100%);">
                                        <label for="" class="lead font-weight-bold text-white">2000</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col my-auto">
                                <div class="row-reverse">
                                    <div class="col p-0 d-flex justify-content-start">
                                        <label for="" class="lead m-0" style="color: #312783">Puntos</label>
                                    </div>
                                    <div class="col p-0 d-flex justify-content-start">
                                        <label for="" class="font-weight-bold m-0" style="color: #312783">GANADOS</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row my-3">
                            <div class="col">
                                <div class="row">
                                    <div class="mx-auto d-flex justify-content-center align-items-center" style="z-index: 1001; width: 80px; height: 80px; border-radius: 80px; background: linear-gradient(90deg, rgba(49,39,131,1) 0%, rgba(227,6,19,1) 100%);">
                                        <label for="" class="lead font-weight-bold text-white">5</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col my-auto">
                                <div class="row-reverse">
                                    <div class="col p-0 d-flex justify-content-start">
                                        <label for="" class="lead m-0" style="color: #312783">Premios</label>
                                    </div>
                                    <div class="col p-0 d-flex justify-content-start">
                                        <label for="" class="font-weight-bold m-0" style="color: #312783">GANADOS</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <a href="{{route('profile_patient.edit')}}" class="btn btn-danger ml-auto my-3" style="background: #312783">
                        EDITAR PERFIL
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-5 mx-auto">
        <div style=" width: 30vw; background: linear-gradient(90deg, rgba(49,39,131,1) 0%, rgba(227,6,19,1) 100%); height: 30vw;" class="rounded-circle mx-auto">
            <img src="{{url('img/image.png')}}" class="rounded-circle" style="margin-left: 10%; margin-top: 5%" width="65%" height="70%" rel="nofollow" alt="...">
        </div>
    </div>
</div>

<script>
    function f1() {
        $('#div1').css({
            'width': '30px',
            'height': '30px',
            'border-radius': '40px',
            'margin-bottom': '150%',
            'background': 'linear-gradient(90deg, rgba(49,39,131,1) 0%, rgba(227,6,19,1) 100%)',
            'box-shadow': '2px 2px 3px rgba(0, 0, 0, 0.25)'
        });
        $('#div2').css({
            'width': '30px',
            'height': '30px',
            'border-radius': '40px',
            'margin-bottom': '150%',
            'background': 'radial-gradient(62% 154.33% at 19% 24.5%, #E6E6E6 0%, #787878 100%)',
            'box-shadow': '2px 2px 3px rgba(0, 0, 0, 0.25)'
        });
        $('#div3').css({
            'width': '30px',
            'height': '30px',
            'border-radius': '40px',
            'margin-bottom': '150%',
            'background': 'radial-gradient(62% 154.33% at 19% 24.5%, #E6E6E6 0%, #787878 100%)',
            'box-shadow': '2px 2px 3px rgba(0, 0, 0, 0.25)'
        });
        $('#contenedor').css({
            'position': 'absolute',
            'width': '12px',
            'border-radius': '40px',
            'z-index': '1000',
            'background': 'transparent'
        });
        $('#card1').css({
            'display': 'block'
        });
        $('#card2').css({
            'display': 'none'
        });
        $('#card3').css({
            'display': 'none'
        });
    };

    function f2() {
        $('#div2').css({
            'width': '30px',
            'height': '30px',
            'border-radius': '40px',
            'margin-bottom': '150%',
            'background': 'linear-gradient(90deg, rgba(49,39,131,1) 0%, rgba(227,6,19,1) 100%)',
            'box-shadow': '2px 2px 3px rgba(0, 0, 0, 0.25)'
        });
        $('#contenedor').css({
            'background': 'linear-gradient(90deg, rgba(49,39,131,1) 0%, rgba(227,6,19,1) 100%)',
            'height': '45%'
        });
        $('#div3').css({
            'width': '30px',
            'height': '30px',
            'border-radius': '40px',
            'margin-bottom': '150%',
            'background': 'linear-gradient(90deg, rgba(49,39,131,1) 0%, rgba(227,6,19,1) 100%)',
            'box-shadow': '2px 2px 3px rgba(0, 0, 0, 0.25)'
        });
        $('#card1').css({
            'display': 'none'
        });
        $('#card2').css({
            'display': 'block'
        });
        $('#card3').css({
            'display': 'none'
        });
    };

    function f3() {
        $('#div3').css({
            'width': '30px',
            'height': '30px',
            'border-radius': '40px',
            'margin-bottom': '150%',
            'background': 'linear-gradient(90deg, rgba(49,39,131,1) 0%, rgba(227,6,19,1) 100%)',
            'box-shadow': '2px 2px 3px rgba(0, 0, 0, 0.25)'
        });
        $('#contenedor').css({
            'background': 'linear-gradient(90deg, rgba(49,39,131,1) 0%, rgba(227,6,19,1) 100%)',
            'height': '100%'
        });
        $('#card1').css({
            'display': 'none'
        });
        $('#card2').css({
            'display': 'none'
        });
        $('#card3').css({
            'display': 'block'
        });

    };
</script>

@endsection

<style>
    .bottom-circle {
        width: 30px;
        height: 30px;
        border-radius: 40px;
        margin-bottom: 150%;
        background: radial-gradient(62% 154.33% at 19% 24.5%, #E6E6E6 0%, #787878 100%);
        box-shadow: 2px 2px 3px rgba(0, 0, 0, 0.25);
    }

    .bottom-circle-one {
        width: 30px;
        height: 30px;
        border-radius: 40px;
        margin-bottom: 150%;
        background: linear-gradient(90deg, rgba(49,39,131,1) 0%, rgba(227,6,19,1) 100%);
        box-shadow: 2px 2px 3px rgba(0, 0, 0, 0.25);
    }
</style>