@extends('layouts.patient.app2')
@section('content')

<div class="row">
    <h3 for="" class="font-weight-bold" style="color:#312783">Editar perfil</h3><br>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="row">
            <div class="card m-0 p-5" id="card1">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col">
                                <input type="text" placeholder="NOMBRES Y APELLIDOS*" class="form-control">
                            </div>
                        </div>
                        <div class="row my-3">
                            <div class="col">
                                <input type="text" placeholder="PROFESION - OCUPACIÓN*" class="form-control">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <textarea name="" id="" cols="30" rows="10" class="form-control" placeholder="DESCRIPCION (OPCIONAL)"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-4">
                    <a href="#" onclick="guardar()" class="btn btn-danger ml-auto" style="background: #312783">
                        GUARDAR
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-5 mx-auto">
        <div style=" width: 30vw; background: linear-gradient(90deg, rgba(49,39,131,1) 0%, rgba(227,6,19,1) 100%);; height: 30vw;" class="p-2 rounded-circle mx-auto">
            <div class="rounded-circle d-flex justify-content-center align-items-center" style="background: rgb(255,255,255);
background: linear-gradient(90deg, rgba(255,255,255,0.4962359943977591) 100%, rgba(0,212,255,1) 100%); width: 65%; height: 65%; margin-left: 10%; margin-top: 5%">
                <img src="{{url('img/camara.png')}}" class="imagen w-50" alt="">
                <div class="circle_percent" data-percent="100" style="display: none">
                    <div class="circle_inner">
                        <div class="round_per"></div>
                    </div>
                </div>
            </div>

            <div class="row mx-auto">
                <a class="mx-auto btn-primary-outline btn-file mt-5">
                    <span class="fileinput-new text-white"><u>CARGAR IMAGEN</u></span>
                    <input type="file" name="..." id="progress" />
                </a>
            </div>
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>


<script>
    function guardar() {
        const swalWithBootstrapButtons = Swal.mixin({
            customClass: {
                confirmButton: 'btn btn-success',
                cancelButton: 'btn btn-danger'
            },
            buttonsStyling: false
        })

        swalWithBootstrapButtons.fire({
            title: 'Estas Seguro de actualizar tu perfil?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Si, por favor!',
            cancelButtonText: 'No, cancelar!',
            reverseButtons: true
        }).then((result) => {
            if (result.isConfirmed) {
                swalWithBootstrapButtons.fire(
                        'Guardando!',
                        'El cambio de información ha sido exitoso.',
                        'success'
                    ),
                    setTimeout(function() {
                            window.top.location = "{{route('profile_patient.index')}}"
                        },
                        2000)
            } else if (
                /* Read more about handling dismissals below */
                result.dismiss === Swal.DismissReason.cancel
            ) {
                swalWithBootstrapButtons.fire(
                    'Cancelado!',
                    'Tus cambios no se han guardado.',
                    'error'
                )
            }
        })

    }

    var progress = document.getElementById('progress')

    progress.onclick = charge

    function charge() {
        document.body.onfocus = roar
        console.log('chargin')
    }

    function roar() {
        if (progress.value.length)
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'No has escogido ninguna imagen!'
            })
        else
            $(".circle_percent").css({
                'display': 'block'
            })
        $(".imagen").css({
            'display': 'none'
        })
        $(".circle_percent").each(function() {
            var $this = $(this),
                $dataV = $this.data("percent"),
                $dataDeg = $dataV * 3.6,
                $round = $this.find(".round_per");
            $round.css("transform", "rotate(" + parseInt($dataDeg + 180) + "deg)");
            $this.append('<div class="circle_inbox"><span class="percent_text"></span></div>');

            $this.prop('Counter', 0).animate({
                Counter: $dataV,
            }, {
                duration: 2000,
                easing: 'swing',
                step: function(now) {
                    $this.find(".percent_text").text(Math.ceil(now) + "%");

                },

            });
            if ($dataV >= 100) {
                $round.css("transform", "rotate(" + 360 + "deg)");
                setTimeout(function() {
                    $this.addClass("percent_more");
                    Swal.fire({
                        position: 'center',
                        icon: 'success',
                        title: 'Imagen de perfil actualizada',
                        showConfirmButton: false,
                        timer: 1800
                    })
                }, 2000);
                setTimeout(function() {
                    $round.css("transform", "rotate(" + parseInt($dataDeg + 180) + "deg)");
                }, 2000);

            }

        });
        document.body.onfocus = null
        console.log('depleted')
    }
</script>

<script>
    function f1() {
        $('#div1').css({
            'width': '30px',
            'height': '30px',
            'border-radius': '40px',
            'margin-bottom': '150%',
            'background': 'linear-gradient(90deg, rgba(49,39,131,1) 0%, rgba(227,6,19,1) 100%);',
            'box-shadow': '2px 2px 3px rgba(0, 0, 0, 0.25)'
        });
        $('#div2').css({
            'width': '30px',
            'height': '30px',
            'border-radius': '40px',
            'margin-bottom': '150%',
            'background': 'radial-gradient(62% 154.33% at 19% 24.5%, #E6E6E6 0%, #787878 100%)',
            'box-shadow': '2px 2px 3px rgba(0, 0, 0, 0.25)'
        });
        $('#div3').css({
            'width': '30px',
            'height': '30px',
            'border-radius': '40px',
            'margin-bottom': '150%',
            'background': 'radial-gradient(62% 154.33% at 19% 24.5%, #E6E6E6 0%, #787878 100%)',
            'box-shadow': '2px 2px 3px rgba(0, 0, 0, 0.25)'
        });
        $('#contenedor').css({
            'position': 'absolute',
            'width': '12px',
            'border-radius': '40px',
            'z-index': '1000',
            'background': 'transparent'
        });
        $('#card1').css({
            'display': 'block'
        });
        $('#card2').css({
            'display': 'none'
        });
        $('#card3').css({
            'display': 'none'
        });
    };

    function f2() {
        $('#div2').css({
            'width': '30px',
            'height': '30px',
            'border-radius': '40px',
            'margin-bottom': '150%',
            'background': 'linear-gradient(90deg, rgba(49,39,131,1) 0%, rgba(227,6,19,1) 100%);',
            'box-shadow': '2px 2px 3px rgba(0, 0, 0, 0.25)'
        });
        $('#contenedor').css({
            'background': 'radial-gradient(805.33% 412.26% at 48.77% -319.62%, #AEDC5A 0%, #02718B 100%)',
            'height': '45%'
        });
        $('#div3').css({
            'width': '30px',
            'height': '30px',
            'border-radius': '40px',
            'margin-bottom': '150%',
            'background': 'radial-gradient(62% 154.33% at 19% 24.5%, #E6E6E6 0%, #787878 100%)',
            'box-shadow': '2px 2px 3px rgba(0, 0, 0, 0.25)'
        });
        $('#card1').css({
            'display': 'none'
        });
        $('#card2').css({
            'display': 'block'
        });
        $('#card3').css({
            'display': 'none'
        });
    };

    function f3() {
        $('#div3').css({
            'width': '30px',
            'height': '30px',
            'border-radius': '40px',
            'margin-bottom': '150%',
            'background': 'linear-gradient(90deg, rgba(49,39,131,1) 0%, rgba(227,6,19,1) 100%);',
            'box-shadow': '2px 2px 3px rgba(0, 0, 0, 0.25)'
        });
        $('#contenedor').css({
            'background': 'radial-gradient(805.33% 412.26% at 48.77% -319.62%, #AEDC5A 0%, #02718B 100%)',
            'height': '100%'
        });
        $('#card1').css({
            'display': 'none'
        });
        $('#card2').css({
            'display': 'none'
        });
        $('#card3').css({
            'display': 'block'
        });

    };
</script>

@endsection

<style>
    .bottom-circle {
        width: 30px;
        height: 30px;
        border-radius: 40px;
        margin-bottom: 150%;
        background: radial-gradient(62% 154.33% at 19% 24.5%, #E6E6E6 0%, #787878 100%);
        box-shadow: 2px 2px 3px rgba(0, 0, 0, 0.25);
    }

    .bottom-circle-one {
        width: 30px;
        height: 30px;
        border-radius: 40px;
        margin-bottom: 150%;
        background: linear-gradient(90deg, rgba(49,39,131,1) 0%, rgba(227,6,19,1) 100%);;
        box-shadow: 2px 2px 3px rgba(0, 0, 0, 0.25);
    }

    .circle_percent {
        font-size: 200px;
        width: 1em;
        height: 1em;
        position: relative;
        background: #eee;
        border-radius: 50%;
        overflow: hidden;
        display: inline-block;
        margin: 20px;
    }

    .circle_inner {
        position: absolute;
        left: 0;
        top: 0;
        width: 1em;
        height: 1em;
        clip: rect(0 1em 1em .5em);
    }

    .round_per {
        position: absolute;
        left: 0;
        top: 0;
        width: 1em;
        height: 1em;
        background: linear-gradient(90deg, rgba(49,39,131,1) 0%, rgba(227,6,19,1) 100%);;
        box-shadow: 2px 2px 3px rgba(0, 0, 0, 0.25);
        clip: rect(0 1em 1em .5em);
        transform: rotate(180deg);
        transition: 1.05s;
    }

    .percent_more .circle_inner {
        clip: rect(0 .5em 1em 0em);
    }

    .percent_more:after {
        position: absolute;
        left: .5em;
        top: 0em;
        right: 0;
        bottom: 0;
        background: linear-gradient(90deg, rgba(49,39,131,1) 0%, rgba(227,6,19,1) 100%);;
        box-shadow: 2px 2px 3px rgba(0, 0, 0, 0.25);
        content: '';
    }

    .circle_inbox {
        position: absolute;
        top: 10px;
        left: 10px;
        right: 10px;
        bottom: 10px;
        background: #fff;
        z-index: 3;
        border-radius: 50%;
    }

    .percent_text {
        position: absolute;
        font-size: 36px;
        left: 50%;
        top: 50%;
        transform: translate(-50%, -50%);
        z-index: 3;
    }
</style>