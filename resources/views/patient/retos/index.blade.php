@extends('layouts.patient.app')
@section('content')


<div class="row-reverse">
    <div class="col">
        <label for="" class="h2 font-weight-bold m-0 p-0" style="color: #E30613">
            Modulo 1
        </label>
    </div>
</div>
<div class="row mx-auto my-5">
    <div class="col-md-8 " >
        <label for="" class="lead">El fin de este Módulo es abordar diferentes temas con el paciente que le permitan conocerse y aceptarse dándole herramientas para su día/día.
        </label>
    </div>
</div>
<div class="row ">
    <div class="col-md-5 ">
        <div class="row mx-auto">
            <div class="col-md-3 p-0 d-flex justify-content-end align-items-center">
                <div class="fileinput fileinput-new text-center m-0" data-provides="fileinput">
                    <div class="fileinput-new thumbnail img-circle img-raised p-1 d-flex justify-content-center align-items-center" id="itemDiv4" style="width: 200px; height: 100px;" >
                        <a href="javascript:;" onclick="clickOn4()"><i class="fas fa-stethoscope" id="ico4" style="font-size: 60px; color: #312783; " ></i></a>
                    </div>
                </div>
            </div>
            <div class="col-md-6 p-0 m-0">
                <div class="row-reverse p-0 m-0">
                    <div class="col m-0 p-0">
                        <label for="" class="h3 font-weight-bold" style="float: right; color: #312783; cursor: pointer" onclick="windows()"  >Introducción</label>
                    </div>
                    <div class="col m-0 p-0">
                        <hr class="m-0 p-0" style="width: 100%; height: 2px; border-style: solid; border-color: #312783; float: left">
                    </div>
                </div>
                
            </div>
        </div>
        <div class="row mx-auto">
            <div class="col-md-3 p-0 d-flex justify-content-end align-items-center">
                <div class="fileinput fileinput-new text-center m-0" data-provides="fileinput">
                    <div class="fileinput-new thumbnail img-circle img-raised p-1 d-flex justify-content-center align-items-center" id="itemDiv" style="width: 200px; height: 100px;" >
                        <a href="javascript:;" onclick="clickOn()"><i class="fas fa-stethoscope" id="ico" style="font-size: 60px; color: #312783; " ></i></a>
                    </div>
                </div>
            </div>
            <div class="col-md-6 p-0 m-0">
                <div class="row-reverse p-0 m-0">
                    <div class="col m-0 p-0">
                        <label for="" class="h3 font-weight-bold" onclick="windows_curso1()" style="cursor: pointer; float: right; color: #312783">CURSO 1</label>
                    </div>
                    <div class="col m-0 p-0">
                        <hr class="m-0 p-0" style="width: 100%; height: 2px; border-style: solid; border-color: #312783; float: left">
                    </div>
                    <div class="col m-0 p-0">
                        <a href="{{route('cuestionario_patient.index')}}" class="h5 font-weight-bold m-0 p-0" style="float: right; color: #312783">Hacer cuestionario</a>
                    </div>
                </div>
                
            </div>
        </div>
        <div class="row mx-auto">
            <div class="col-md-3 p-0 d-flex justify-content-end align-items-center">
                <div class="fileinput fileinput-new text-center m-0" data-provides="fileinput">
                    <div class="fileinput-new thumbnail img-circle img-raised p-1 d-flex justify-content-center align-items-center" id="itemDiv2" style="width: 200px; height: 100px" >
                        <a href="javascript:;"  onclick="clickOn2()"><i class="fas fa-stethoscope" id="ico2" style="font-size: 60px; color: #312783" ></i></a>
                    </div>
                </div>
            </div>
            <div class="col-md-6 p-0 m-0">
                <div class="row-reverse p-0 m-0">
                    <div class="col m-0 p-0">
                        <label  for="" class="h3 font-weight-bold" onclick="windows_curso2()" style="cursor: pointer; float: right; color: #312783">CURSO 2</label>
                    </div>
                    <div class="col m-0 p-0">
                        <hr class="m-0 p-0" style="width: 100%; height: 2px; border-style: solid; border-color: #A0C96D; float: left">
                    </div>
                    <div class="col m-0 p-0">
                        <a href="{{route('cuestionario2_patient.index')}}" class="h5 font-weight-bold m-0 p-0" style="float: right; color: #312783">Hacer cuestionario</a>
                    </div>
                </div>
                
            </div>
        </div>
        <div class="row mx-auto">
            <div class="col-md-3 p-0 d-flex justify-content-end align-items-center">
                <div class="fileinput fileinput-new text-center m-0" data-provides="fileinput">
                    <div class="fileinput-new thumbnail img-circle img-raised p-1 d-flex justify-content-center align-items-center" id="itemDiv3" style="width: 200px; height: 100px" >
                        <a href="javascript:; "  onclick="clickOn3()"><i class="fas fa-stethoscope" id="ico3" style="font-size: 60px; color: #312783" ></i></a>
                    </div>
                </div>
            </div>
            <div class="col-md-6 p-0 m-0">
                <div class="row-reverse p-0 m-0">
                    <div class="col m-0 p-0">
                        <label for="" class="h3 font-weight-bold" onclick="windows_curso3()" style="cursor: pointer; float: right; color: #312783">CURSO 3</label>
                    </div>
                    <div class="col m-0 p-0">
                        <hr class="m-0 p-0" style="width: 100%; height: 2px; border-style: solid; border-color: #A0C96D; float: left">
                    </div>
                    <div class="col m-0 p-0">
                        <a href="javascript:;" class="h5 font-weight-bold m-0 p-0" style="float: right; color: #312783">Hacer cuestionario</a>
                    </div>
                </div>
                
            </div>
        </div>
        <div class="row mx-auto ">
            <div class="col">
                <a href="javascript:;" class="btn rounded w-50 my-3" style="background: #312783">SIGUIENTE</a>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title text-left h3 font-weight-bold mx-auto" style="color: #312783" id="exampleModalLabel">Introducción</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close" >
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row mx-auto">
              <div class="card col-md-5 mx-auto p-3 " style="box-shadow: 0 2px 2px 0 rgb(0 0 0 / 14%), 0 3px 1px -2px rgb(0 0 0 / 20%), 0 1px 5px 0 rgb(0 0 0 / 12%) !important">
                <a href="javascript:;" onclick="windows()" id="clickCuest">
                <img src="{{ url('img/pdf.png') }}" style="width: 90%; height: 100%" rel="nofollow" alt="...">
                </a>
                <img src="{{ url('img/pdf.png') }}" id="after_pdf" style="width: 90%; height: 100%; display: none" rel="nofollow" alt="...">
              </div>
          </div>
          <div class="row mx-auto my-3">
              <a href="javascript:;" onclick="exitmodal()" data-dismiss="modal" id="next" class="mx-auto font-weight-bold" style="text-decoration-line: underline; color: #312783; display: none">
                  FINALIZAR
              </a>
          </div>
        </div>

      </div>
    </div>
  </div>

<script>
    function windows() {
        window.open("{{ url('/') }}/pdf/1_INTRODUCCIÓN_baja.pdf", "ventana1", "toolbar=yes,scrollbars=yes,resizable=yes,top=500,left=500,width=4000,height=4000")
       {/*
       
        $('#next').css({
        'display' : 'block'
        })
        $('#clickCuest').css({
        'display' : 'none'
        })
        $('#after_pdf').show();


    */}
    }

    function windows_curso1() {


        window.open("{{ url('/') }}/pdf/2_PLANEA_TU_FELICIDAD_baja.pdf", "ventana1", "toolbar=yes,scrollbars=yes,resizable=yes,top=500,left=500,width=4000,height=4000")
        
          {/*
        
        $('#next').css({
        'display' : 'block'
        })
        $('#clickCuest').css({
        'display' : 'none'
        })
        $('#after_pdf').show();
        

    */}
    }

    function windows_curso2() {
        window.open("{{ url('/') }}/pdf/3_LADRONES_DE_LA_FELICIDAD_baja.pdf", "ventana1", "toolbar=yes,scrollbars=yes,resizable=yes,top=500,left=500,width=4000,height=4000")
          {/*
        
        
        $('#next').css({
        'display' : 'block'
        })
        $('#clickCuest').css({
        'display' : 'none'
        })
        $('#after_pdf').show();

          */}
    }

    function windows_curso3() {
        window.open("{{ url('/') }}/pdf/4_HABITOS_PROPOSITOS_baja.pdf", "ventana1", "toolbar=yes,scrollbars=yes,resizable=yes,top=500,left=500,width=4000,height=4000")
       {/*
       
        $('#next').css({
        'display' : 'block'
        })
        $('#clickCuest').css({
        'display' : 'none'
        })
        $('#after_pdf').show();

         */}
    }



    function exitmodal(){
        $('#next').css({
        'display' : 'none'
        })
        $('#clickCuest').css({
        'display' : 'none'
        })
    }
</script>

<script>
function clickCuest(){
    
}



function clickOn(){
    $('#ico').css({
        'color' : 'white'
    })

    $('#itemDiv').css({
        'background' : '#312783'
    })

    $('#ico2').css({
        'color' : '#312783'
    })

    $('#itemDiv2').css({
        'background' : 'white'
    })

    $('#ico3').css({
        'color' : '#312783'
    })

    $('#ico4').css({
        'color' : '#312783'
    })

    $('#itemDiv3').css({
        'background' : 'white'
    })

    $('#itemDiv4').css({
        'background' : '#ffffff'
    })
}

function clickOn2(){
    $('#ico').css({
        'color' : '#312783'
    })
    $('#ico4').css({
        'color' : '#312783'
    })

    $('#itemDiv').css({
        'background' : 'white'
    })

    $('#ico2').css({
        'color' : 'white'
    })

    $('#itemDiv2').css({
        'background' : '#312783'
    })

    $('#ico3').css({
        'color' : '#312783'
    })

    $('#itemDiv3').css({
        'background' : 'white'
    })

    $('#itemDiv4').css({
        'background' : '#ffffff'
    })
}

function clickOn3(){
    $('#ico').css({
        'color' : '#312783'
    })

    $('#itemDiv').css({
        'background' : 'white'
    })

    $('#ico2').css({
        'color' : '#312783'
    })

    $('#itemDiv2').css({
        'background' : 'white'
    })

    $('#ico3').css({
        'color' : 'white'
    })

    $('#itemDiv3').css({
        'background' : '#312783'
    })

    
}


function clickOn4(){
    $('#ico').css({
        'color' : '#312783'
    })

    $('#itemDiv').css({
        'background' : '#ffffff'
    })

    $('#ico2').css({
        'color' : '#312783'
    })

    $('#itemDiv2').css({
        'background' : 'white'
    })

    $('#ico3').css({
        'color' : '#312783'
    })

    $('#ico4').css({
        'color' : '#ffffff'
    })

    $('#itemDiv3').css({
        'background' : 'white'
    })

    $('#itemDiv4').css({
        'background' : '#312783'
    })
}
</script>

@endsection