@extends('layouts.professional.app')
@section('content')


<div class="row-reverse">
    <h3 style="color: #38B9C3" class="font-weight-bold">German Lopez</h3>
    <label for="" style="color: #38B9C3" class="font-weight-bold lead">Enfermero</label>
</div>

<div class="row my-4">
    <div class="col-md-3">
        <label for="" class="lead font-weight-bold">ENTRENAMIENTOS</label>
    </div>
    <div class="col-md-9" id="hrtitle">
        <hr style="height:2px;border-width:0;color:gray;background-color:gray">
    </div>
</div>
<div class="row my-4">
    <div class="col-md-12">
        <a href="{{route('practice_profesional.index')}}">
        @include('layouts.carousel.carousel');
    </a>
    </div>
</div>
<div class="row my-4">
    <div class="col-md-12">
        <hr style="height:2px;border-width:0;color:gray;background-color:gray">
    </div>
</div>


@endsection

<style>
    @media (max-width: 768px){
        #hrtitle {
            display: none
        }
    }
</style>