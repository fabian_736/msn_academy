<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="{{ url('img/apple-icon.png') }}">
    <link rel="icon" type="image/png" href="{{ url('img/logo_login.png') }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>
        Bayer Contigo
    </title>
    <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
    <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css"
        href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css"
        integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />
    <link rel="stylesheet" href="{{ url('datatables/main.css') }}">
    <link href="{{ url('icons/css/uicons-regular-rounded.css') }}" rel="stylesheet">
    <!--datables CSS básico-->
    <link rel="stylesheet" type="text/css" href="{{ url('datatables/datatables.min.css') }}" />
    <!--datables estilo bootstrap 4 CSS-->
    <link rel="stylesheet" type="text/css"
        href="{{ url('datatables/DataTables-1.10.18/css/dataTables.bootstrap4.min.css') }}">
    <!-- CSS Files -->
    <link href="{{ url('css/app.css') }}" rel="stylesheet" />
</head>

<body class="" style="margin: 0 !important; padding: 0 !important;">
    <div class="row p-0 m-0">
        <div class="p-0 m-0" style="background-color: #38B9C3; width: 30%" id="div1">
            <div class="row" style="margin-top: 15%; margin-bottom: 15%">
                <div class="col text-center">
                    <label class="font-weight-bold text-white lead" for="" id="title">ENTRENAMIENTO</label>
                </div>
            </div>
            <div class="row">
                <div class="col d-flex justify-content-end" style="left: 30px; z-index: 1000;"  >
                    <a onclick="f1()"
                        class="grow text-white font-weight-bold d-flex justify-content-center align-items-center"
                        style="width: 65px; height: 65px; background-color: #A0C96D; border-radius: 60px; font-size: 30px; cursor: pointer">
                        1
                    </a>

                </div>
            </div>
            <div class="row" style="margin-top: 15%; margin-bottom: 15%">
                <div class="col d-flex justify-content-end" style="left: 30px; z-index: 1000;">
                    <a onclick="f2()"
                        class="grow text-white font-weight-bold d-flex justify-content-center align-items-center"
                        style="width: 65px; height: 65px; background-color: #A0C96D; border-radius: 60px; font-size: 30px; cursor: pointer">
                        2
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col d-flex justify-content-end" style="left: 30px; z-index: 1000;">
                    <a onclick="f3()"
                        class="grow text-white font-weight-bold d-flex justify-content-center align-items-center"
                        style="width: 65px; height: 65px; background-color: #A0C96D; border-radius: 60px; font-size: 30px; cursor: pointer">
                        3
                    </a>
                </div>
            </div>
            <div class="row" style="margin-top: 15%; margin-bottom: 15%">
                <div class="col d-flex justify-content-end" style="left: 30px; z-index: 1000;">
                    <a onclick="f4()"
                        class="grow text-white font-weight-bold d-flex justify-content-center align-items-center"
                        style="width: 65px; height: 65px; background-color: #A0C96D; border-radius: 60px; font-size: 30px; cursor: pointer">
                        4
                    </a>
                </div>
            </div>
        </div>
        <div style="position: relative;">
            <div id="div3" style="display: none"></div>
        </div>
        <div style="width: 70%; background-image: url('/img/home.png'); height: 100%; background-position: center; background-repeat: no-repeat; background-size: cover;"
            id="div2" class="p-0 m-0">
            <div class="row p-3" style="height: 20%">
                <img src="{{ url('/img/logo_login.png') }}" style="height: 75%" class="ml-auto mr-5" alt="">
            </div>

            <div class="row px-3" style="height: 60%">
                <iframe class="mx-auto" style="display: none" id="video1" width="853" height="100%"
                    src="https://www.youtube.com/embed/_0AG0Fmn0_c" title="YouTube video player" frameborder="0"
                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                    allowfullscreen></iframe>
                <iframe class="mx-auto" style="display: none" id="video2" width="853" height="100%"
                    src="https://www.youtube.com/embed/luyXBZJVrRs" title="YouTube video player" frameborder="0"
                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                    allowfullscreen></iframe>
                <iframe class="mx-auto" style="display: none" id="video3" width="853" height="100%"
                    src="https://www.youtube.com/embed/ko2swYF0tPw" title="YouTube video player" frameborder="0"
                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                    allowfullscreen></iframe>
                <iframe class="mx-auto" style="display: none" id="video4" width="853" height="100%"
                    src="https://www.youtube.com/embed/_JDOGSkVMNY" title="YouTube video player" frameborder="0"
                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                    allowfullscreen></iframe>
            </div>

            <div class="row p-3 " style="height: 20%">
                <div class="col d-flex justify-content-end align-items-center mr-5">
                    <a href="#" class="mx-3">Omitir</a>
                    <button id="twofuns" onclick="f4()" class="btn"
                        style="background-color: #A0C96D;">Siguiente <i
                            class="fas fa-chevron-circle-right"></i></button>
                </div>
                <div class="col d-flex justify-content-end align-items-center mr-5">
                    <a href="{{ route('portal_patient.index') }}" class="mx-3">Ir a vistas paciente</a>
                    <a href="{{ route('portal_professional.index') }}" class="mx-3">Ir a vistas
                        profesional</a>
                </div>
            </div>


        </div>

    </div>







    <!--   Core JS Files   -->
    <script src="{{ url('js/app.js') }}"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>

    <script type="text/javascript" src="{{ url('datatables/datatables.min.js') }}"></script>

    <!-- para usar botones en datatables JS -->
    <script src="{{ url('datatables/Buttons-1.5.6/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ url('datatables/JSZip-2.5.0/jszip.min.js') }}"></script>
    <script src="{{ url('datatables/pdfmake-0.1.36/pdfmake.min.js') }}"></script>
    <script src="{{ url('datatables/pdfmake-0.1.36/vfs_fonts.js') }}"></script>
    <script src="{{ url('datatables/Buttons-1.5.6/js/buttons.html5.min.js') }}"></script>

    <!-- código JS propìo-->
    <script type="text/javascript" src="{{ url('datatables/main.js') }}"></script>


    <style>
        .grow:hover {
            -webkit-transform: scale(1.3);
            -ms-transform: scale(1.3);
            transform: scale(1.3);
        }

        @media (min-width: 500px) {
            #title {
                font-size: 15px;
            }

            #video1 {
                height: 300px;
            }

            #video2 {
                height: 300px;
            }

            #video3 {
                height: 300px;
            }

            #video4 {
                height: 300px;
            }

        }

    </style>



    <script>
        $(document).ready(function() {
            md.initDashboardPageCharts();

        });
    </script>


    <script type="text/javascript">
        $(document).ready(function() {

            var height = $(window).height();

            $('#div1').height(height);
            $('#div2').height(height);
        });

        function f1() {
            $('#div3').css({
                'position': 'absolute',
                'width': '5px',
                'height': '26%',
                'background-color': '#9ac7a0',
                'display': 'block',
                'border-radius': '30px'
            });
            $('#video1').css({
                'display': 'block',
            });
            $('#video2').css({
                'display': 'none',
            });
            $('#video3').css({
                'display': 'none',
            });
            $('#video4').css({
                'display': 'none',
            });
        };

        function f2() {
            $('#div3').css({
                'position': 'absolute',
                'width': '5px',
                'height': '45%',
                'background-color': '#9ac7a0',
                'display': 'block',
                'border-radius': '30px'
            });
            $('#video1').css({
                'display': 'none',
            });
            $('#video2').css({
                'display': 'block',
            });
            $('#video3').css({
                'display': 'none',
            });
            $('#video4').css({
                'display': 'none',
            });
        };

        function f3() {
            $('#div3').css({
                'position': 'absolute',
                'width': '5px',
                'height': '65%',
                'background-color': '#9ac7a0',
                'display': 'block',
                'border-radius': '30px'
            });
            $('#video1').css({
                'display': 'none',
            });
            $('#video2').css({
                'display': 'none',
            });
            $('#video3').css({
                'display': 'block',
            });
            $('#video4').css({
                'display': 'none',
            });
        };

        function f4() {
            $('#div3').css({
                'position': 'absolute',
                'width': '5px',
                'height': '100%',
                'background-color': '#9ac7a0',
                'display': 'block',
                'border-radius': '30px'
            });
            $('#video1').css({
                'display': 'none',
            });
            $('#video2').css({
                'display': 'none',
            });
            $('#video3').css({
                'display': 'none',
            });
            $('#video4').css({
                'display': 'block',
            });
        };
    </script>

</body>


</html>
