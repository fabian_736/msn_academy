@extends('layouts.professional.app2')
@section('content')

<div class="row">
    <div class="col-md-1">
        <div style="width: 16px; height: 100% !important; border-radius: 40px; padding: 3px; background: #FFFFFF; box-shadow: inset 0px 12.6135px 12.6135px rgba(0, 0, 0, 0.25);">
            <div class="d-flex justify-content-center" style="height: 100%; width: 100%; border-radius: 40px;">
                <div id="contenedor" style="position:absolute; width: 12px; border-radius: 40px; z-index: 1000"></div>
                <div class="row d-flex justify-content-center" style="z-index: 1001">
                    <div class="bottom-circle-one" onclick="f1()" id="div1"></div>
                    <div class="bottom-circle" onclick="f2()" id="div2"></div>
                    <div class="bottom-circle" onclick="f3()" id="div3"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="row">
            <div class="card m-0 p-5" id="card1">
                <div class="row">
                    <div class="col-md-8">
                        <div class="row">
                            <h3 for="" class="font-weight-bold" style="color:#38B9C3;">Cristina Sarin</h3><br>
                        </div>
                        <div class="row">
                            <label for="" class="lead" style="color:#38B9C3; font-style: italic;">Enfermera</label>
                        </div>
                        <div class="row my-3">
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                Delectus, ex iure assumenda eius voluptatibus recusandae perferendis
                                optio harum temporibus ipsa eligendi! Harum laborum,
                                libero earum neque autem minima sed dignissimos.</p>
                        </div>
                        <div class="row">
                            <a class="lead font-weight-bold" href="{{route('certificado_profesional.edit')}}" style="color: #669D00; cursor: pointer"><i class="fas fa-file-download mr-3"></i>DESCARGAR CERTIFICADOS</a>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div style="width: 2px; height: 75%; background-color: rgba(3, 114, 140, 1); position: absolute; left: 50%; z-index: 1000"></div>
                        <div class="row">
                            <div class="mx-auto d-flex justify-content-center align-items-center" style="z-index: 1001; width: 80px; height: 80px; border-radius: 80px; background: linear-gradient(90deg, rgba(56,185,195,1) 35%, rgba(245,215,142,1) 100%);">
                                <label for="" class="lead font-weight-bold text-white">2000</label>
                            </div>
                        </div>
                        <div class="row my-3">
                            <div class="mx-auto d-flex justify-content-center align-items-center" style="z-index: 1001; width: 60px; height: 60px; border-radius: 60px; background: linear-gradient(90deg, rgba(56,185,195,1) 35%, rgba(245,215,142,1) 100%);">
                                <label for="" class="lead font-weight-bold text-white">5</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="mx-auto d-flex justify-content-center align-items-center" style="z-index: 1001; width: 60px; height: 60px; border-radius: 60px; background: linear-gradient(90deg, rgba(56,185,195,1) 35%, rgba(245,215,142,1) 100%);">
                                <label for="" class="lead font-weight-bold text-white">10</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <a href="{{route('perfil_professional.edit')}}" class="btn btn-danger ml-auto my-3" style="background: radial-gradient(59.13% 153.27% at 6.94% 90.83%, #C4C4C4 0%, #787878 100%); box-shadow: 4.53333px 2.26667px 4.53333px rgba(0, 0, 0, 0.18);">
                        EDITAR PERFIL
</a>
                </div>
            </div>
            <div class="card m-0 p-5" id="card2" style="display: none;">
                <div class="row">
                    <div class="col-md-8">
                        <div class="row">
                            <h3 for="" class="font-weight-bold" style="color:#38B9C3;">Nombre de usuario</h3><br>
                        </div>
                        <div class="row">
                            <label for="" class="lead" style="color:#38B9C3; font-style: italic;">Especialidad</label>
                        </div>
                        <div class="row my-3">
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                Delectus, ex iure assumenda eius voluptatibus recusandae perferendis
                                optio harum temporibus ipsa eligendi! Harum laborum,
                                libero earum neque autem minima sed dignissimos.</p>
                        </div>
                        <div class="row">
                            <a class="lead font-weight-bold" style="color: #669D00;"><i class="fas fa-file-download mr-3"></i>DESCARGAR CERTIFICADOS</a>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div style="width: 2px; height: 75%; background-color: rgba(3, 114, 140, 1); position: absolute; left: 50%; z-index: 1000"></div>
                        <div class="row">
                            <div class="mx-auto d-flex justify-content-center align-items-center" style="z-index: 1001; width: 80px; height: 80px; border-radius: 80px; background: linear-gradient(90deg, rgba(56,185,195,1) 35%, rgba(245,215,142,1) 100%);">
                                <label for="" class="lead font-weight-bold text-white">2000</label>
                            </div>
                        </div>
                        <div class="row my-3">
                            <div class="mx-auto d-flex justify-content-center align-items-center" style="z-index: 1001; width: 60px; height: 60px; border-radius: 60px; background: linear-gradient(90deg, rgba(56,185,195,1) 35%, rgba(245,215,142,1) 100%);">
                                <label for="" class="lead font-weight-bold text-white">5</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="mx-auto d-flex justify-content-center align-items-center" style="z-index: 1001; width: 60px; height: 60px; border-radius: 60px; background: linear-gradient(90deg, rgba(56,185,195,1) 35%, rgba(245,215,142,1) 100%);">
                                <label for="" class="lead font-weight-bold text-white">10</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <a href="{{route('perfil_professional.edit')}}" class="btn btn-danger ml-auto my-3" style="background: radial-gradient(59.13% 153.27% at 6.94% 90.83%, #C4C4C4 0%, #787878 100%); box-shadow: 4.53333px 2.26667px 4.53333px rgba(0, 0, 0, 0.18);">
                        EDITAR PERFIL
</a>
                </div>
            </div>
            <div class="card m-0 p-5" id="card3" style="display: none;">
                <div class="row">
                    <div class="col-md-8">
                        <div class="row">
                            <h3 for="" class="font-weight-bold" style="color:#38B9C3;">Nombre de usuario 2</h3><br>
                        </div>
                        <div class="row">
                            <label for="" class="lead" style="color:#38B9C3; font-style: italic;">Especialidad</label>
                        </div>
                        <div class="row my-3">
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                Delectus, ex iure assumenda eius voluptatibus recusandae perferendis
                                optio harum temporibus ipsa eligendi! Harum laborum,
                                libero earum neque autem minima sed dignissimos.</p>
                        </div>
                        <div class="row">
                            <a class="lead font-weight-bold" style="color: #A0C96D;"><i class="fas fa-file-download mr-3"></i>DESCARGAR CERTIFICADOS</a>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div style="width: 2px; height: 75%; background-color: rgba(3, 114, 140, 1); position: absolute; left: 50%; z-index: 1000"></div>
                        <div class="row">
                            <div class="mx-auto d-flex justify-content-center align-items-center" style="z-index: 1001; width: 80px; height: 80px; border-radius: 80px; background: linear-gradient(90deg, rgba(56,185,195,1) 35%, rgba(245,215,142,1) 100%);">
                                <label for="" class="lead font-weight-bold text-white">2000</label>
                            </div>
                        </div>
                        <div class="row my-3">
                            <div class="mx-auto d-flex justify-content-center align-items-center" style="z-index: 1001; width: 60px; height: 60px; border-radius: 60px; background: linear-gradient(90deg, rgba(56,185,195,1) 35%, rgba(245,215,142,1) 100%);">
                                <label for="" class="lead font-weight-bold text-white">5</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="mx-auto d-flex justify-content-center align-items-center" style="z-index: 1001; width: 60px; height: 60px; border-radius: 60px; background: linear-gradient(90deg, rgba(56,185,195,1) 35%, rgba(245,215,142,1) 100%);">
                                <label for="" class="lead font-weight-bold text-white">10</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <a href="{{route('perfil_professional.edit')}}" class="btn btn-danger ml-auto my-3" style="background: radial-gradient(59.13% 153.27% at 6.94% 90.83%, #C4C4C4 0%, #787878 100%); box-shadow: 4.53333px 2.26667px 4.53333px rgba(0, 0, 0, 0.18);">
                        EDITAR PERFIL
</a>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-5 mx-auto">
        <div style=" width: 30vw; background: linear-gradient(90deg, rgba(56,185,195,1) 35%, rgba(245,215,142,1) 100%); height: 30vw;" class="rounded-circle mx-auto">
            <img src="{{url('img/image.png')}}" class="rounded-circle" style="margin-left: 10%; margin-top: 5%" width="65%" height="70%" rel="nofollow" alt="...">
            <div class="row d-flex justify-content-end">
                <div class="row">
                    <div class="col" style="top: 70%;">
                        <div class="d-flex justify-content-center align-items-center" style="width: 5vw; height: 5vw; border-radius: 60px; background: radial-gradient(62% 154.33% at 19% 24.5%, #E6E6E6 0%, #787878 100%); box-shadow: 3.7358px 1.8679px 3.7358px rgba(0, 0, 0, 0.18);">
                            <i class="fas fa-medal text-white" style="font-size: 30px"></i>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="d-flex justify-content-center align-items-center" style="width: 5vw; height: 5vw; border-radius: 60px; background: radial-gradient(62% 154.33% at 19% 24.5%, #E6E6E6 0%, #787878 100%); box-shadow: 3.7358px 1.8679px 3.7358px rgba(0, 0, 0, 0.18);">
                            <i class="fas fa-medal text-white" style="font-size: 30px"></i>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col" style="bottom: 100% !important; right: 20%;">
                        <div class="d-flex justify-content-center align-items-center" style="width: 5vw; height: 5vw; border-radius: 60px; background: radial-gradient(62% 154.33% at 19% 24.5%, #E6E6E6 0%, #787878 100%); box-shadow: 3.7358px 1.8679px 3.7358px rgba(0, 0, 0, 0.18);">
                            <i class="fas fa-medal text-white" style="font-size: 30px"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    function f1() {
        $('#div1').css({
            'width': '30px',
            'height': '30px',
            'border-radius': '40px',
            'margin-bottom': '150%',
            'background': 'linear-gradient(90deg, rgba(56,185,195,1) 35%, rgba(245,215,142,1) 100%)',
            'box-shadow': '2px 2px 3px rgba(0, 0, 0, 0.25)'
        });
        $('#div2').css({
            'width': '30px',
            'height': '30px',
            'border-radius': '40px',
            'margin-bottom': '150%',
            'background': 'linear-gradient(90deg, rgba(56,185,195,1) 35%, rgba(245,215,142,1) 100%)',
            'box-shadow': '2px 2px 3px rgba(0, 0, 0, 0.25)'
        });
        $('#div3').css({
            'width': '30px',
            'height': '30px',
            'border-radius': '40px',
            'margin-bottom': '150%',
            'background': 'linear-gradient(90deg, rgba(56,185,195,1) 35%, rgba(245,215,142,1) 100%)',
            'box-shadow': '2px 2px 3px rgba(0, 0, 0, 0.25)'
        });
        $('#contenedor').css({
            'position': 'absolute',
            'width': '12px',
            'border-radius': '40px',
            'z-index': '1000',
            'background': 'transparent'
        });
        $('#card1').css({
            'display':'block'
        });
        $('#card2').css({
            'display':'none'
        });
        $('#card3').css({
            'display':'none'
        });
    };

    function f2() {
        $('#div2').css({
            'width': '30px',
            'height': '30px',
            'border-radius': '40px',
            'margin-bottom': '150%',
            'background': 'linear-gradient(90deg, rgba(56,185,195,1) 35%, rgba(245,215,142,1) 100%)',
            'box-shadow': '2px 2px 3px rgba(0, 0, 0, 0.25)'
        });
        $('#contenedor').css({
            'background': 'linear-gradient(90deg, rgba(56,185,195,1) 35%, rgba(245,215,142,1) 100%)',
            'height': '45%'
        });
        $('#div3').css({
            'width': '30px',
            'height': '30px',
            'border-radius': '40px',
            'margin-bottom': '150%',
            'background': 'linear-gradient(90deg, rgba(56,185,195,1) 35%, rgba(245,215,142,1) 100%)',
            'box-shadow': '2px 2px 3px rgba(0, 0, 0, 0.25)'
        });
        $('#card1').css({
            'display':'none'
        });
        $('#card2').css({
            'display':'block'
        });
        $('#card3').css({
            'display':'none'
        });
    };

    function f3() {
        $('#div3').css({
            'width': '30px',
            'height': '30px',
            'border-radius': '40px',
            'margin-bottom': '150%',
            'background': 'linear-gradient(90deg, rgba(56,185,195,1) 35%, rgba(245,215,142,1) 100%)',
            'box-shadow': '2px 2px 3px rgba(0, 0, 0, 0.25)'
        });
        $('#contenedor').css({
            'background': 'linear-gradient(90deg, rgba(56,185,195,1) 35%, rgba(245,215,142,1) 100%)',
            'height': '100%'
        });
        $('#card1').css({
            'display':'none'
        });
        $('#card2').css({
            'display':'none'
        });
        $('#card3').css({
            'display':'block'
        });

    };
</script>

@endsection

<style>
    .bottom-circle {
        width: 30px;
        height: 30px;
        border-radius: 40px;
        margin-bottom: 150%;
        background: radial-gradient(62% 154.33% at 19% 24.5%, #E6E6E6 0%, #787878 100%);
        box-shadow: 2px 2px 3px rgba(0, 0, 0, 0.25);
    }

    .bottom-circle-one {
        width: 30px;
        height: 30px;
        border-radius: 40px;
        margin-bottom: 150%;
        background: linear-gradient(90deg, rgba(56,185,195,1) 35%, rgba(245,215,142,1) 100%);
        box-shadow: 2px 2px 3px rgba(0, 0, 0, 0.25);
    }
</style>