


<!-- HEAD (HTML) -->

@include('layouts.professional.components.head');

<!-- FIN HEAD (HTML) -->

<!-- NAVBAR -->

@include('layouts.professional.components.navbar');

<!-- FIN NAVBAR -->

<div class="content">
  <div class="content">
    <div class="row">

      <!-- BASE PRINCIPAL (SIDEBAR-LEFT - CONTENIDO - SIDEBAR-RIGHT) -->

      <div class="col-md-1" style="padding-left: 0 !important; padding-right: 0 !important;">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              @include('layouts.professional.components.left');
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-8" style="padding-left: 0 !important; padding-right: 0 !important;">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              <div class="card-transparent">
                @yield('content')
              </div>
            </div>
          </div>

        </div>
      </div>
      <div class="col-md-3" style="padding-left: 0 !important; padding-right: 0 !important;">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
                <div class="card-transparent py-3">
                    <div class="form-reverse mx-auto">
                        <div class="col">
                            <div class="bg-dark mx-auto d-flex align-items-center" style="background: linear-gradient(90deg, rgba(56,185,195,1) 35%, rgba(245,215,142,1) 100%);
                            border-radius: 80px; width: 130px; height: 130px;">
                                <i class="fas fa-heartbeat text-white mx-auto" style="font-size: 75px"></i>
                            </div>
                        </div>
                        <div class="col d-flex justify-content-center mt-2">
                            <label for="" class="">ENTRENAMIENTO 1</label>
                        </div>
                    </div>
                
                    <div class="form-reverse mx-auto my-3">
                        <div class="col">
                            <div class="bg-dark mx-auto d-flex align-items-center" style="background: #CCCCCC;
                            border-radius: 80px; width: 130px; height: 130px;">
                                <i class="fas fa-heartbeat text-white mx-auto" style="font-size: 75px"></i>
                            </div>
                        </div>
                        <div class="col d-flex justify-content-center mt-2">
                            <label for="" class="">ENTRENAMIENTO 2</label>
                        </div>
                    </div>
                
                    <div class="form-reverse mx-auto">
                        <div class="col">
                            <div class="bg-dark mx-auto d-flex align-items-center" style="background: #CCCCCC;
                            border-radius: 80px; width: 130px; height: 130px;">
                                <i class="fas fa-heartbeat text-white mx-auto" style="font-size: 75px"></i>
                            </div>
                        </div>
                        <div class="col d-flex justify-content-center mt-2">
                            <label for="" class="">ENTRENAMIENTO 3</label>
                        </div>
                    </div>
                </div>
                
                
            </div>
          </div>
        </div>
      </div>

      <!-- FIN BASE PRINCIPAL (SIDEBAR-LEFT - CONTENIDO - SIDEBAR-RIGHT) -->

    </div>
  </div>

</div>

<!-- FOOTER -->

@include('layouts.professional.components.footer');

<!-- FIN FOOTER -->



<!-- END (HTML) -->

@include('layouts.professional.components.end');

<!-- FIN END (HTML) -->