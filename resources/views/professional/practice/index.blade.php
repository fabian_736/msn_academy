@extends('professional.practice.sidebar_right')
@section('content')

<div class="row-reverse">
    <div class="col">
        <label for="" class="h2 font-weight-bold m-0 p-0" style="color: #38B9C3">
        Xarelto®
        </label>
    </div>
    <div class="col">
        <label for="" class="h3 m-0 p-0"  style="color: #38B9C3">
            Entrenamiento 1
        </label>
    </div>
</div>
<div class="row mx-auto my-5">
    <div class="col-md-6 " >
        <label for="" class="lead">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.
            Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
        </label>
    </div>
</div>
<div class="row ">
    <div class="col-md-5 ">
        <div class="row mx-auto">
            <div class="col-md-3 p-0 d-flex justify-content-end align-items-center">
                <div class="fileinput fileinput-new text-center m-0" data-provides="fileinput">
                    <div class="fileinput-new thumbnail img-circle img-raised p-1 d-flex justify-content-center align-items-center" id="itemDiv" style="width: 200px; height: 100px;" >
                        <a href="javascript:;" onclick="clickOn()"><i class="fas fa-stethoscope" id="ico" style="font-size: 60px; color: #38B9C3; " ></i></a>
                    </div>
                </div>
            </div>
            <div class="col-md-6 p-0 m-0" id="itemejercicio">
                <div class="row-reverse p-0 m-0">
                    <div class="col m-0 p-0">
                        <label for="" class="h3 font-weight-bold" style="float: right; color: #38B9C3">EJERCICIO 1</label>
                    </div>
                    <div class="col m-0 p-0">
                        <hr class="m-0 p-0" style="width: 100%; height: 2px; border-style: solid; border-color: #38B9C3; float: left">
                    </div>
                </div>
                
            </div>
        </div>
        <div class="row mx-auto">
            <div class="col-md-3 p-0 d-flex justify-content-end align-items-center">
                <div class="fileinput fileinput-new text-center m-0" data-provides="fileinput">
                    <div class="fileinput-new thumbnail img-circle img-raised p-1 d-flex justify-content-center align-items-center" id="itemDiv2" style="width: 200px; height: 100px" >
                        <a href="javascript:;"  onclick="clickOn2()"><i class="fas fa-stethoscope" id="ico2" style="font-size: 60px; color: #38B9C3" ></i></a>
                    </div>
                </div>
            </div>
            <div class="col-md-6 p-0 m-0" id="itemejercicio">
                <div class="row-reverse p-0 m-0" >
                    <div class="col m-0 p-0" >
                        <label for="" class="h3 font-weight-bold" style="float: right; color: #38B9C3">EJERCICIO 2</label>
                    </div>
                    <div class="col m-0 p-0">
                        <hr class="m-0 p-0" style="width: 100%; height: 2px; border-style: solid; border-color: #38B9C3; float: left">
                    </div>
                </div>
                
            </div>
        </div>
        <div class="row mx-auto">
            <div class="col-md-3 p-0 d-flex justify-content-end align-items-center">
                <div class="fileinput fileinput-new text-center m-0" data-provides="fileinput">
                    <div class="fileinput-new thumbnail img-circle img-raised p-1 d-flex justify-content-center align-items-center" id="itemDiv3" style="width: 200px; height: 100px" >
                        <a href="javascript:; "  onclick="clickOn3()"><i class="fas fa-stethoscope" id="ico3" style="font-size: 60px; color: #38B9C3" ></i></a>
                    </div>
                </div>
            </div>
            <div class="col-md-6 p-0 m-0" id="itemejercicio">
                <div class="row-reverse p-0 m-0">
                    <div class="col m-0 p-0">
                        <label for="" class="h3 font-weight-bold" style="float: right; color: #38B9C3">EJERCICIO 3</label>
                    </div>
                    <div class="col m-0 p-0">
                        <hr class="m-0 p-0" style="width: 100%; height: 2px; border-style: solid; border-color: #38B9C3; float: left">
                    </div>
                </div>
                
            </div>
        </div>
        <div class="row mx-auto ">
            <a href="" data-toggle="modal" data-target="#exampleModal">
                <label for="" class="lead font-weight-bold my-5 " style="text-decoration-line: underline; color: #38B9C3; cursor: pointer">HACER EL CUESTIONARIO</label>
            </a>
        </div>
    </div>
    <div class="col-md-3 d-flex align-items-center">
        <div class="col">
            <a href="{{route('cuestionario_profesional.index')}}" class="btn rounded" style="background: #A0C96D">SIGUIENTE</a>
        </div>
    </div>
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title text-left h3 font-weight-bold mx-auto" style="color: #38B9C3" id="exampleModalLabel">EJERCICIO 3</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row mx-auto">
              <div class="card col-md-5 mx-auto p-3 " style="box-shadow: 0 2px 2px 0 rgb(0 0 0 / 14%), 0 3px 1px -2px rgb(0 0 0 / 20%), 0 1px 5px 0 rgb(0 0 0 / 12%) !important">
                <img src="{{ url('img/pdf.png') }}" style="width: 90%; height: 100%" rel="nofollow" alt="..." id="img">
              </div>
              <div class="card col-md-5 mx-auto p-3" style="box-shadow: 0 2px 2px 0 rgb(0 0 0 / 14%), 0 3px 1px -2px rgb(0 0 0 / 20%), 0 1px 5px 0 rgb(0 0 0 / 12%) !important">
                <img src="{{ url('img/video.png') }}" style="width: 90%; height: 100%" rel="nofollow" alt="..." id="img">
              </div>
          </div>
          <div class="row mx-auto my-3">
              <a href="javascript:;" onclick="windows()" id="clickCuest" class="mx-auto font-weight-bold" style="text-decoration-line: underline; color: #38B9C3">
                HACER EL CUESTIONARIO
              </a>
              <a href="javascript:;" onclick="exitmodal()" data-dismiss="modal" id="next" class="mx-auto font-weight-bold" style="text-decoration-line: underline; color: #38B9C3; display: none">
                  FINALIZAR
              </a>
          </div>
        </div>

      </div>
    </div>
  </div>
@endsection


<style>
   @media screen and (max-width: 500px){
        #itemejercicio {
            margin-bottom: 5em !important;
        }
        #img{
            width: 30% !important; height: 90% !important; margin: auto;
        }
    }
</style>


<script>
    function windows() {
        window.open("https://www.proturbiomarspa.com/files/_pdf-prueba.pdf", "ventana1", "toolbar=yes,scrollbars=yes,resizable=yes,top=500,left=500,width=4000,height=4000")
        $('#next').css({
        'display' : 'block'
        })
        $('#clickCuest').css({
        'display' : 'none'
        })
    }

    function exitmodal(){
        $('#next').css({
        'display' : 'none'
        })
        $('#clickCuest').css({
        'display' : 'block'
        })
    }
</script>

<script>
function clickCuest(){
    
}



function clickOn(){
    $('#ico').css({
        'color' : 'white'
    })

    $('#itemDiv').css({
        'background' : '#38B9C3'
    })

    $('#ico2').css({
        'color' : '#38B9C3'
    })

    $('#itemDiv2').css({
        'background' : 'white'
    })

    $('#ico3').css({
        'color' : '#38B9C3'
    })

    $('#itemDiv3').css({
        'background' : 'white'
    })
}

function clickOn2(){
    $('#ico').css({
        'color' : '#38B9C3'
    })

    $('#itemDiv').css({
        'background' : 'white'
    })

    $('#ico2').css({
        'color' : 'white'
    })

    $('#itemDiv2').css({
        'background' : '#38B9C3'
    })

    $('#ico3').css({
        'color' : '#38B9C3'
    })

    $('#itemDiv3').css({
        'background' : 'white'
    })
}

function clickOn3(){
    $('#ico').css({
        'color' : '#38B9C3'
    })

    $('#itemDiv').css({
        'background' : 'white'
    })

    $('#ico2').css({
        'color' : '#38B9C3'
    })

    $('#itemDiv2').css({
        'background' : 'white'
    })

    $('#ico3').css({
        'color' : 'white'
    })

    $('#itemDiv3').css({
        'background' : '#38B9C3'
    })
}
</script>