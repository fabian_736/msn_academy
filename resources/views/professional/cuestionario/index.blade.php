@extends('layouts.patient.app2')
@section('content')

    <div class="row ">
        <div class="row-reverse">
            <div class="col">
                <label for="" class="h2" style="color: #38B9C3"><b>Cuestionario |</b> Entrenamiento 1 </label>
            </div>
            <div class="col">
                <label for="" class="h4" style="color: #38B9C3">Bienvenido a la prueba de adherencia al
                    conocimiento del módulo de producto Betaferon®</label>
            </div>
        </div>
    </div>


    <div class="row my-5 ">
        <div class="col-md-1 p-0 m-0">
            <div class="row-reverse" id="fila">
                <div class="col">
                    <div class="col-md-1" >
                        <div class="d-flex justify-content-center align-items-center"
                            style="width: 40px; height: 40px; border-radius: 60px; background: linear-gradient(90deg, rgba(56,185,195,1) 35%, rgba(245,215,142,1) 100%);">
                            <a href="" class="text-white font-weight-bold" id="one">1</a>
                            <i class="fas fa-check" id="ico1" style="display: none"></i>
                        </div>
                    </div>
                </div>
                <div class="col my-3">
                    <div class="col-md-1">
                        <div class="d-flex justify-content-center align-items-center" id="divtwo"
                            style="background: #C1C1C1; width: 40px; height: 40px; border-radius: 60px; ">
                            <a href="" class="text-white font-weight-bold" id="two">2</a>
                            <i class="fas fa-check" id="ico2" style="display: none"></i>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="col-md-1">
                        <div class="d-flex justify-content-center align-items-center" id="divthree"
                            style="background: #C1C1C1; width: 40px; height: 40px; border-radius: 60px; ">
                            <a href="" class="text-white font-weight-bold" id="three">3</a>
                            <i class="fas fa-check" id="ico3" style="display: none"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col p-0" id="item1">
            <div class="row-reverse" >
                <div class="col p-0">
                    <label for="" class="h4">Seleccione la respuesta correcta. Esta Betaferon indicado para el
                        tratamiento de:</label>
                </div>
                <div class="row mx-auto mb-2">
                    <div class="col-md-12 p-0">
                        <div class="row mx-auto p-0">
                            <div class="col-md-1 p-0 mt-3 d-flex justify-content-center align-items-center">
                                <div class="list-group fileinput fileinput-new text-center m-0" data-provides="fileinput">
                                    <div class="fileinput-new thumbnail img-circle img-raised p-1 d-flex justify-content-center align-items-center"
                                        id="itemDiv" style="width: 40px; height: 40px;">
                                        <a href="javascript:;" onclick="clickOn()"
                                            class="list-group-item font-weight-bold">a</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-10 p-0 mt-3">
                                <div class="row-reverse p-0 m-0 ">
                                    <div class="col m-0 p-0">
                                        <label for="" class="h5 font-weight-bold"
                                            style="float: left; color: #38B9C3">Esclerosis Múltiple</label>
                                    </div>
                                    <div class="col m-0 p-0">
                                        <hr class="m-0 p-0"
                                            style="width: 100%; height: 2px; border-style: solid; border-color: #38B9C3; float: left">
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mx-auto mb-2">
                    <div class="col-md-12 p-0">
                        <div class="row mx-auto p-0">
                            <div class="col-md-1 p-0 mt-3 d-flex justify-content-center align-items-center">
                                <div class="fileinput fileinput-new text-center m-0" data-provides="fileinput">
                                    <div class="list-group fileinput-new thumbnail img-circle img-raised p-1 d-flex justify-content-center align-items-center"
                                        id="itemDiv" style="width: 40px; height: 40px;">
                                        <a href="javascript:;" onclick="clickOn()"
                                            class="list-group-item font-weight-bold">b</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-10 p-0 mt-3">
                                <div class="row-reverse p-0 m-0 ">
                                    <div class="col m-0 p-0">
                                        <label for="" class="h5 font-weight-bold" style="float: left; color: #38B9C3">Un
                                            evento clínico único presuntivo de esclerosismúltiple (EM), (Síndrome clínico
                                            aislado)</label>
                                    </div>
                                    <div class="col m-0 p-0">
                                        <hr class="m-0 p-0"
                                            style="width: 100%; height: 2px; border-style: solid; border-color: #38B9C3; float: left">
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mx-auto mb-2">
                    <div class="col-md-12 p-0">
                        <div class="row mx-auto p-0">
                            <div class="col-md-1 p-0 mt-3 d-flex justify-content-center align-items-center">
                                <div class="fileinput fileinput-new text-center m-0" data-provides="fileinput">
                                    <div class="list-group fileinput-new thumbnail img-circle img-raised p-1 d-flex justify-content-center align-items-center"
                                        id="itemDiv" style="width: 40px; height: 40px;">
                                        <a href="javascript:;" onclick="clickOn()"
                                            class="list-group-item font-weight-bold">c</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-10 p-0 mt-3">
                                <div class="row-reverse p-0 m-0 ">
                                    <div class="col m-0 p-0">
                                        <label for="" class="h5 font-weight-bold" style="float: left; color: #38B9C3">Formas
                                            recidivantes de esclerosis múltiple (EM)</label>
                                    </div>
                                    <div class="col m-0 p-0">
                                        <hr class="m-0 p-0"
                                            style="width: 100%; height: 2px; border-style: solid; border-color: #38B9C3; float: left">
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mx-auto mb-2">
                    <div class="col-md-12 p-0">
                        <div class="row mx-auto p-0">
                            <div class="col-md-1 p-0 mt-3 d-flex justify-content-center align-items-center">
                                <div class="fileinput fileinput-new text-center m-0" data-provides="fileinput">
                                    <div class="list-group fileinput-new thumbnail img-circle img-raised p-1 d-flex justify-content-center align-items-center"
                                        style="width: 40px; height: 40px;">
                                        <a href="javascript:;" class="list-group-item font-weight-bold">d</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-10 p-0 mt-3">
                                <div class="row-reverse p-0 m-0 ">
                                    <div class="col m-0 p-0">
                                        <label for="" class="h5 font-weight-bold" style="float: left; color: #38B9C3">Todas
                                            las anteriores</label>
                                    </div>
                                    <div class="col m-0 p-0">
                                        <hr class="m-0 p-0"
                                            style="width: 100%; height: 2px; border-style: solid; border-color: #38B9C3; float: left">
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col p-0" id="item2" style="display: none;">
            <div class="row-reverse">
                <div class="col p-0">
                    <label for="" class="h4">Seleccione la respuesta correcta. ¿Cuál es el ingrediente activo de
                        Betaferon?</label>
                </div>
                <div class="row mx-auto mb-2">
                    <div class="col-md-12 p-0">
                        <div class="row mx-auto p-0">
                            <div class="col-md-1 p-0 mt-3 d-flex justify-content-center align-items-center">
                                <div class="list-group fileinput fileinput-new text-center m-0" data-provides="fileinput">
                                    <div class="fileinput-new thumbnail img-circle img-raised p-1 d-flex justify-content-center align-items-center"
                                        id="itemDiv" style="width: 40px; height: 40px;">
                                        <a href="javascript:;" onclick="clickOn()"
                                            class="list-group-item font-weight-bold">a</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-10 p-0 mt-3">
                                <div class="row-reverse p-0 m-0 ">
                                    <div class="col m-0 p-0">
                                        <label for="" class="h5 font-weight-bold"
                                            style="float: left; color: #38B9C3">Sorafenib-1</label>
                                    </div>
                                    <div class="col m-0 p-0">
                                        <hr class="m-0 p-0"
                                            style="width: 100%; height: 2px; border-style: solid; border-color: #38B9C3; float: left">
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mx-auto mb-2">
                    <div class="col-md-12 p-0">
                        <div class="row mx-auto p-0">
                            <div class="col-md-1 p-0 mt-3 d-flex justify-content-center align-items-center">
                                <div class="fileinput fileinput-new text-center m-0" data-provides="fileinput">
                                    <div class="list-group fileinput-new thumbnail img-circle img-raised p-1 d-flex justify-content-center align-items-center"
                                        id="itemDiv" style="width: 40px; height: 40px;">
                                        <a href="javascript:;" onclick="clickOn()"
                                            class="list-group-item font-weight-bold">b</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-10 p-0 mt-3">
                                <div class="row-reverse p-0 m-0 ">
                                    <div class="col m-0 p-0">
                                        <label for="" class="h5 font-weight-bold"
                                            style="float: left; color: #38B9C3">Regorafenib</label>
                                    </div>
                                    <div class="col m-0 p-0">
                                        <hr class="m-0 p-0"
                                            style="width: 100%; height: 2px; border-style: solid; border-color: #38B9C3; float: left">
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mx-auto mb-2">
                    <div class="col-md-12 p-0">
                        <div class="row mx-auto p-0">
                            <div class="col-md-1 p-0 mt-3 d-flex justify-content-center align-items-center">
                                <div class="fileinput fileinput-new text-center m-0" data-provides="fileinput">
                                    <div class="list-group fileinput-new thumbnail img-circle img-raised p-1 d-flex justify-content-center align-items-center"
                                        id="itemDiv" style="width: 40px; height: 40px;">
                                        <a href="javascript:;" onclick="clickOn()"
                                            class="list-group-item font-weight-bold">c</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-10 p-0 mt-3">
                                <div class="row-reverse p-0 m-0 ">
                                    <div class="col m-0 p-0">
                                        <label for="" class="h5 font-weight-bold"
                                            style="float: left; color: #38B9C3">Interferon beta-1b</label>
                                    </div>
                                    <div class="col m-0 p-0">
                                        <hr class="m-0 p-0"
                                            style="width: 100%; height: 2px; border-style: solid; border-color: #38B9C3; float: left">
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mx-auto mb-2">
                    <div class="col-md-12 p-0">
                        <div class="row mx-auto p-0">
                            <div class="col-md-1 p-0 mt-3 d-flex justify-content-center align-items-center">
                                <div class="fileinput fileinput-new text-center m-0" data-provides="fileinput">
                                    <div class="list-group fileinput-new thumbnail img-circle img-raised p-1 d-flex justify-content-center align-items-center"
                                        style="width: 40px; height: 40px;">
                                        <a href="javascript:;" class="list-group-item font-weight-bold">d</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-10 p-0 mt-3">
                                <div class="row-reverse p-0 m-0 ">
                                    <div class="col m-0 p-0">
                                        <label for="" class="h5 font-weight-bold"
                                            style="float: left; color: #38B9C3">Ninguna de las anteriores</label>
                                    </div>
                                    <div class="col m-0 p-0">
                                        <hr class="m-0 p-0"
                                            style="width: 100%; height: 2px; border-style: solid; border-color: #38B9C3; float: left">
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col p-0" id="item3" style="display: none;">
            <div class="row-reverse">
                <div class="col p-0">
                    <label for="" class="h4">Conecte las afirmaciones con el circulo correspondiente indiando
                        si es verdadero (V) o falso (F)</label>
                </div>
                <div class="row mx-auto mb-2">
                    <div class="col-md-12 p-0">
                        <div class="row mx-auto p-0">
                            <div class="col-md-1 p-0 mt-3 d-flex justify-content-center align-items-center">
                                <div class="row-reverse">
                                    <div class="col mb-2">
                                        <i class="fas fa-check-circle text-success" onclick="check_true()"
                                            style="cursor: pointer;"> V</i>
                                    </div>
                                    <div class="col">
                                        <i class=" fas fa-times-circle text-danger" onclick="check_false()"
                                            style="cursor: pointer;"> F</i>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-10 p-0 mt-3">
                                <div class="row-reverse p-0 m-0 ">
                                    <div class="col m-0 p-0">
                                        <label for="" class="h5 font-weight-bold"
                                            style="float: left; color: #38B9C3">Pacientes en Alto Riesgo II hace referencia
                                            aquellos que tiene más de 24 meses en el tratamiento y que tienen niveles de
                                            adherencia inferiores al 80% durante los últims 6 meses.</label>
                                    </div>
                                    <div class="col m-0 p-0">
                                        <hr class="m-0 p-0"
                                            style="width: 100%; height: 2px; border-style: solid; border-color: #38B9C3; float: left">
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-1 d-flex align-items-center">
                                <div class="col" id="true" style="display: none">
                                    <button class="btn-sm btn-success border-0" style="cursor: pointer;">
                                        <span>Verdadero
                                            <input type="checkbox" class="mx-auto" name="true" id="myChecktrue"
                                                disabled style="display: none;"></span>
                                    </button>
                                </div>
                                <div class="col" id="false" style="display: none">
                                    <button class="btn-sm btn-danger border-0" style="cursor: pointer;">
                                        <span>Falso
                                            <input type="checkbox" class="mx-auto" name="false" id="myCheckfalse"
                                                disabled style="display: none;"></span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- PRIMER MODAL -->

        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row ">
                            <div class="col d-flex justify-content-center">
                                <label for="" class="font-weight-bold lead" style="color: #38B9C3;">Has terminado el
                                    Entrenamiento 1</label>
                            </div>
                        </div>
                        <div class="row mt-5">
                            <div class="col-md-3 p-0 d-flex justify-content-end align-items-center">
                                <div class="fileinput fileinput-new text-center m-0" data-provides="fileinput">
                                    <div class="fileinput-new thumbnail img-circle img-raised d-flex justify-content-center align-items-center"
                                        style="background: linear-gradient(90deg, rgba(56,185,195,1) 35%, rgba(245,215,142,1) 100%); width: 200px; height: 100px;">
                                        <a href="javascript:;" class="h2 font-weight-bold text-white">20</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-8 p-0 m-0">
                                <div class="row-reverse p-0 m-0">
                                    <div class="col m-0 p-0">
                                        <label for="" class="text-right">Tienes 3 de 5 respuestas correctas, has
                                            ganado:</label>
                                        <label for="" class="h3 font-weight-bold"
                                            style="float: right; color: #38B9C3">PUNTOS</label>
                                    </div>
                                    <div class="col m-0 p-0">
                                        <hr class="m-0 p-0"
                                            style="width: 100%; height: 2px; border-style: solid; border-color: #38B9C3; float: left">
                                    </div>
                                </div>


                            </div>
                            <div class="row-reverse ml-auto mt-3 mr-3">
                                <div class="col mx-auto">
                                    <a class="btn text-white " style="background: #AEDC5A;" data-toggle="modal"
                                        data-target="#exampleModal2" data-dismiss="modal">SIGUIENTE ENTRENAMIENTO</a>
                                </div>
                                <div class="col d-flex justify-content-end">
                                    <a style="color: #02718B; text-decoration-line: underline"
                                        href="{{ route('practice_profesional.index') }}">Volver a hacer el examen</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- SEGUNDO MODAL -->

        <div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row ">
                            <div class="col d-flex justify-content-center">
                                <label for="" class="font-weight-bold lead" style="color: #38B9C3;">Has terminado los
                                    entrenamientos básicos de Adempas®</label>
                            </div>
                        </div>
                        <div class="row mt-5">
                            <div class="col-md-3 p-0 d-flex justify-content-end align-items-center">
                                <div class="fileinput fileinput-new text-center m-0" data-provides="fileinput">
                                    <div class="fileinput-new thumbnail img-circle img-raised d-flex justify-content-center align-items-center"
                                        style="background: linear-gradient(90deg, rgba(56,185,195,1) 35%, rgba(245,215,142,1) 100%); width: 200px; height: 100px;">
                                        <a href="javascript:;" class="h2 font-weight-bold text-white">20</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-8 p-0 m-0">
                                <div class="row-reverse p-0 m-0">
                                    <div class="col m-0 p-0">
                                        <label for="" class="text-right">Felicitaciones, has cumplido con todos los
                                            ejercicios del entrenamiento básico de Adempas®, has ganado:</label>
                                        <label for="" class="h3 font-weight-bold"
                                            style="float: right; color: #38B9C3">insignia Adempas JR.</label>
                                    </div>
                                    <div class="col m-0 p-0">
                                        <hr class="m-0 p-0"
                                            style="width: 100%; height: 2px; border-style: solid; border-color: #38B9C3; float: left">
                                    </div>
                                    <div class="col m-0 p-0">
                                        <label for="" class="h3 p-0 m-0" style="float: right; color: #AEDC5A">200
                                            Puntos</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row-reverse ml-auto mt-3">
                                <div class="col d-flex justify-content-end">
                                    <a class="btn text-white" style="background: #AEDC5A;"
                                        href="{{ route('practice_profesional.index') }}">ENTRENAMIENTOS MEDIO</a>
                                </div>
                                <div class="col d-flex justify-content-end">
                                    <a href="{{ route('practice_profesional.index') }}"
                                        style="color: #02718B; text-decoration-line: underline">Volver a hacer el
                                        entrenamiento básico</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>


    <div class="row mx-auto" id="bottom1">
        <div class="col d-flex align-items-center">
            <label for="" style="text-decoration-line: underline;">SALIR</label>
        </div>
        <div class="col d-flex justify-content-end">
            <a href="javascript:;" class="btn" style="background: #AEDC5A;" onclick="item1()">Siguiente</a>
        </div>

    </div>

    <div class="row mx-auto" id="bottom2" style="display: none;">
        <div class="col d-flex align-items-center">
            <label for="" style="text-decoration-line: underline;">SALIR</label>
        </div>
        <div class="col d-flex justify-content-end">
            <a href="javascript:;" class="btn" style="background: #AEDC5A;" onclick="item2()">Siguiente</a>
        </div>

    </div>

    <div class="row mx-auto" id="bottom3" style="display: none;">
        <div class="col d-flex align-items-center">
            <label for="" style="text-decoration-line: underline;">SALIR</label>
        </div>
        <div class="col d-flex justify-content-end">
            <a href="javascript:;" class="btn" style="background: #AEDC5A;" onclick="item3()"
                data-toggle="modal" data-target="#exampleModal">Finalizar</a>
        </div>

    </div>

    
<script>
    function myFunction() {
        if (window.matchMedia("(max-width: 500px)").matches) {
            var elemento = document.getElementById("fila");
            $('#fila').css({
                'margin-left' : '10%'
            })

            if (elemento.className == "row-reverse") {
                elemento.className = "row"
            } else {
                alert("error")
            }

        } else {
           return null;
        }
    }

    window.onload=myFunction();
</script>

@endsection
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>


<style>
    @media screen and (max-width: 500px) {
        #item1 {
            margin: 3em !important;
        }

        #item2 {
            margin: 3em !important;
        }

        #item3 {
            margin: 3em !important;
        }
    }

</style>

<script>
    function check_true() {
        $("#myChecktrue").prop('checked', true);
        $("#true").css({
            'display': 'block'
        })
        $("#false").css({
            'display': 'none'
        })
    }

    function check_false() {
        $("#myCheckfalse").prop('checked', true);
        $("#false").css({
            'display': 'block'
        })
        $("#true").css({
            'display': 'none'
        })
    }
</script>

<script>
    $(document).ready(function() {
        $('.list-group-item').click(function() {
            $('.list-group-item').removeClass('active');
            $(this).closest('.list-group-item').addClass('active')
        });
    });


    function item1() {
        $('#item1').css({
            'display': 'none'
        })
        $('#item2').css({
            'display': 'block'
        })
        $('#ico1').css({
            'display': 'block',
            'color': 'white'
        })
        $('#one').css({
            'display': 'none'
        })
        $('#divtwo').css({
            'background': 'linear-gradient(90deg, rgba(56,185,195,1) 35%, rgba(245,215,142,1) 100%)'
        })
        $('#bottom1').css({
            'display': 'none'
        })
        $('#bottom2').css({
            'display': 'block'
        })

        $('#bottom3').css({
            'display': 'none'
        })
    }

    function item2() {
        $('#item1').css({
            'display': 'none'
        })
        $('#item2').css({
            'display': 'none'
        })
        $('#item3').css({
            'display': 'block'
        })
        $('#ico1').css({
            'display': 'block',
            'color': 'white'
        })
        $('#one').css({
            'display': 'none'
        })
        $('#ico2').css({
            'display': 'block',
            'color': 'white'
        })
        $('#two').css({
            'display': 'none'
        })
        $('#ico3').css({
            'display': 'none',
            'color': 'white'
        })
        $('#three').css({
            'display': 'block'
        })
        $('#divtwo').css({
            'background': 'linear-gradient(90deg, rgba(56,185,195,1) 35%, rgba(245,215,142,1) 100%)'
        })
        $('#divthree').css({
            'background': 'linear-gradient(90deg, rgba(56,185,195,1) 35%, rgba(245,215,142,1) 100%)'
        })
        $('#bottom1').css({
            'display': 'none'
        })
        $('#bottom2').css({
            'display': 'none'
        })
        $('#bottom3').css({
            'display': 'block'
        })
    }

    function item3() {
        $('#item1').css({
            'display': 'none'
        })
        $('#item2').css({
            'display': 'none'
        })
        $('#item3').css({
            'display': 'none'
        })
        $('#ico1').css({
            'display': 'block',
            'color': 'white'
        })
        $('#one').css({
            'display': 'none'
        })
        $('#ico2').css({
            'display': 'block',
            'color': 'white'
        })
        $('#two').css({
            'display': 'none'
        })
        $('#ico3').css({
            'display': 'block',
            'color': 'white'
        })
        $('#three').css({
            'display': 'none'
        })
        $('#divtwo').css({
            'background': 'linear-gradient(90deg, rgba(56,185,195,1) 35%, rgba(245,215,142,1) 100%)'
        })
        $('#divthree').css({
            'background': 'linear-gradient(90deg, rgba(56,185,195,1) 35%, rgba(245,215,142,1) 100%)'
        })
        $('#bottom1').css({
            'display': 'none'
        })
        $('#bottom2').css({
            'display': 'none'
        })
        $('#bottom3').css({
            'display': 'block'
        })
    }
</script>
